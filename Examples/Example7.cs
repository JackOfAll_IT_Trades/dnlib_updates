﻿using System;
using System.Collections.Generic;

using dnlib.DotNet;

namespace dnlib.Examples {

	// This example will open mscorlib.dll and then print out all types
	// in the assembly, including the number of methods, fields, properties
	// and events each type has.
	public class Example7 {

		public static void Run() {
			// Load mscorlib.dll
			var filename = typeof(Dictionary<string, string>).Module.FullyQualifiedName;
			var mod = ModuleDefMD.Load(filename);

			string searchTypeName   = "System.Collections.Generic.Dictionary`2";
			string searchMethodName = "Insert";
			bool      bMethodFound  = false;
			MethodDef vMethodData;

			foreach ( var type in mod.GetTypes() ) {
				if ( type.FullName.ToLower() == searchTypeName.ToLower() ) {
					Console.WriteLine(String.Format("Found Dictionary type: <{0}>...", type.FullName));

					foreach (var method in type.Methods) {
						if ( method.Name.ToLower() == searchMethodName.ToLower() ) {
							Console.WriteLine(String.Format("Found Dictionary method: <{0}>...", method.FullName));

							bMethodFound = true;
							vMethodData  = method;
						}

					} // END foreach (var method in type.Methods) { ... }

				} // END if (type.FullName.ToLower() == "system.collections.generic.dictionary`2") { ... }

			} // END foreach (var type in mod.GetTypes()) { ... }

			Console.WriteLine();
			Console.WriteLine("All done!");

		} // END public class Example7 { ... }

	} // END public class Example7 { ... }

} // END namespace dnlib.Examples { ... }

