﻿using System;
using System.Collections.Generic;

using Pose;

using System.Reflection;
using System.Threading;
using System.Globalization;


namespace PoseTest_DLL
{
	public class PoseTest
	{
		/*
		static void PoseTestDictionaryShims(FieldInfo dictionaryField)
		{
			Dictionary<string, string> PoseTestDictionary = new Dictionary<string, string>();

			string PoseTestKey   = "PoseTestKey";
			string PoseTestValue = "PoseTestValue";

			System.Console.WriteLine( "Inserting dictionary element:  PoseTestDictionary[{0}] = '{1}'...",
									  PoseTestKey, PoseTestValue);
			PoseTestDictionary[PoseTestKey] = PoseTestValue;

			System.Console.WriteLine( "Reading dictionary element:    PoseTestDictionary[{0}] = '{1}'...",
										PoseTestKey, PoseTestDictionary[PoseTestKey] );

			System.Console.WriteLine( "Removing dictionary element:   PoseTestDictionary[{0}]...",
									  PoseTestKey);

			PoseTestDictionary.Remove(PoseTestKey);

			Shim shim = Shim.Replace(() => PoseTestDictionary.this, true);

			Shim shim = Shim.Replace(() => Is.A<Thread>().CurrentCulture, true);

			Assert.AreEqual(typeof(Thread).GetProperty(nameof(Thread.CurrentCulture), typeof(CultureInfo)).SetMethod, shim.Original);
			Assert.IsNull(shim.Replacement);
		}
		*/

		public static void FixDictionaryBug(string newValue)
		{
			System.Console.WriteLine("FixDictionaryBug() -- Entering...");

			System.Console.WriteLine("FixDictionaryBug() -- Attempting to get 'entry' assembly...");
			Assembly entryAssembly = Assembly.GetEntryAssembly();

			System.Console.WriteLine("FixDictionaryBug() -- Attempting to get 'Program' class...");
			Type TestHostClass = entryAssembly.GetType("PoseTestHost.Program");

//			System.Console.WriteLine("FixDictionaryBug() -- Attempting to get 'TopLevelDictionary' members...");
//			MemberInfo[] topLevelDictionaries = TestHostClass.GetMember("TopLevelDictionary");

			System.Console.WriteLine("FixDictionaryBug() -- Attempting to get all 'Program' fields...");
			FieldInfo[] programFields = TestHostClass.GetFields( BindingFlags.Instance  |
																 BindingFlags.Static    |
																 BindingFlags.Public    |
																 BindingFlags.NonPublic |
																 BindingFlags.FlattenHierarchy );

			System.Console.WriteLine("FixDictionaryBug() -- Searching for 'TopLevelDictionary'...");
			bool bTopLevelDictionaryFound = false;
			bool  bAnotherDictionaryFound = false;
			foreach (FieldInfo f in programFields)
			{
				string fieldName = f.Name;
				if (fieldName.Contains("TopLevelDictionary"))
				{
					bTopLevelDictionaryFound = true;
					System.Console.WriteLine("FixDictionaryBug() -- 'TopLevelDictionary' found.  (Yay!)");

					// Dictionary<string, string> originalDict = new Dictionary<string, string>();
					// f.GetValue(originalDict);

//					BetterDictionary<string, string> newBetterDict = new BetterDictionary<string, string>();
//					f.SetValue(TestHostClass, newBetterDict);
				}
				else if (fieldName.Contains("AnotherDictionary"))
				{
					bAnotherDictionaryFound = true;
					System.Console.WriteLine("FixDictionaryBug() -- 'AnotherDictionary' found.  (Yay!)");

					// Dictionary<string, string> originalDict = new Dictionary<string, string>();
					// f.GetValue(originalDict);

					//					BetterDictionary<string, string> newBetterDict = new BetterDictionary<string, string>();
					//					f.SetValue(TestHostClass, newBetterDict);
				}
			}
			if (!bTopLevelDictionaryFound)
			{
				System.Console.WriteLine("FixDictionaryBug() -- 'TopLevelDictionary' not found.  (Boo!)");
			}
			if (!bAnotherDictionaryFound)
			{
				System.Console.WriteLine("FixDictionaryBug() -- 'AnotherDictionary' not found.  (Boo!)");
			}
		}
	}
}
