﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.


using System;
using System.Collections;
using System.Collections.Generic;

using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Threading;


namespace PoseTest_DLL
{
	/// <summary>
	/// Used internally to control behavior of insertion into a <see cref="Dictionary{TKey, TValue}"/>.
	/// </summary>
	internal enum InsertionBehavior : byte
	{
		/// <summary>
		/// The default insertion behavior.
		/// </summary>
		None = 0,

		/// <summary>
		/// Specifies that an existing entry with the same key should be overwritten if encountered.
		/// </summary>
		OverwriteExisting = 1,

		/// <summary>
		/// Specifies that if an existing entry with the same key is encountered, an exception should be thrown.
		/// </summary>
		ThrowOnExisting = 2
	}

//	[DebuggerTypeProxy(typeof(IDictionaryDebugView<,>))]
	[DebuggerDisplay("Count = {Count}")]
//	[Serializable]
//	[TypeForwardedFrom("mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")]

	public class BetterDictionary<TKey, TValue> : ICollection<KeyValuePair<TKey, TValue>>, IEnumerable<KeyValuePair<TKey, TValue>>, IEnumerable, IDictionary, IDictionary<TKey, TValue>, IReadOnlyDictionary<TKey, TValue>, ISerializable, IDeserializationCallback
	{
		private Dictionary<TKey, TValue> _InternalDictionary;

		public static implicit operator Dictionary<TKey, TValue>(BetterDictionary<TKey, TValue> d)
		{
			return d._InternalDictionary;
		}


		public BetterDictionary() : this(0, null) {
			_InternalDictionary = new Dictionary<TKey, TValue>(0, null);
		}

		public BetterDictionary(int capacity) : this(capacity, null)
		{
			_InternalDictionary = new Dictionary<TKey, TValue>(capacity, null);
		}

		public BetterDictionary(IEqualityComparer<TKey> comparer) : this(0, comparer)
		{
			_InternalDictionary = new Dictionary<TKey, TValue>(0, comparer);
		}

		public BetterDictionary(int capacity, IEqualityComparer<TKey> comparer)
		{
			_InternalDictionary = new Dictionary<TKey, TValue>(capacity, comparer);
		}

		public BetterDictionary(IDictionary<TKey, TValue> dictionary) : this(dictionary, null)
		{
			_InternalDictionary = new Dictionary<TKey, TValue>(dictionary, null);
		}

		public BetterDictionary(IDictionary<TKey, TValue> dictionary, IEqualityComparer<TKey> comparer) :
			this(dictionary != null ? dictionary.Count : 0, comparer)
		{
			_InternalDictionary = new Dictionary<TKey, TValue>(dictionary, comparer);
		}

		public BetterDictionary(IEnumerable<KeyValuePair<TKey, TValue>> collection) : this(collection, null)
		{
			if (collection == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.collection);
			}

			_InternalDictionary = new Dictionary<TKey, TValue>((collection as ICollection<KeyValuePair<TKey, TValue>>)?.Count ?? 0, null);

			foreach (KeyValuePair<TKey, TValue> pair in collection)
			{
				_InternalDictionary.Add(pair.Key, pair.Value);
			}

			return;
		}

		public BetterDictionary(IEnumerable<KeyValuePair<TKey, TValue>> collection, IEqualityComparer<TKey> comparer) :
			this((collection as ICollection<KeyValuePair<TKey, TValue>>)?.Count ?? 0, comparer)
		{
			if (collection == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.collection);
			}

			_InternalDictionary = new Dictionary<TKey, TValue>((collection as ICollection<KeyValuePair<TKey, TValue>>)?.Count ?? 0, comparer);

			foreach (KeyValuePair<TKey, TValue> pair in collection)
			{
				_InternalDictionary.Add(pair.Key, pair.Value);
			}

			return;
		}

		//		protected BetterDictionary(SerializationInfo info, StreamingContext context)
		//		{
		//			// We can't do anything with the keys and values until the entire graph has been deserialized
		//			// and we have a resonable estimate that GetHashCode is not going to fail.  For the time being,
		//			// we'll just cache this.  The graph is not valid until OnDeserialization has been called.
		//			HashHelpers.SerializationInfoTable.Add(this, info);
		//		}


		//
		// Summary:
		//     Copies the elements of the System.Collections.ICollection to an System.Array,
		//     starting at a particular System.Array index.
		//
		// Parameters:
		//   array:
		//     The one-dimensional System.Array that is the destination of the elements copied
		//     from System.Collections.ICollection. The System.Array must have zero-based indexing.
		//
		//   index:
		//     The zero-based index in array at which copying begins.
		//
		// Exceptions:
		//   T:System.ArgumentNullException:
		//     array is null.
		//
		//   T:System.ArgumentOutOfRangeException:
		//     index is less than zero.
		//
		//   T:System.ArgumentException:
		//     array is multidimensional. -or- The number of elements in the source System.Collections.ICollection
		//     is greater than the available space from index to the end of the destination
		//     array. -or- The type of the source System.Collections.ICollection cannot be cast
		//     automatically to the type of the destination array.
//		public void ICollection.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)

		void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{
			throw new System.NotImplementedException();
		}

		void ICollection.CopyTo(Array array, int arrayIndex)
		{
			throw new System.NotImplementedException();
		}


		public IEqualityComparer<TKey> Comparer
		{
			get
			{
				if ((_InternalDictionary.Comparer == null) ||
					(_InternalDictionary.Comparer is NonRandomizedStringEqualityComparer))
				{
					return EqualityComparer<TKey>.Default;
				}

				return _InternalDictionary.Comparer;
			}
		}

		public int Count
		{
			get { return _InternalDictionary.Count; }
		}

		public Dictionary<TKey, TValue>.KeyCollection Keys
		{
			get
			{
				return _InternalDictionary.Keys;
			}
		}

		ICollection<TKey> IDictionary<TKey, TValue>.Keys
		{
			get
			{
				return _InternalDictionary.Keys;
			}
		}

		IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys
		{
			get
			{
				return _InternalDictionary.Keys;
			}
		}

		public Dictionary<TKey, TValue>.ValueCollection Values
		{
			get
			{
				return _InternalDictionary.Values;
			}
		}

		ICollection<TValue> IDictionary<TKey, TValue>.Values
		{
			get
			{
				return _InternalDictionary.Values;
			}
		}

		IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values
		{
			get
			{
				return _InternalDictionary.Values;
			}
		}

		public TValue this[TKey key]
		{
			get
			{
				return _InternalDictionary[key];
			}
			set
			{
				System.Console.WriteLine("BetterDictionary::TValue this[] -- Requested to set key [{0}] to '{1}'...", key, value);
				_InternalDictionary[key] = value;
			}
		}

		public void Add(TKey key, TValue value)
		{
			System.Console.WriteLine("BetterDictionary::TValue Add() -- Requested to set key [{0}] to '{1}'...", key, value);
			_InternalDictionary.Add(key, value);
		}

		void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> keyValuePair)
			=> this.Add(keyValuePair.Key, keyValuePair.Value);

		bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> keyValuePair)
		{
			TKey key = keyValuePair.Key;
			bool keyPresent = _InternalDictionary.ContainsKey(key);
			if (!keyPresent)
			{
				return false;
			}

			TValue value = keyValuePair.Value;
			if (EqualityComparer<TValue>.Default.Equals(_InternalDictionary[key], value))
			{
				return true;
			}

			return false;
		}

		bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> keyValuePair)
		{
			TKey key = keyValuePair.Key;
			bool keyPresent = _InternalDictionary.ContainsKey(key);
			if (!keyPresent)
			{
				return false;
			}

			TValue value = keyValuePair.Value;
			if (EqualityComparer<TValue>.Default.Equals(_InternalDictionary[key], value))
			{
				_InternalDictionary.Remove(keyValuePair.Key);
				return true;
			}

			return false;
		}

		public void Clear()
		{
			_InternalDictionary.Clear();
		}

		public bool ContainsKey(TKey key)
		{
			bool   keyPresent = _InternalDictionary.ContainsKey(key);
			return keyPresent;
		}

		public bool ContainsValue(TValue value)
		{
			bool   valuePresent = _InternalDictionary.ContainsValue(value);
			return valuePresent;
		}

		//public Enumerator GetEnumerator()
		//	=> new Enumerator(this._InternalDictionary, Enumerator.KeyValuePair);

		IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
		{
			return this._InternalDictionary.GetEnumerator();
		}
//		=> new Enumerator(this._InternalDictionary, Enumerator.KeyValuePair);


		// The overload Remove(TKey key, out TValue value) is a copy of this method with one additional
		// statement to copy the value for entry being removed into the output parameter.
		// Code has been intentionally duplicated for performance reasons.
		public bool Remove(TKey key)
		{
			return _InternalDictionary.Remove(key);
		}


		// This overload is a copy of the overload Remove(TKey key) with one additional
		// statement to copy the value for entry being removed into the output parameter.
		// Code has been intentionally duplicated for performance reasons.
		public bool Remove(TKey key, out TValue value)
		{
			bool keyPresent = _InternalDictionary.ContainsKey(key);
			if (!keyPresent)
			{
				value = _InternalDictionary[	key];
			}
			else
			{
				value = default;
			}

			return _InternalDictionary.Remove(key);
		}


		public bool TryGetValue(TKey key, out TValue value)
		{
			bool keyPresent = _InternalDictionary.ContainsKey(key);
			if (!keyPresent)
			{
				value = _InternalDictionary[key];
				return true;
			}

			value = default;
			return false;
		}

//		public bool TryAdd(TKey key, TValue value)
//		{
//			System.Console.WriteLine("BetterDictionary::TryAdd() --  Requested to set key [{0}] to '{1}'...", key, value);
//			_InternalDictionary.TryAdd(key, value);
//		}
//		=> TryInsert(key, value, InsertionBehavior.None);

		bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly => false;


		IEnumerator IEnumerable.GetEnumerator()
		{
			return this._InternalDictionary.GetEnumerator();
		}
//		=> new Enumerator(this._InternalDictionary, Enumerator.KeyValuePair);

		//
		// Summary:
		//     Implements the System.Runtime.Serialization.ISerializable interface and returns
		//     the data needed to serialize the System.Collections.Generic.Dictionary`2 instance.
		//
		// Parameters:
		//   info:
		//     A System.Runtime.Serialization.SerializationInfo object that contains the information
		//     required to serialize the System.Collections.Generic.Dictionary`2 instance.
		//
		//   context:
		//     A System.Runtime.Serialization.StreamingContext structure that contains the source
		//     and destination of the serialized stream associated with the System.Collections.Generic.Dictionary`2
		//     instance.
		//
		// Exceptions:
		//   T:System.ArgumentNullException:
		//     info is null.
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context) { this._InternalDictionary.GetObjectData(info, context); }

		//
		// Summary:
		//     Implements the System.Runtime.Serialization.ISerializable interface and raises
		//     the deserialization event when the deserialization is complete.
		//
		// Parameters:
		//   sender:
		//     The source of the deserialization event.
		//
		// Exceptions:
		//   T:System.Runtime.Serialization.SerializationException:
		//     The System.Runtime.Serialization.SerializationInfo object associated with the
		//     current System.Collections.Generic.Dictionary`2 instance is invalid.
		public virtual void OnDeserialization(object sender) { this._InternalDictionary.OnDeserialization(sender); }


		bool ICollection.IsSynchronized => false;

		object ICollection.SyncRoot => this;

		bool IDictionary.IsFixedSize => false;

		bool IDictionary.IsReadOnly => false;

		ICollection IDictionary.Keys => (ICollection)Keys;

		ICollection IDictionary.Values => (ICollection)Values;

		int ICollection<KeyValuePair<TKey, TValue>>.Count => throw new System.NotImplementedException();

		TValue IDictionary<TKey, TValue>.this[TKey key] { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

		object IDictionary.this[object key]
		{
			get
			{
				if (IsCompatibleKey(key))
				{
					TKey tempKey = (TKey)key;
					if (_InternalDictionary.ContainsKey(tempKey))
					{
						return _InternalDictionary[tempKey];
					}
				}
				return null;
			}
			set
			{
				if (key == null)
				{
					ThrowHelper.ThrowArgumentNullException(ExceptionArgument.key);
				}
				ThrowHelper.IfNullAndNullsAreIllegalThenThrow<TValue>(value, ExceptionArgument.value);

				try
				{
					System.Console.WriteLine("BetterDictionary::object this[] -- Requested to set key [{0}] to '{1}'...", key, value);

					TKey tempKey = (TKey)key;
					try
					{
						TValue tempValue = (TValue)value;
						this.Add(tempKey, tempValue);
					}
					catch (InvalidCastException)
					{
						ThrowHelper.ThrowWrongValueTypeArgumentException(value, typeof(TValue));
					}
				}
				catch (InvalidCastException)
				{
					ThrowHelper.ThrowWrongKeyTypeArgumentException(key, typeof(TKey));
				}
			}
		}

		private static bool IsCompatibleKey(object key)
		{
			if (key == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.key);
			}
			return (key is TKey);
		}

		void IDictionary.Add(object key, object value)
		{
			if (key == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.key);
			}
			ThrowHelper.IfNullAndNullsAreIllegalThenThrow<TValue>(value, ExceptionArgument.value);

			try
			{
				System.Console.WriteLine("BetterDictionary::object Add() -- Requested to set key [{0}] to '{1}'...", key, value);

				TKey tempKey = (TKey)key;

				try
				{
					Add(tempKey, (TValue)value);
				}
				catch (InvalidCastException)
				{
					ThrowHelper.ThrowWrongValueTypeArgumentException(value, typeof(TValue));
				}
			}
			catch (InvalidCastException)
			{
				ThrowHelper.ThrowWrongKeyTypeArgumentException(key, typeof(TKey));
			}
		}

		bool IDictionary.Contains(object key)
		{
			if (IsCompatibleKey(key))
			{
				return ContainsKey((TKey)key);
			}

			return false;
		}

		IDictionaryEnumerator IDictionary.GetEnumerator()
		{
			return this._InternalDictionary.GetEnumerator();
		}
//		=> new Enumerator(this, Enumerator.DictEntry);

		void IDictionary.Remove(object key)
		{
			if (IsCompatibleKey(key))
			{
				Remove((TKey)key);
			}
		}

		void IDictionary<TKey, TValue>.Add(TKey key, TValue value)
		{
			throw new System.NotImplementedException();
		}

		bool IDictionary<TKey, TValue>.ContainsKey(TKey key)
		{
			throw new System.NotImplementedException();
		}

		bool IDictionary<TKey, TValue>.Remove(TKey key)
		{
			throw new System.NotImplementedException();
		}

		bool IDictionary<TKey, TValue>.TryGetValue(TKey key, out TValue value)
		{
			throw new System.NotImplementedException();
		}

		void ICollection<KeyValuePair<TKey, TValue>>.Clear()
		{
			throw new System.NotImplementedException();
		}

	}
}
