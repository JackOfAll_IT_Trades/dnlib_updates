﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PoseTest_DLL
{
	class StringResources
	{
		// .NET StringResources class, necessary for interoperability with .NET Standard Dictionary code,
		//  mostly taken from "https://github.com/Microsoft/referencesource/blob/master/mscorlib/mscorlib.txt".
		//

//;==++==
//;
//;   Copyright (c) Microsoft Corporation.  All rights reserved.
//;
//;==--==
//;
//; These are the managed resources for mscorlib.dll.
//; See those first three bytes in the file?  This is in UTF-8.  Leave the
//; Unicode byte order mark (U+FEFF) written in UTF-8 at the start of this file.

//; For resource info, see the ResourceManager documentation and the ResGen tool,
//; which is a managed app using ResourceWriter.
//; ResGen now supports C++ & C# style #ifdef's, like #ifndef FOO and #if BAR

//; The naming scheme is: [Namespace.] ExceptionName _ Reason
//; We'll suppress "System." where possible.
//; Examples:
//; Argument_Null
//; Reflection.TargetInvokation_someReason

//; Usage Notes:
//; * Keep exceptions in alphabetical order by package
//; * A single space may exist on either side of the equal sign.
//; * Follow the naming conventions.
//; * Any lines starting with a '#' or ';' are ignored
//; * Equal signs aren't legal characters for keys, but may occur in values.
//; * Correctly punctuate all sentences. Most resources should end in a period.
//;       Remember, your mother will probably read some of these messages.
//; * You may use " (quote), \n and \t. Use \\ for a single '\' character.
//; * String inserts work.  i.e., BadNumber_File = "Wrong number in file "{0}".";

//; Real words, used by code like Environment.StackTrace
//#if INCLUDE_RUNTIME
public static string Word_At = "at";
public static string StackTrace_InFileLineNumber = "in {0}:line {1}";
public static string UnknownError_Num = "Unknown error \"{0}\".";
public static string AllocatedFrom = "Allocated from:";

//; Note this one is special, used as a divider between stack traces!
public static string Exception_EndOfInnerExceptionStack = "--- End of inner exception stack trace ---";
public static string Exception_WasThrown = "Exception of type '{0}' was thrown.";

//; The following are used in the implementation of ExceptionDispatchInfo
public static string Exception_EndStackTraceFromPreviousThrow = "--- End of stack trace from previous location where exception was thrown ---";

public static string Arg_ParamName_Name = "Parameter name: {0}";
public static string ArgumentOutOfRange_ActualValue = "Actual value was {0}.";

public static string NoDebugResources = "[{0}]\r\nArguments: {1}\r\nDebugging resource strings are unavailable. Often the key and arguments provide sufficient information to diagnose the problem. See http://go.microsoft.com/fwlink/?linkid=106663&Version={2}&File={3}&Key={4}";
//#endif // INCLUDE_RUNTIME

//#if !FEATURE_CORECLR
public static string UnknownError = "Unknown error.";
//#endif // !FEATURE_CORECLR

//#if INCLUDE_DEBUG

//; For code contracts
public static string AssumptionFailed = "Assumption failed.";
public static string AssumptionFailed_Cnd = "Assumption failed: {0}";
public static string AssertionFailed = "Assertion failed.";
public static string AssertionFailed_Cnd = "Assertion failed: {0}";
public static string PreconditionFailed = "Precondition failed.";
public static string PreconditionFailed_Cnd = "Precondition failed: {0}";
public static string PostconditionFailed = "Postcondition failed.";
public static string PostconditionFailed_Cnd = "Postcondition failed: {0}";
public static string PostconditionOnExceptionFailed = "Postcondition failed after throwing an exception.";
public static string PostconditionOnExceptionFailed_Cnd = "Postcondition failed after throwing an exception: {0}";
public static string InvariantFailed = "Invariant failed.";
public static string InvariantFailed_Cnd = "Invariant failed: {0}";
public static string StackTrace_Stack = "Stack trace: \r\n{0}";
public static string MustUseCCRewrite = "An assembly (probably \"{1}\") must be rewritten using the code contracts binary rewriter (CCRewrite) because it is calling Contract.{0} and the CONTRACTS_FULL symbol is defined.  Remove any explicit definitions of the CONTRACTS_FULL symbol from your project and rebuild.  CCRewrite can be downloaded from http://go.microsoft.com/fwlink/?LinkID=169180. \r\nAfter the rewriter is installed, it can be enabled in Visual Studio from the project's Properties page on the Code Contracts pane.  Ensure that \"Perform Runtime Contract Checking\" is enabled, which will define CONTRACTS_FULL.";

//; Access Control
//#if FEATURE_MACL
public static string AccessControl_MustSpecifyContainerAcl = "The named parameter must be a container ACL.";
public static string AccessControl_MustSpecifyLeafObjectAcl = "The named parameter must be a non-container ACL.";
public static string AccessControl_AclTooLong = "Length of the access control list exceed the allowed maximum.";
public static string AccessControl_MustSpecifyDirectoryObjectAcl = "The named parameter must be a directory-object ACL.";
public static string AccessControl_MustSpecifyNonDirectoryObjectAcl = "The named parameter must be a non-directory-object ACL.";
public static string AccessControl_InvalidSecurityDescriptorRevision = "Security descriptor with revision other than '1' are not legal.";
public static string AccessControl_InvalidSecurityDescriptorSelfRelativeForm = "Security descriptor must be in the self-relative form.";
public static string AccessControl_NoAssociatedSecurity = "Unable to perform a security operation on an object that has no associated security. This can happen when trying to get an ACL of an anonymous kernel object.";
public static string AccessControl_InvalidHandle = "The supplied handle is invalid. This can happen when trying to set an ACL on an anonymous kernel object.";
public static string AccessControl_UnexpectedError = "Method failed with unexpected error code {0}.";
public static string AccessControl_InvalidSidInSDDLString = "The SDDL string contains an invalid sid or a sid that cannot be translated.";
public static string AccessControl_InvalidOwner = "The security identifier is not allowed to be the owner of this object.";
public static string AccessControl_InvalidGroup = "The security identifier is not allowed to be the primary group of this object.";
public static string AccessControl_InvalidAccessRuleType = "The access rule is not the correct type.";
public static string AccessControl_InvalidAuditRuleType = "The audit rule is not the correct type.";
//#endif  // FEATURE_MACL

//; Identity Reference Library
//#if FEATURE_IDENTITY_REFERENCE
public static string IdentityReference_IdentityNotMapped = "Some or all identity references could not be translated.";
public static string IdentityReference_MustBeIdentityReference = "The targetType parameter must be of IdentityReference type.";
public static string IdentityReference_AccountNameTooLong = "Account name is too long.";
public static string IdentityReference_DomainNameTooLong = "Domain name is too long.";
public static string IdentityReference_InvalidNumberOfSubauthorities = "The number of sub-authorities must not exceed {0}.";
public static string IdentityReference_IdentifierAuthorityTooLarge = "The size of the identifier authority must not exceed 6 bytes.";
public static string IdentityReference_InvalidSidRevision = "SIDs with revision other than '1' are not supported.";
public static string IdentityReference_CannotCreateLogonIdsSid = "Well-known SIDs of type LogonIdsSid cannot be created.";
public static string IdentityReference_DomainSidRequired = "The domainSid parameter must be specified for creating well-known SID of type {0}.";
public static string IdentityReference_NotAWindowsDomain = "The domainSid parameter is not a valid Windows domain SID.";
//#endif // FEATURE_IDENTITY_REFERENCE

//; AccessException
public static string Acc_CreateGeneric = "Cannot create a type for which Type.ContainsGenericParameters is true.";
public static string Acc_CreateAbst = "Cannot create an abstract class.";
public static string Acc_CreateInterface = "Cannot create an instance of an interface.";
public static string Acc_NotClassInit = "Type initializer was not callable.";
public static string Acc_CreateGenericEx = "Cannot create an instance of {0} because Type.ContainsGenericParameters is true.";
public static string Acc_CreateArgIterator = "Cannot dynamically create an instance of ArgIterator.";
public static string Acc_CreateAbstEx = "Cannot create an instance of {0} because it is an abstract class.";
public static string Acc_CreateInterfaceEx = "Cannot create an instance of {0} because it is an interface.";
public static string Acc_CreateVoid = "Cannot dynamically create an instance of System.Void.";
public static string Acc_ReadOnly = "Cannot set a constant field.";
public static string Acc_RvaStatic = "SkipVerification permission is needed to modify an image-based (RVA) static field.";
public static string Access_Void = "Cannot create an instance of void.";

//; ArgumentException
public static string Arg_TypedReference_Null = "The TypedReference must be initialized.";
public static string Argument_AddingDuplicate__ = "Item has already been added. Key in dictionary: '{0}'  Key being added: '{1}'";
public static string Argument_AddingDuplicate = "An item with the same key has already been added.";
public static string Argument_MethodDeclaringTypeGenericLcg = "Method '{0}' has a generic declaring type '{1}'. Explicitly provide the declaring type to GetTokenFor. ";
public static string Argument_MethodDeclaringTypeGeneric = "Cannot resolve method {0} because the declaring type of the method handle {1} is generic. Explicitly provide the declaring type to GetMethodFromHandle. ";
public static string Argument_FieldDeclaringTypeGeneric = "Cannot resolve field {0} because the declaring type of the field handle {1} is generic. Explicitly provide the declaring type to GetFieldFromHandle.";
public static string Argument_ApplicationTrustShouldHaveIdentity = "An ApplicationTrust must have an application identity before it can be persisted.";
public static string Argument_ConversionOverflow = "Conversion buffer overflow.";
public static string Argument_CodepageNotSupported = "{0} is not a supported code page.";
public static string Argument_CultureNotSupported = "Culture is not supported.";
public static string Argument_CultureInvalidIdentifier = "{0} is an invalid culture identifier.";
public static string Argument_OneOfCulturesNotSupported = "Culture name {0} or {1} is not supported.";
public static string Argument_CultureIetfNotSupported = "Culture IETF Name {0} is not a recognized IETF name.";
public static string Argument_CultureIsNeutral = "Culture ID {0} (0x{0:X4}) is a neutral culture; a region cannot be created from it.";
public static string Argument_InvalidNeutralRegionName = "The region name {0} should not correspond to neutral culture; a specific culture name is required.";
public static string Argument_InvalidGenericInstArray = "Generic arguments must be provided for each generic parameter and each generic argument must be a RuntimeType.";
public static string Argument_GenericArgsCount = "The number of generic arguments provided doesn't equal the arity of the generic type definition.";
public static string Argument_CultureInvalidFormat = "Culture '{0}' is a neutral culture. It cannot be used in formatting and parsing and therefore cannot be set as the thread's current culture.";
public static string Argument_CompareOptionOrdinal = "CompareOption.Ordinal cannot be used with other options.";
public static string Argument_CustomCultureCannotBePassedByNumber = "Customized cultures cannot be passed by LCID, only by name.";
public static string Argument_EncodingConversionOverflowChars = "The output char buffer is too small to contain the decoded characters, encoding '{0}' fallback '{1}'.";
public static string Argument_EncodingConversionOverflowBytes = "The output byte buffer is too small to contain the encoded data, encoding '{0}' fallback '{1}'.";
public static string Argument_EncoderFallbackNotEmpty = "Must complete Convert() operation or call Encoder.Reset() before calling GetBytes() or GetByteCount(). Encoder '{0}' fallback '{1}'.";
public static string Argument_EmptyFileName = "Empty file name is not legal.";
public static string Argument_EmptyPath = "Empty path name is not legal.";
public static string Argument_EmptyName = "Empty name is not legal.";
public static string Argument_ImplementIComparable = "At least one object must implement IComparable.";
public static string Argument_InvalidType = "The type of arguments passed into generic comparer methods is invalid.";
public static string Argument_InvalidTypeForCA = "Cannot build type parameter for custom attribute with a type that does not support the AssemblyQualifiedName property. The type instance supplied was of type '{0}'.";
public static string Argument_IllegalEnvVarName = "Environment variable name cannot contain equal character.";
public static string Argument_IllegalAppId = "Application identity does not have same number of components as manifest paths.";
public static string Argument_IllegalAppBase = "The application base specified is not valid.";
public static string Argument_UnableToParseManifest = "Unexpected error while parsing the specified manifest.";
public static string Argument_IllegalAppIdMismatch = "Application identity does not match identities in manifests.";
public static string Argument_InvalidAppId = "Invalid identity: no deployment or application identity specified.";
public static string Argument_InvalidGenericArg = "The generic type parameter was not valid";
public static string Argument_InvalidArrayLength = "Length of the array must be {0}.";
public static string Argument_InvalidArrayType = "Target array type is not compatible with the type of items in the collection.";
public static string Argument_InvalidAppendMode = "Append access can be requested only in write-only mode.";
public static string Argument_InvalidEnumValue = "The value '{0}' is not valid for this usage of the type {1}.";
public static string Argument_EnumIsNotIntOrShort = "The underlying type of enum argument must be Int32 or Int16.";
public static string Argument_InvalidEnum = "The Enum type should contain one and only one instance field.";
public static string Argument_InvalidKeyStore = "'{0}' is not a valid KeyStore name. ";
public static string Argument_InvalidFileMode_AccessCombo = "Combining FileMode: {0} with FileAccess: {1} is invalid.";
public static string Argument_InvalidFileMode_RightsCombo = "Combining FileMode: {0} with FileSystemRights: {1} is invalid.";
public static string Argument_InvalidFileModeTruncate_RightsCombo = "Combining FileMode: {0} with FileSystemRights: {1} is invalid. FileMode.Truncate is valid only when used with FileSystemRights.Write.";
public static string Argument_InvalidFlag = "Value of flags is invalid.";
public static string Argument_InvalidAnyFlag = "No flags can be set.";
public static string Argument_InvalidHandle = "The handle is invalid.";
public static string Argument_InvalidRegistryKeyPermissionCheck = "The specified RegistryKeyPermissionCheck value is invalid.";
public static string Argument_InvalidRegistryOptionsCheck = "The specified RegistryOptions value is invalid.";
public static string Argument_InvalidRegistryViewCheck = "The specified RegistryView value is invalid.";
public static string Argument_InvalidSubPath = "The directory specified, '{0}', is not a subdirectory of '{1}'.";
public static string Argument_NoRegionInvariantCulture = "There is no region associated with the Invariant Culture (Culture ID: 0x7F).";
public static string Argument_ResultCalendarRange = "The result is out of the supported range for this calendar. The result should be between {0} (Gregorian date) and {1} (Gregorian date), inclusive.";
public static string Argument_ResultIslamicCalendarRange = "The date is out of the supported range for the Islamic calendar. The date should be greater than July 18th, 622 AD (Gregorian date).";
public static string Argument_NeverValidGenericArgument = "The type '{0}' may not be used as a type argument.";
public static string Argument_NotEnoughGenArguments = "The type or method has {1} generic parameter(s), but {0} generic argument(s) were provided. A generic argument must be provided for each generic parameter.";
public static string Argument_NullFullTrustAssembly = "A null StrongName was found in the full trust assembly list.";
public static string Argument_GenConstraintViolation = "GenericArguments[{0}], '{1}', on '{2}' violates the constraint of type '{3}'.";
public static string Argument_InvalidToken = "Token {0:x} is not valid in the scope of module {1}.";
public static string Argument_InvalidTypeToken = "Token {0:x} is not a valid Type token.";
public static string Argument_ResolveType = "Token {0:x} is not a valid Type token in the scope of module {1}.";
public static string Argument_ResolveMethod = "Token {0:x} is not a valid MethodBase token in the scope of module {1}.";
public static string Argument_ResolveField = "Token {0:x} is not a valid FieldInfo token in the scope of module {1}.";
public static string Argument_ResolveMember = "Token {0:x} is not a valid MemberInfo token in the scope of module {1}.";
public static string Argument_ResolveString = "Token {0:x} is not a valid string token in the scope of module {1}.";
public static string Argument_ResolveModuleType = "Token {0} resolves to the special module type representing this module.";
public static string Argument_ResolveMethodHandle = "Type handle '{0}' and method handle with declaring type '{1}' are incompatible. Get RuntimeMethodHandle and declaring RuntimeTypeHandle off the same MethodBase.";
public static string Argument_ResolveFieldHandle = "Type handle '{0}' and field handle with declaring type '{1}' are incompatible. Get RuntimeFieldHandle and declaring RuntimeTypeHandle off the same FieldInfo.";
public static string Argument_ResourceScopeWrongDirection = "Resource type in the ResourceScope enum is going from a more restrictive resource type to a more general one.  From: \"{0}\"  To: \"{1}\"";
public static string Argument_BadResourceScopeTypeBits = "Unknown value for the ResourceScope: {0}  Too many resource type bits may be set.";
public static string Argument_BadResourceScopeVisibilityBits = "Unknown value for the ResourceScope: {0}  Too many resource visibility bits may be set.";
public static string Argument_WaitHandleNameTooLong = "The name can be no more than 260 characters in length.";
public static string Argument_EnumTypeDoesNotMatch = "The argument type, '{0}', is not the same as the enum type '{1}'.";
public static string InvalidOperation_MethodBuilderBaked = "The signature of the MethodBuilder can no longer be modified because an operation on the MethodBuilder caused the methodDef token to be created. For example, a call to SetCustomAttribute requires the methodDef token to emit the CustomAttribute token.";
public static string InvalidOperation_GenericParametersAlreadySet = "The generic parameters are already defined on this MethodBuilder.";
public static string Arg_AccessException = "Cannot access member.";
public static string Arg_AppDomainUnloadedException = "Attempted to access an unloaded AppDomain.";
public static string Arg_ApplicationException = "Error in the application.";
public static string Arg_ArgumentOutOfRangeException = "Specified argument was out of the range of valid values.";
public static string Arg_ArithmeticException = "Overflow or underflow in the arithmetic operation.";
public static string Arg_ArrayLengthsDiffer = "Array lengths must be the same.";
public static string Arg_ArrayPlusOffTooSmall = "Destination array is not long enough to copy all the items in the collection. Check array index and length.";
public static string Arg_ArrayTypeMismatchException = "Attempted to access an element as a type incompatible with the array.";
public static string Arg_BadImageFormatException = "Format of the executable (.exe) or library (.dll) is invalid.";
public static string Argument_BadImageFormatExceptionResolve = "A BadImageFormatException has been thrown while parsing the signature. This is likely due to lack of a generic context. Ensure genericTypeArguments and genericMethodArguments are provided and contain enough context.";
public static string Arg_BufferTooSmall = "Not enough space available in the buffer.";
public static string Arg_CATypeResolutionFailed = "Failed to resolve type from string \"{0}\" which was embedded in custom attribute blob.";
public static string Arg_CannotHaveNegativeValue = "String cannot contain a minus sign if the base is not 10.";
public static string Arg_CannotUnloadAppDomainException = "Attempt to unload the AppDomain failed.";
public static string Arg_CannotMixComparisonInfrastructure = "The usage of IKeyComparer and IHashCodeProvider/IComparer interfaces cannot be mixed; use one or the other.";
public static string Arg_ContextMarshalException = "Attempted to marshal an object across a context boundary.";
public static string Arg_DataMisalignedException = "A datatype misalignment was detected in a load or store instruction.";
public static string Arg_DevicesNotSupported = "FileStream will not open Win32 devices such as disk partitions and tape drives. Avoid use of \"\\\\.\\\" in the path.";
public static string Arg_DuplicateWaitObjectException = "Duplicate objects in argument.";
public static string Arg_EntryPointNotFoundException = "Entry point was not found.";
public static string Arg_DllNotFoundException = "Dll was not found.";
public static string Arg_ExecutionEngineException = "Internal error in the runtime.";
public static string Arg_FieldAccessException = "Attempted to access a field that is not accessible by the caller.";
public static string Arg_FileIsDirectory_Name = "The target file \"{0}\" is a directory, not a file.";
public static string Arg_FormatException = "One of the identified items was in an invalid format.";
public static string Arg_IndexOutOfRangeException = "Index was outside the bounds of the array.";
public static string Arg_InsufficientExecutionStackException = "Insufficient stack to continue executing the program safely. This can happen from having too many functions on the call stack or function on the stack using too much stack space.";
public static string Arg_InvalidCastException = "Specified cast is not valid.";
public static string Arg_InvalidOperationException = "Operation is not valid due to the current state of the object.";
public static string Arg_CorruptedCustomCultureFile = "The file of the custom culture {0} is corrupt. Try to unregister this culture.";
public static string Arg_InvokeMember = "InvokeMember can be used only for COM objects.";
public static string Arg_InvalidNeutralResourcesLanguage_Asm_Culture = "The NeutralResourcesLanguageAttribute on the assembly \"{0}\" specifies an invalid culture name: \"{1}\".";
public static string Arg_InvalidNeutralResourcesLanguage_FallbackLoc = "The NeutralResourcesLanguageAttribute specifies an invalid or unrecognized ultimate resource fallback location: \"{0}\".";
public static string Arg_InvalidSatelliteContract_Asm_Ver = "Satellite contract version attribute on the assembly '{0}' specifies an invalid version: {1}.";
public static string Arg_MethodAccessException = "Attempt to access the method failed.";
public static string Arg_MethodAccessException_WithMethodName = "Attempt to access the method \"{0}\" on type \"{1}\" failed.";
public static string Arg_MethodAccessException_WithCaller = "Attempt by security transparent method '{0}' to access security critical method '{1}' failed.";
public static string Arg_MissingFieldException = "Attempted to access a non-existing field.";
public static string Arg_MissingMemberException = "Attempted to access a missing member.";
public static string Arg_MissingMethodException = "Attempted to access a missing method.";
public static string Arg_MulticastNotSupportedException = "Attempted to add multiple callbacks to a delegate that does not support multicast.";
public static string Arg_NotFiniteNumberException = "Number encountered was not a finite quantity.";
public static string Arg_NotSupportedException = "Specified method is not supported.";
public static string Arg_UnboundGenParam = "Late bound operations cannot be performed on types or methods for which ContainsGenericParameters is true.";
public static string Arg_UnboundGenField = "Late bound operations cannot be performed on fields with types for which Type.ContainsGenericParameters is true.";
public static string Arg_NotGenericParameter = "Method may only be called on a Type for which Type.IsGenericParameter is true.";
public static string Arg_GenericParameter = "Method must be called on a Type for which Type.IsGenericParameter is false.";
public static string Arg_NotGenericTypeDefinition = "{0} is not a GenericTypeDefinition. MakeGenericType may only be called on a type for which Type.IsGenericTypeDefinition is true.";
public static string Arg_NotGenericMethodDefinition = "{0} is not a GenericMethodDefinition. MakeGenericMethod may only be called on a method for which MethodBase.IsGenericMethodDefinition is true.";
public static string Arg_BadLiteralFormat = "Encountered an invalid type for a default value.";
public static string Arg_MissingActivationArguments = "The AppDomainSetup must specify the activation arguments for this call.";
public static string Argument_BadParameterTypeForCAB = "Cannot emit a CustomAttribute with argument of type {0}.";
public static string Argument_InvalidMemberForNamedArgument = "The member must be either a field or a property.";
public static string Argument_InvalidTypeName = "The name of the type is invalid.";

//; Note - don't change the NullReferenceException default message. This was
//; negotiated carefully with the VB team to avoid saying "null" or "nothing".
public static string Arg_NullReferenceException = "Object reference not set to an instance of an object.";

public static string Arg_AccessViolationException = "Attempted to read or write protected memory. This is often an indication that other memory is corrupt.";
public static string Arg_OverflowException = "Arithmetic operation resulted in an overflow.";
public static string Arg_PathGlobalRoot = "Paths that begin with \\\\?\\GlobalRoot are internal to the kernel and should not be opened by managed applications.";
public static string Arg_PathIllegal = "The path is not of a legal form.";
public static string Arg_PathIllegalUNC = "The UNC path should be of the form \\\\server\\share.";
public static string Arg_RankException = "Attempted to operate on an array with the incorrect number of dimensions.";
public static string Arg_RankMultiDimNotSupported = "Only single dimensional arrays are supported for the requested action.";
public static string Arg_NonZeroLowerBound = "The lower bound of target array must be zero.";
public static string Arg_RegSubKeyValueAbsent = "No value exists with that name.";
public static string Arg_ResourceFileUnsupportedVersion = "The ResourceReader class does not know how to read this version of .resources files. Expected version: {0}  This file: {1}";
public static string Arg_ResourceNameNotExist = "The specified resource name \"{0}\" does not exist in the resource file.";
public static string Arg_SecurityException = "Security error.";
public static string Arg_SerializationException = "Serialization error.";
public static string Arg_StackOverflowException = "Operation caused a stack overflow.";
public static string Arg_SurrogatesNotAllowedAsSingleChar = "Unicode surrogate characters must be written out as pairs together in the same call, not individually. Consider passing in a character array instead.";
public static string Arg_SynchronizationLockException = "Object synchronization method was called from an unsynchronized block of code.";
public static string Arg_RWLockRestoreException = "ReaderWriterLock.RestoreLock was called without releasing all locks acquired since the call to ReleaseLock.";
public static string Arg_SystemException = "System error.";
public static string Arg_TimeoutException = "The operation has timed out.";
public static string Arg_UnauthorizedAccessException = "Attempted to perform an unauthorized operation.";
public static string Arg_ArgumentException = "Value does not fall within the expected range.";
public static string Arg_DirectoryNotFoundException = "Attempted to access a path that is not on the disk.";
public static string Arg_DriveNotFoundException = "Attempted to access a drive that is not available.";
public static string Arg_EndOfStreamException = "Attempted to read past the end of the stream.";
public static string Arg_HexStyleNotSupported = "The number style AllowHexSpecifier is not supported on floating point data types.";
public static string Arg_IOException = "I/O error occurred.";
public static string Arg_InvalidHexStyle = "With the AllowHexSpecifier bit set in the enum bit field, the only other valid bits that can be combined into the enum value must be a subset of those in HexNumber.";
public static string Arg_KeyNotFound = "The given key was not present in the dictionary.";
public static string Argument_InvalidNumberStyles = "An undefined NumberStyles value is being used.";
public static string Argument_InvalidDateTimeStyles = "An undefined DateTimeStyles value is being used.";
public static string Argument_InvalidTimeSpanStyles = "An undefined TimeSpanStyles value is being used.";
public static string Argument_DateTimeOffsetInvalidDateTimeStyles = "The DateTimeStyles value 'NoCurrentDateDefault' is not allowed when parsing DateTimeOffset.";
public static string Argument_NativeResourceAlreadyDefined = "Native resource has already been defined.";
public static string Argument_BadObjRef = "Invalid ObjRef provided to '{0}'.";
public static string Argument_InvalidCultureName = "Culture name '{0}' is not supported.";
public static string Argument_NameTooLong = "The name '{0}' is too long to be a Culture or Region name, which is limited to {1} characters.";
public static string Argument_NameContainsInvalidCharacters = "The name '{0}' contains characters that are not valid for a Culture or Region.";
public static string Argument_InvalidRegionName = "Region name '{0}' is not supported.";
public static string Argument_CannotCreateTypedReference = "Cannot use function evaluation to create a TypedReference object.";
public static string Arg_ArrayZeroError = "Array must not be of length zero.";
public static string Arg_BogusIComparer = "Unable to sort because the IComparer.Compare() method returns inconsistent results. Either a value does not compare equal to itself, or one value repeatedly compared to another value yields different results. IComparer: '{0}'.";
public static string Arg_CreatInstAccess = "Cannot specify both CreateInstance and another access type.";
public static string Arg_CryptographyException = "Error occurred during a cryptographic operation.";
public static string Arg_DateTimeRange = "Combination of arguments to the DateTime constructor is out of the legal range.";
public static string Arg_DecBitCtor = "Decimal byte array constructor requires an array of length four containing valid decimal bytes.";
public static string Arg_DlgtTargMeth = "Cannot bind to the target method because its signature or security transparency is not compatible with that of the delegate type.";
public static string Arg_DlgtTypeMis = "Delegates must be of the same type.";
public static string Arg_DlgtNullInst = "Delegate to an instance method cannot have null 'this'.";
public static string Arg_DllInitFailure = "One machine may not have remote administration enabled, or both machines may not be running the remote registry service.";
public static string Arg_EmptyArray = "Array may not be empty.";
public static string Arg_EmptyOrNullArray = "Array may not be empty or null.";
public static string Arg_EmptyCollection = "Collection must not be empty.";
public static string Arg_EmptyOrNullString = "String may not be empty or null.";
public static string Argument_ItemNotExist = "The specified item does not exist in this KeyedCollection.";
public static string Argument_EncodingNotSupported = "'{0}' is not a supported encoding name. For information on defining a custom encoding, see the documentation for the Encoding.RegisterProvider method.";
public static string Argument_FallbackBufferNotEmpty = "Cannot change fallback when buffer is not empty. Previous Convert() call left data in the fallback buffer.";
public static string Argument_InvalidCodePageConversionIndex = "Unable to translate Unicode character \\u{0:X4} at index {1} to specified code page.";
public static string Argument_InvalidCodePageBytesIndex = "Unable to translate bytes {0} at index {1} from specified code page to Unicode.";
public static string Argument_RecursiveFallback = "Recursive fallback not allowed for character \\u{0:X4}.";
public static string Argument_RecursiveFallbackBytes = "Recursive fallback not allowed for bytes {0}.";
public static string Arg_EnumAndObjectMustBeSameType = "Object must be the same type as the enum. The type passed in was '{0}'; the enum type was '{1}'.";
public static string Arg_EnumIllegalVal = "Illegal enum value: {0}.";
public static string Arg_EnumNotSingleFlag = "Must set exactly one flag.";
public static string Arg_EnumAtLeastOneFlag = "Must set at least one flag.";
public static string Arg_EnumUnderlyingTypeAndObjectMustBeSameType = "Enum underlying type and the object must be same type or object must be a String. Type passed in was '{0}'; the enum underlying type was '{1}'.";
public static string Arg_EnumFormatUnderlyingTypeAndObjectMustBeSameType = "Enum underlying type and the object must be same type or object. Type passed in was '{0}'; the enum underlying type was '{1}'.";
public static string Arg_EnumMustHaveUnderlyingValueField = "All enums must have an underlying value__ field.";
public static string Arg_COMAccess = "Must specify property Set or Get or method call for a COM Object.";
public static string Arg_COMPropSetPut = "Only one of the following binding flags can be set: BindingFlags.SetProperty, BindingFlags.PutDispProperty,  BindingFlags.PutRefDispProperty.";
public static string Arg_FldSetGet = "Cannot specify both Get and Set on a field.";
public static string Arg_PropSetGet = "Cannot specify both Get and Set on a property.";
public static string Arg_CannotBeNaN = "TimeSpan does not accept floating point Not-a-Number values.";
public static string Arg_FldGetPropSet = "Cannot specify both GetField and SetProperty.";
public static string Arg_FldSetPropGet = "Cannot specify both SetField and GetProperty.";
public static string Arg_FldSetInvoke = "Cannot specify Set on a Field and Invoke on a method.";
public static string Arg_FldGetArgErr = "No arguments can be provided to Get a field value.";
public static string Arg_FldSetArgErr = "Only the field value can be specified to set a field value.";
public static string Arg_GetMethNotFnd = "Property Get method was not found.";
public static string Arg_GuidArrayCtor = "Byte array for GUID must be exactly {0} bytes long.";
public static string Arg_HandleNotAsync = "Handle does not support asynchronous operations. The parameters to the FileStream constructor may need to be changed to indicate that the handle was opened synchronously (that is, it was not opened for overlapped I/O).";
public static string Arg_HandleNotSync = "Handle does not support synchronous operations. The parameters to the FileStream constructor may need to be changed to indicate that the handle was opened asynchronously (that is, it was opened explicitly for overlapped I/O).";
public static string Arg_HTCapacityOverflow = "Hashtable's capacity overflowed and went negative. Check load factor, capacity and the current size of the table.";
public static string Arg_IndexMustBeInt = "All indexes must be of type Int32.";
public static string Arg_InvalidConsoleColor = "The ConsoleColor enum value was not defined on that enum. Please use a defined color from the enum.";
public static string Arg_InvalidFileAttrs = "Invalid File or Directory attributes value.";
public static string Arg_InvalidHandle = "Invalid handle.";
public static string Arg_InvalidTypeInSignature = "The signature Type array contains some invalid type (i.e. null, void)";
public static string Arg_InvalidTypeInRetType = "The return Type contains some invalid type (i.e. null, ByRef)";
public static string Arg_EHClauseNotFilter = "This ExceptionHandlingClause is not a filter.";
public static string Arg_EHClauseNotClause = "This ExceptionHandlingClause is not a clause.";
public static string Arg_ReflectionOnlyCA = "It is illegal to reflect on the custom attributes of a Type loaded via ReflectionOnlyGetType (see Assembly.ReflectionOnly) -- use CustomAttributeData instead.";
public static string Arg_ReflectionOnlyInvoke = "It is illegal to invoke a method on a Type loaded via ReflectionOnlyGetType.";
public static string Arg_ReflectionOnlyField = "It is illegal to get or set the value on a field on a Type loaded via ReflectionOnlyGetType.";
public static string Arg_MemberInfoNullModule = "The Module object containing the member cannot be null.";
public static string Arg_ParameterInfoNullMember = "The MemberInfo object defining the parameter cannot be null.";
public static string Arg_ParameterInfoNullModule = "The Module object containing the parameter cannot be null.";
public static string Arg_AssemblyNullModule = "The manifest module of the assembly cannot be null.";
public static string Arg_LongerThanSrcArray = "Source array was not long enough. Check srcIndex and length, and the array's lower bounds.";
public static string Arg_LongerThanDestArray = "Destination array was not long enough. Check destIndex and length, and the array's lower bounds.";
public static string Arg_LowerBoundsMustMatch = "The arrays' lower bounds must be identical.";
public static string Arg_MustBeBoolean = "Object must be of type Boolean.";
public static string Arg_MustBeByte = "Object must be of type Byte.";
public static string Arg_MustBeChar = "Object must be of type Char.";
public static string Arg_MustBeDateTime = "Object must be of type DateTime.";
public static string Arg_MustBeDateTimeOffset = "Object must be of type DateTimeOffset.";
public static string Arg_MustBeDecimal = "Object must be of type Decimal.";
public static string Arg_MustBeDelegate = "Type must derive from Delegate.";
public static string Arg_MustBeDouble = "Object must be of type Double.";
public static string Arg_MustBeDriveLetterOrRootDir = "Object must be a root directory (\"C:\\\") or a drive letter (\"C\").";
public static string Arg_MustBeEnum = "Type provided must be an Enum.";
public static string Arg_MustBeEnumBaseTypeOrEnum = "The value passed in must be an enum base or an underlying type for an enum, such as an Int32.";
public static string Arg_MustBeGuid = "Object must be of type GUID.";
public static string Arg_MustBeIdentityReferenceType = "Type must be an IdentityReference, such as NTAccount or SecurityIdentifier.";
public static string Arg_MustBeInterface = "Type passed must be an interface.";
public static string Arg_MustBeInt16 = "Object must be of type Int16.";
public static string Arg_MustBeInt32 = "Object must be of type Int32.";
public static string Arg_MustBeInt64 = "Object must be of type Int64.";
public static string Arg_MustBePrimArray = "Object must be an array of primitives.";
public static string Arg_MustBePointer = "Type must be a Pointer.";
public static string Arg_MustBeStatic = "Method must be a static method.";
public static string Arg_MustBeString = "Object must be of type String.";
public static string Arg_MustBeStringPtrNotAtom = "The pointer passed in as a String must not be in the bottom 64K of the process's address space.";
public static string Arg_MustBeSByte = "Object must be of type SByte.";
public static string Arg_MustBeSingle = "Object must be of type Single.";
public static string Arg_MustBeTimeSpan = "Object must be of type TimeSpan.";
public static string Arg_MustBeType = "Type must be a type provided by the runtime.";
public static string Arg_MustBeUInt16 = "Object must be of type UInt16.";
public static string Arg_MustBeUInt32 = "Object must be of type UInt32.";
public static string Arg_MustBeUInt64 = "Object must be of type UInt64.";
public static string Arg_MustBeVersion = "Object must be of type Version.";
public static string Arg_MustBeTrue = "Argument must be true.";
public static string Arg_MustAllBeRuntimeType = "At least one type argument is not a runtime type.";
public static string Arg_NamedParamNull = "Named parameter value must not be null.";
public static string Arg_NamedParamTooBig = "Named parameter array cannot be bigger than argument array.";
public static string Arg_Need1DArray = "Array was not a one-dimensional array.";
public static string Arg_Need2DArray = "Array was not a two-dimensional array.";
public static string Arg_Need3DArray = "Array was not a three-dimensional array.";
public static string Arg_NeedAtLeast1Rank = "Must provide at least one rank.";
public static string Arg_NoDefCTor = "No parameterless constructor defined for this object.";
public static string Arg_BitArrayTypeUnsupported = "Only supported array types for CopyTo on BitArrays are Boolean[], Int32[] and Byte[].";
public static string Arg_DivideByZero = "Attempted to divide by zero.";
public static string Arg_NoAccessSpec = "Must specify binding flags describing the invoke operation required (BindingFlags.InvokeMethod CreateInstance GetField SetField GetProperty SetProperty).";
public static string Arg_NoStaticVirtual = "Method cannot be both static and virtual.";
public static string Arg_NotFoundIFace = "Interface not found.";
public static string Arg_ObjObjEx = "Object of type '{0}' cannot be converted to type '{1}'.";
public static string Arg_ObjObj = "Object type cannot be converted to target type.";
public static string Arg_FieldDeclTarget = "Field '{0}' defined on type '{1}' is not a field on the target object which is of type '{2}'.";
public static string Arg_OleAutDateInvalid = "Not a legal OleAut date.";
public static string Arg_OleAutDateScale = "OleAut date did not convert to a DateTime correctly.";
public static string Arg_PlatformNotSupported = "Operation is not supported on this platform.";
public static string Arg_PlatformSecureString = "SecureString is only supported on Windows 2000 SP3 and higher platforms.";
public static string Arg_ParmCnt = "Parameter count mismatch.";
public static string Arg_ParmArraySize = "Must specify one or more parameters.";
public static string Arg_Path2IsRooted = "Second path fragment must not be a drive or UNC name.";
public static string Arg_PathIsVolume = "Path must not be a drive.";
public static string Arg_PrimWiden = "Cannot widen from source type to target type either because the source type is a not a primitive type or the conversion cannot be accomplished.";
public static string Arg_NullIndex = "Arrays indexes must be set to an object instance.";
public static string Arg_VarMissNull = "Missing parameter does not have a default value.";
public static string Arg_PropSetInvoke = "Cannot specify Set on a property and Invoke on a method.";
public static string Arg_PropNotFound = "Could not find the specified property.";
public static string Arg_RankIndices = "Indices length does not match the array rank.";
public static string Arg_RanksAndBounds = "Number of lengths and lowerBounds must match.";
public static string Arg_RegSubKeyAbsent = "Cannot delete a subkey tree because the subkey does not exist.";
public static string Arg_RemoveArgNotFound = "Cannot remove the specified item because it was not found in the specified Collection.";
public static string Arg_RegKeyDelHive = "Cannot delete a registry hive's subtree.";
public static string Arg_RegKeyNoRemoteConnect = "No remote connection to '{0}' while trying to read the registry.";
public static string Arg_RegKeyOutOfRange = "Registry HKEY was out of the legal range.";
public static string Arg_RegKeyNotFound = "The specified registry key does not exist.";
public static string Arg_RegKeyStrLenBug = "Registry key names should not be greater than 255 characters.";
public static string Arg_RegValStrLenBug = "Registry value names should not be greater than 16,383 characters.";
public static string Arg_RegBadKeyKind = "The specified RegistryValueKind is an invalid value.";
public static string Arg_RegGetOverflowBug = "RegistryKey.GetValue does not allow a String that has a length greater than Int32.MaxValue.";
public static string Arg_RegSetMismatchedKind = "The type of the value object did not match the specified RegistryValueKind or the object could not be properly converted.";
public static string Arg_RegSetBadArrType = "RegistryKey.SetValue does not support arrays of type '{0}'. Only Byte[] and String[] are supported.";
public static string Arg_RegSetStrArrNull = "RegistryKey.SetValue does not allow a String[] that contains a null String reference.";
public static string Arg_RegInvalidKeyName = "Registry key name must start with a valid base key name.";
public static string Arg_ResMgrNotResSet = "Type parameter must refer to a subclass of ResourceSet.";
public static string Arg_SetMethNotFnd = "Property set method not found.";
public static string Arg_TypeRefPrimitve = "TypedReferences cannot be redefined as primitives.";
public static string Arg_UnknownTypeCode = "Unknown TypeCode value.";
public static string Arg_VersionString = "Version string portion was too short or too long.";
public static string Arg_NoITypeInfo = "Specified TypeInfo was invalid because it did not support the ITypeInfo interface.";
public static string Arg_NoITypeLib = "Specified TypeLib was invalid because it did not support the ITypeLib interface.";
public static string Arg_NoImporterCallback = "Specified type library importer callback was invalid because it did not support the ITypeLibImporterNotifySink interface.";
public static string Arg_ImporterLoadFailure = "The type library importer encountered an error during type verification. Try importing without class members.";
public static string Arg_InvalidBase = "Invalid Base.";
public static string Arg_EnumValueNotFound = "Requested value '{0}' was not found.";
public static string Arg_EnumLitValueNotFound = "Literal value was not found.";
public static string Arg_MustContainEnumInfo = "Must specify valid information for parsing in the string.";
public static string Arg_InvalidSearchPattern = "Search pattern cannot contain \"..\" to move up directories and can be contained only internally in file/directory names, as in \"a..b\".";
public static string Arg_NegativeArgCount = "Argument count must not be negative.";
public static string Arg_InvalidAccessEntry = "Specified access entry is invalid because it is unrestricted. The global flags should be specified instead.";
public static string Arg_InvalidFileName = "Specified file name was invalid.";
public static string Arg_InvalidFileExtension = "Specified file extension was not a valid extension.";
public static string Arg_COMException = "Error HRESULT E_FAIL has been returned from a call to a COM component.";
public static string Arg_ExternalException = "External component has thrown an exception.";
public static string Arg_InvalidComObjectException = "Attempt has been made to use a COM object that does not have a backing class factory.";
public static string Arg_InvalidOleVariantTypeException = "Specified OLE variant was invalid.";
public static string Arg_MarshalDirectiveException = "Marshaling directives are invalid.";
public static string Arg_MarshalAsAnyRestriction = "AsAny cannot be used on return types, ByRef parameters, ArrayWithOffset, or parameters passed from unmanaged to managed.";
public static string Arg_NDirectBadObject = "No PInvoke conversion exists for value passed to Object-typed parameter.";
public static string Arg_SafeArrayTypeMismatchException = "Specified array was not of the expected type.";
public static string Arg_VTableCallsNotSupportedException = "Attempted to make an early bound call on a COM dispatch-only interface.";
public static string Arg_SafeArrayRankMismatchException = "Specified array was not of the expected rank.";
public static string Arg_AmbiguousMatchException = "Ambiguous match found.";
public static string Arg_CustomAttributeFormatException = "Binary format of the specified custom attribute was invalid.";
public static string Arg_InvalidFilterCriteriaException = "Specified filter criteria was invalid.";
public static string Arg_TypeLoadNullStr = "A null or zero length string does not represent a valid Type.";
public static string Arg_TargetInvocationException = "Exception has been thrown by the target of an invocation.";
public static string Arg_TargetParameterCountException = "Number of parameters specified does not match the expected number.";
public static string Arg_TypeAccessException = "Attempt to access the type failed.";
public static string Arg_TypeLoadException = "Failure has occurred while loading a type.";
public static string Arg_TypeUnloadedException = "Type had been unloaded.";
public static string Arg_ThreadStateException = "Thread was in an invalid state for the operation being executed.";
public static string Arg_ThreadStartException = "Thread failed to start.";
public static string Arg_WrongAsyncResult = "IAsyncResult object did not come from the corresponding async method on this type.";
public static string Arg_WrongType = "The value \"{0}\" is not of type \"{1}\" and cannot be used in this generic collection.";
public static string Argument_InvalidArgumentForComparison = "Type of argument is not compatible with the generic comparer.";
public static string Argument_ALSInvalidCapacity = "Specified capacity must not be less than the current capacity.";
public static string Argument_ALSInvalidSlot = "Specified slot number was invalid.";
public static string Argument_IdnIllegalName = "Decoded string is not a valid IDN name.";
public static string Argument_IdnBadBidi = "Left to right characters may not be mixed with right to left characters in IDN labels.";
public static string Argument_IdnBadLabelSize = "IDN labels must be between 1 and 63 characters long.";
public static string Argument_IdnBadNameSize = "IDN names must be between 1 and {0} characters long.";
public static string Argument_IdnBadPunycode = "Invalid IDN encoded string.";
public static string Argument_IdnBadStd3 = "Label contains character '{0}' not allowed with UseStd3AsciiRules";
public static string Arg_InvalidANSIString = "The ANSI string passed in could not be converted from the default ANSI code page to Unicode.";
public static string Arg_InvalidUTF8String = "The UTF8 string passed in could not be converted to Unicode.";
public static string Argument_InvalidCharSequence = "Invalid Unicode code point found at index {0}.";
public static string Argument_InvalidCharSequenceNoIndex = "String contains invalid Unicode code points.";
public static string Argument_InvalidCalendar = "Not a valid calendar for the given culture.";
public static string Argument_InvalidNormalizationForm = "Invalid or unsupported normalization form.";
public static string Argument_InvalidPathChars = "Illegal characters in path.";
public static string Argument_InvalidOffLen = "Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection.";
public static string Argument_InvalidSeekOrigin = "Invalid seek origin.";
public static string Argument_SeekOverflow = "The specified seek offset '{0}' would result in a negative Stream position.";
public static string Argument_InvalidUnity = "Invalid Unity type.";
public static string Argument_LongEnvVarName = "Environment variable name cannot contain 1024 or more characters.";
public static string Argument_LongEnvVarValue = "Environment variable name or value is too long.";
public static string Argument_StringFirstCharIsZero = "The first char in the string is the null character.";
public static string Argument_OnlyMscorlib = "Only mscorlib's assembly is valid.";
public static string Argument_PathEmpty = "Path cannot be the empty string or all whitespace.";
public static string Argument_PathFormatNotSupported = "The given path's format is not supported.";
public static string Argument_PathUriFormatNotSupported = "URI formats are not supported.";
public static string Argument_TypeNameTooLong = "Type name was too long. The fully qualified type name must be less than 1,024 characters.";
public static string Argument_StreamNotReadable = "Stream was not readable.";
public static string Argument_StreamNotWritable = "Stream was not writable.";
public static string Argument_InvalidNumberOfMembers = "MemberData contains an invalid number of members.";
public static string Argument_InvalidValue = "Value was invalid.";
public static string Argument_InvalidKey = "Key was invalid.";
public static string Argument_MinMaxValue = "'{0}' cannot be greater than {1}.";
public static string Argument_InvalidGroupSize = "Every element in the value array should be between one and nine, except for the last element, which can be zero.";
public static string Argument_MustHaveAttributeBaseClass = "Type passed in must be derived from System.Attribute or System.Attribute itself.";
public static string Argument_NoUninitializedStrings = "Uninitialized Strings cannot be created.";
public static string Argument_UnequalMembers = "Supplied MemberInfo does not match the expected type.";
public static string Argument_BadFormatSpecifier = "Format specifier was invalid.";
public static string Argument_InvalidHighSurrogate = "Found a high surrogate char without a following low surrogate at index: {0}. The input may not be in this encoding, or may not contain valid Unicode (UTF-16) characters.";
public static string Argument_InvalidLowSurrogate = "Found a low surrogate char without a preceding high surrogate at index: {0}. The input may not be in this encoding, or may not contain valid Unicode (UTF-16) characters.";
public static string Argument_UnmatchingSymScope = "Non-matching symbol scope.";
public static string Argument_NotInExceptionBlock = "Not currently in an exception block.";
public static string Argument_BadExceptionCodeGen = "Incorrect code generation for exception block.";
public static string Argument_NotExceptionType = "Does not extend Exception.";
public static string Argument_DuplicateResourceName = "Duplicate resource name within an assembly.";
public static string Argument_BadPersistableModuleInTransientAssembly = "Cannot have a persistable module in a transient assembly.";
public static string Argument_InvalidPermissionState = "Invalid permission state.";
public static string Argument_UnrestrictedIdentityPermission = "Identity permissions cannot be unrestricted.";
public static string Argument_WrongType = "Operation on type '{0}' attempted with target of incorrect type.";
public static string Argument_IllegalZone = "Illegal security permission zone specified.";
public static string Argument_HasToBeArrayClass = "Must be an array type.";
public static string Argument_InvalidDirectory = "Invalid directory, '{0}'.";
public static string Argument_DataLengthDifferent = "Parameters 'members' and 'data' must have the same length.";
public static string Argument_SigIsFinalized = "Completed signature cannot be modified.";
public static string Argument_ArraysInvalid = "Array or pointer types are not valid.";
public static string Argument_GenericsInvalid = "Generic types are not valid.";
public static string Argument_LargeInteger = "Integer or token was too large to be encoded.";
public static string Argument_BadSigFormat = "Incorrect signature format.";
public static string Argument_UnmatchedMethodForLocal = "Local passed in does not belong to this ILGenerator.";
public static string Argument_DuplicateName = "Tried to add NamedPermissionSet with non-unique name.";
public static string Argument_InvalidXMLElement = "Invalid XML. Missing required tag <{0}> for type '{1}'.";
public static string Argument_InvalidXMLMissingAttr = "Invalid XML. Missing required attribute '{0}'.";
public static string Argument_CannotGetTypeTokenForByRef = "Cannot get TypeToken for a ByRef type.";
public static string Argument_NotASimpleNativeType = "The UnmanagedType passed to DefineUnmanagedMarshal is not a simple type. None of the following values may be used: UnmanagedType.ByValTStr, UnmanagedType.SafeArray, UnmanagedType.ByValArray, UnmanagedType.LPArray, UnmanagedType.CustomMarshaler.";
public static string Argument_NotACustomMarshaler = "Not a custom marshal.";
public static string Argument_NoUnmanagedElementCount = "Unmanaged marshal does not have ElementCount.";
public static string Argument_NoNestedMarshal = "Only LPArray or SafeArray has nested unmanaged marshal.";
public static string Argument_InvalidXML = "Invalid Xml.";
public static string Argument_NoUnderlyingCCW = "The object has no underlying COM data associated with it.";
public static string Argument_BadFieldType = "Bad field type in defining field.";
public static string Argument_InvalidXMLBadVersion = "Invalid Xml - can only parse elements of version one.";
public static string Argument_NotAPermissionElement = "'elem' was not a permission element.";
public static string Argument_NPMSInvalidName = "Name can be neither null nor empty.";
public static string Argument_InvalidElementTag = "Invalid element tag '{0}'.";
public static string Argument_InvalidElementText = "Invalid element text '{0}'.";
public static string Argument_InvalidElementName = "Invalid element name '{0}'.";
public static string Argument_InvalidElementValue = "Invalid element value '{0}'.";
public static string Argument_AttributeNamesMustBeUnique = "Attribute names must be unique.";
//#if FEATURE_CAS_POLICY
public static string Argument_UninitializedCertificate = "Uninitialized certificate object.";
public static string Argument_MembershipConditionElement = "Element must be a <IMembershipCondition> element.";
public static string Argument_ReservedNPMS = "Cannot remove or modify reserved permissions set '{0}'.";
public static string Argument_NPMSInUse = "Permission set '{0}' was in use and could not be deleted.";
public static string Argument_StrongNameGetPublicKey = "Unable to obtain public key for StrongNameKeyPair.";
public static string Argument_SiteCannotBeNull = "Site name must be specified.";
public static string Argument_BlobCannotBeNull = "Public key must be specified.";
public static string Argument_ZoneCannotBeNull = "Zone must be specified.";
public static string Argument_UrlCannotBeNull = "URL must be specified.";
public static string Argument_NoNPMS = "Unable to find a permission set with the provided name.";
public static string Argument_FailedCodeGroup = "Failed to create a code group of type '{0}'.";
public static string Argument_CodeGroupChildrenMustBeCodeGroups = "All objects in the input list must have a parent type of 'CodeGroup'.";
//#endif // FEATURE_CAS_POLICY
//#if FEATURE_IMPERSONATION
public static string Argument_InvalidPrivilegeName = "Privilege '{0}' is not valid on this system.";
public static string Argument_TokenZero = "Token cannot be zero.";
public static string Argument_InvalidImpersonationToken = "Invalid token for impersonation - it cannot be duplicated.";
public static string Argument_ImpersonateUser = "Unable to impersonate user.";
//#endif // FEATURE_IMPERSONATION
public static string Argument_InvalidHexFormat = "Improperly formatted hex string.";
public static string Argument_InvalidSite = "Invalid site.";
public static string Argument_InterfaceMap = "'this' type cannot be an interface itself.";
public static string Argument_ArrayGetInterfaceMap = "Interface maps for generic interfaces on arrays cannot be retrived.";
public static string Argument_InvalidName = "Invalid name.";
public static string Argument_InvalidDirectoryOnUrl = "Invalid directory on URL.";
public static string Argument_InvalidUrl = "Invalid URL.";
public static string Argument_InvalidKindOfTypeForCA = "This type cannot be represented as a custom attribute.";
public static string Argument_MustSupplyContainer = "When supplying a FieldInfo for fixing up a nested type, a valid ID for that containing object must also be supplied.";
public static string Argument_MustSupplyParent = "When supplying the ID of a containing object, the FieldInfo that identifies the current field within that object must also be supplied.";
public static string Argument_NoClass = "Element does not specify a class.";
public static string Argument_WrongElementType = "'{0}' element required.";
public static string Argument_UnableToGeneratePermissionSet = "Unable to generate permission set; input XML may be malformed.";
public static string Argument_NoEra = "No Era was supplied.";
public static string Argument_AssemblyAlreadyFullTrust = "Assembly was already fully trusted.";
public static string Argument_AssemblyNotFullTrust = "Assembly was not fully trusted.";
public static string Argument_AssemblyWinMD = "Assembly must not be a Windows Runtime assembly.";
public static string Argument_MemberAndArray = "Cannot supply both a MemberInfo and an Array to indicate the parent of a value type.";
public static string Argument_ObjNotComObject = "The object's type must be __ComObject or derived from __ComObject.";
public static string Argument_ObjIsWinRTObject = "The object's type must not be a Windows Runtime type.";
public static string Argument_TypeNotComObject = "The type must be __ComObject or be derived from __ComObject.";
public static string Argument_TypeIsWinRTType = "The type must not be a Windows Runtime type.";
public static string Argument_CantCallSecObjFunc = "Cannot evaluate a security function.";
public static string Argument_StructMustNotBeValueClass = "The structure must not be a value class.";
public static string Argument_NoSpecificCulture = "Please select a specific culture, such as zh-CN, zh-HK, zh-TW, zh-MO, zh-SG.";
public static string Argument_InvalidResourceCultureName = "The given culture name '{0}' cannot be used to locate a resource file. Resource filenames must consist of only letters, numbers, hyphens or underscores.";
public static string Argument_InvalidParamInfo = "Invalid type for ParameterInfo member in Attribute class.";
public static string Argument_EmptyDecString = "Decimal separator cannot be the empty string.";
public static string Argument_OffsetOfFieldNotFound = "Field passed in is not a marshaled member of the type '{0}'.";
public static string Argument_EmptyStrongName = "StrongName cannot have an empty string for the assembly name.";
public static string Argument_NotSerializable = "Argument passed in is not serializable.";
public static string Argument_EmptyApplicationName = "ApplicationId cannot have an empty string for the name.";
public static string Argument_NoDomainManager = "The domain manager specified by the host could not be instantiated.";
public static string Argument_NoMain = "Main entry point not defined.";
public static string Argument_InvalidDateTimeKind = "Invalid DateTimeKind value.";
public static string Argument_ConflictingDateTimeStyles = "The DateTimeStyles values AssumeLocal and AssumeUniversal cannot be used together.";
public static string Argument_ConflictingDateTimeRoundtripStyles = "The DateTimeStyles value RoundtripKind cannot be used with the values AssumeLocal, AssumeUniversal or AdjustToUniversal.";
public static string Argument_InvalidDigitSubstitution = "The DigitSubstitution property must be of a valid member of the DigitShapes enumeration. Valid entries include Context, NativeNational or None.";
public static string Argument_InvalidNativeDigitCount = "The NativeDigits array must contain exactly ten members.";
public static string Argument_InvalidNativeDigitValue = "Each member of the NativeDigits array must be a single text element (one or more UTF16 code points) with a Unicode Nd (Number, Decimal Digit) property indicating it is a digit.";
public static string ArgumentException_InvalidAceBinaryForm = "The binary form of an ACE object is invalid.";
public static string ArgumentException_InvalidAclBinaryForm = "The binary form of an ACL object is invalid.";
public static string ArgumentException_InvalidSDSddlForm = "The SDDL form of a security descriptor object is invalid.";
public static string Argument_InvalidSafeHandle = "The SafeHandle is invalid.";
public static string Argument_CannotPrepareAbstract = "Abstract methods cannot be prepared.";
public static string Argument_ArrayTooLarge = "The input array length must not exceed Int32.MaxValue / {0}. Otherwise BitArray.Length would exceed Int32.MaxValue.";
public static string Argument_RelativeUrlMembershipCondition = "UrlMembershipCondition requires an absolute URL.";
public static string Argument_EmptyWaithandleArray = "Waithandle array may not be empty.";
public static string Argument_InvalidSafeBufferOffLen = "Offset and length were greater than the size of the SafeBuffer.";
public static string Argument_NotEnoughBytesToRead = "There are not enough bytes remaining in the accessor to read at this position.";
public static string Argument_NotEnoughBytesToWrite = "There are not enough bytes remaining in the accessor to write at this position.";
public static string Argument_OffsetAndLengthOutOfBounds = "Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection.";
public static string Argument_OffsetAndCapacityOutOfBounds = "Offset and capacity were greater than the size of the view.";
public static string Argument_UnmanagedMemAccessorWrapAround = "The UnmanagedMemoryAccessor capacity and offset would wrap around the high end of the address space.";
public static string Argument_UnrecognizedLoaderOptimization = "Unrecognized LOADER_OPTIMIZATION property value.  Supported values may include \"SingleDomain\", \"MultiDomain\", \"MultiDomainHost\", and \"NotSpecified\".";
public static string ArgumentException_NotAllCustomSortingFuncsDefined = "Implementations of all the NLS functions must be provided.";
public static string ArgumentException_MinSortingVersion = "The runtime does not support a version of \"{0}\" less than {1}.";

//;
//; =====================================================
//; Reflection Emit resource strings
public static string Arugment_EmitMixedContext1 = "Type '{0}' was loaded in the ReflectionOnly context but the AssemblyBuilder was not created as AssemblyBuilderAccess.ReflectionOnly.";
public static string Arugment_EmitMixedContext2 = "Type '{0}' was not loaded in the ReflectionOnly context but the AssemblyBuilder was created as AssemblyBuilderAccess.ReflectionOnly.";
public static string Argument_BadSizeForData = "Data size must be > 0 and < 0x3f0000";
public static string Argument_InvalidLabel = "Invalid Label.";
public static string Argument_RedefinedLabel = "Label multiply defined.";
public static string Argument_UnclosedExceptionBlock = "The IL Generator cannot be used while there are unclosed exceptions.";
public static string Argument_MissingDefaultConstructor = "was missing default constructor.";
public static string Argument_TooManyFinallyClause = "Exception blocks may have at most one finally clause.";
public static string Argument_NotInTheSameModuleBuilder = "The argument passed in was not from the same ModuleBuilder.";
public static string Argument_BadCurrentLocalVariable = "Bad current local variable for setting symbol information.";
public static string Argument_DuplicateModuleName = "Duplicate dynamic module name within an assembly.";
public static string Argument_DuplicateTypeName = "Duplicate type name within an assembly.";
public static string Argument_InvalidAssemblyName = "Assembly names may not begin with whitespace or contain the characters '/', or '\\' or ':'.";
public static string Argument_InvalidGenericInstantiation = "The given generic instantiation was invalid.";
public static string Argument_DuplicatedFileName = "Duplicate file names.";
public static string Argument_GlobalFunctionHasToBeStatic = "Global members must be static.";
public static string Argument_BadPInvokeOnInterface = "PInvoke methods cannot exist on interfaces.";
public static string Argument_BadPInvokeMethod = "PInvoke methods must be static and native and cannot be abstract.";
public static string Argument_MethodRedefined = "Method has been already defined.";
public static string Argument_BadTypeAttrAbstractNFinal = "Bad type attributes. A type cannot be both abstract and final.";
public static string Argument_BadTypeAttrNestedVisibilityOnNonNestedType = "Bad type attributes. Nested visibility flag set on a non-nested type.";
public static string Argument_BadTypeAttrNonNestedVisibilityNestedType = "Bad type attributes. Non-nested visibility flag set on a nested type.";
public static string Argument_BadTypeAttrInvalidLayout = "Bad type attributes. Invalid layout attribute specified.";
public static string Argument_BadTypeAttrReservedBitsSet = "Bad type attributes. Reserved bits set on the type.";
public static string Argument_BadFieldSig = "Field signatures do not have return types.";
public static string Argument_ShouldOnlySetVisibilityFlags = "Should only set visibility flags when creating EnumBuilder.";
public static string Argument_BadNestedTypeFlags = "Visibility of interfaces must be one of the following: NestedAssembly, NestedFamANDAssem, NestedFamily, NestedFamORAssem, NestedPrivate or NestedPublic.";
public static string Argument_ShouldNotSpecifyExceptionType = "Should not specify exception type for catch clause for filter block.";
public static string Argument_BadLabel = "Bad label in ILGenerator.";
public static string Argument_BadLabelContent = "Bad label content in ILGenerator.";
public static string Argument_EmitWriteLineType = "EmitWriteLine does not support this field or local type.";
public static string Argument_ConstantNull = "Null is not a valid constant value for this type.";
public static string Argument_ConstantDoesntMatch = "Constant does not match the defined type.";
public static string Argument_ConstantNotSupported = "{0} is not a supported constant type.";
public static string Argument_BadConstructor = "Cannot have private or static constructor.";
public static string Argument_BadConstructorCallConv = "Constructor must have standard calling convention.";
public static string Argument_BadPropertyForConstructorBuilder = "Property must be on the same type of the given ConstructorInfo.";
public static string Argument_NotAWritableProperty = "Not a writable property.";
public static string Argument_BadFieldForConstructorBuilder = "Field must be on the same type of the given ConstructorInfo.";
public static string Argument_BadAttributeOnInterfaceMethod = "Interface method must be abstract and virtual.";
public static string ArgumentException_BadMethodImplBody = "MethodOverride's body must be from this type.";
public static string Argument_BadParameterCountsForConstructor = "Parameter count does not match passed in argument value count.";
public static string Argument_BadParameterTypeForConstructor = "Passed in argument value at index {0} does not match the parameter type.";
public static string Argument_BadTypeInCustomAttribute = "An invalid type was used as a custom attribute constructor argument, field or property.";
public static string Argument_DateTimeBadBinaryData = "The binary data must result in a DateTime with ticks between DateTime.MinValue.Ticks and DateTime.MaxValue.Ticks.";
public static string Argument_VerStringTooLong = "The unmanaged Version information is too large to persist.";
public static string Argument_UnknownUnmanagedCallConv = "Unknown unmanaged calling convention for function signature.";
public static string Argument_BadConstantValue = "Bad default value.";
public static string Argument_IllegalName = "Illegal name.";
public static string Argument_cvtres_NotFound = "Cannot find cvtres.exe";
public static string Argument_BadCAForUnmngRSC = "Bad '{0}' while generating unmanaged resource information.";
public static string Argument_MustBeInterfaceMethod = "The MemberInfo must be an interface method.";
public static string Argument_CORDBBadVarArgCallConv = "Cannot evaluate a VarArgs function.";
public static string Argument_CORDBBadMethod = "Cannot find the method on the object instance.";
public static string Argument_InvalidOpCodeOnDynamicMethod = "Ldtoken, Ldftn and Ldvirtftn OpCodes cannot target DynamicMethods.";
public static string Argument_InvalidTypeForDynamicMethod = "Invalid type owner for DynamicMethod.";
public static string Argument_NeedGenericMethodDefinition = "Method must represent a generic method definition on a generic type definition.";
public static string Argument_MethodNeedGenericDeclaringType = "The specified method cannot be dynamic or global and must be declared on a generic type definition.";
public static string Argument_ConstructorNeedGenericDeclaringType = "The specified constructor must be declared on a generic type definition.";
public static string Argument_FieldNeedGenericDeclaringType = "The specified field must be declared on a generic type definition.";
public static string Argument_InvalidMethodDeclaringType = "The specified method must be declared on the generic type definition of the specified type.";
public static string Argument_InvalidConstructorDeclaringType = "The specified constructor must be declared on the generic type definition of the specified type.";
public static string Argument_InvalidFieldDeclaringType = "The specified field must be declared on the generic type definition of the specified type.";
public static string Argument_NeedNonGenericType = "The specified Type must not be a generic type definition.";
public static string Argument_MustBeTypeBuilder = "'type' must contain a TypeBuilder as a generic argument.";
public static string Argument_CannotSetParentToInterface = "Cannot set parent to an interface.";
public static string Argument_MismatchedArrays = "Two arrays, {0} and {1}, must be of  the same size.";
public static string Argument_NeedNonGenericObject = "The specified object must not be an instance of a generic type.";
public static string Argument_NeedStructWithNoRefs = "The specified Type must be a struct containing no references.";
public static string Argument_NotMethodCallOpcode = "The specified opcode cannot be passed to EmitCall.";

//; =====================================================
//;
public static string Argument_ModuleAlreadyLoaded = "The specified module has already been loaded.";
public static string Argument_MustHaveLayoutOrBeBlittable = "The specified structure must be blittable or have layout information.";
public static string Argument_NotSimpleFileName = "The filename must not include a path specification.";
public static string Argument_TypeMustBeVisibleFromCom = "The specified type must be visible from COM.";
public static string Argument_TypeMustBeComCreatable = "The type must be creatable from COM.";
public static string Argument_TypeMustNotBeComImport = "The type must not be imported from COM.";
public static string Argument_PolicyFileDoesNotExist = "The requested policy file does not exist.";
public static string Argument_NonNullObjAndCtx = "Either obj or ctx must be null.";
public static string Argument_NoModuleFileExtension = "Module file name '{0}' must have file extension.";
public static string Argument_TypeDoesNotContainMethod = "Type does not contain the given method.";
public static string Argument_StringZeroLength = "String cannot be of zero length.";
public static string Argument_MustBeString = "String is too long or has invalid contents.";
public static string Argument_AbsolutePathRequired = "Absolute path information is required.";
public static string Argument_ManifestFileDoesNotExist = "The specified manifest file does not exist.";
public static string Argument_MustBeRuntimeType = "Type must be a runtime Type object.";
public static string Argument_TypeNotValid = "The Type object is not valid.";
public static string Argument_MustBeRuntimeMethodInfo = "MethodInfo must be a runtime MethodInfo object.";
public static string Argument_MustBeRuntimeFieldInfo = "FieldInfo must be a runtime FieldInfo object.";
public static string Argument_InvalidFieldInfo = "The FieldInfo object is not valid.";
public static string Argument_InvalidConstructorInfo = "The ConstructorInfo object is not valid.";
public static string Argument_MustBeRuntimeAssembly = "Assembly must be a runtime Assembly object.";
public static string Argument_MustBeRuntimeModule = "Module must be a runtime Module object.";
public static string Argument_MustBeRuntimeParameterInfo = "ParameterInfo must be a runtime ParameterInfo object.";
public static string Argument_InvalidParameterInfo = "The ParameterInfo object is not valid.";
public static string Argument_MustBeRuntimeReflectionObject = "The object must be a runtime Reflection object.";
public static string Argument_InvalidMarshalByRefObject = "The MarshalByRefObject is not valid.";
public static string Argument_TypedReferenceInvalidField = "Field in TypedReferences cannot be static or init only.";
public static string Argument_HandleLeak = "Cannot pass a GCHandle across AppDomains.";
public static string Argument_ArgumentZero = "Argument cannot be zero.";
public static string Argument_ImproperType = "Improper types in collection.";
public static string Argument_NotAMembershipCondition = "The type does not implement IMembershipCondition";
public static string Argument_NotAPermissionType = "The type does not implement IPermission";
public static string Argument_NotACodeGroupType = "The type does not inherit from CodeGroup";
public static string Argument_NotATP = "Type must be a TransparentProxy";
public static string Argument_AlreadyACCW = "The object already has a CCW associated with it.";
public static string Argument_OffsetLocalMismatch = "The UTC Offset of the local dateTime parameter does not match the offset argument.";
public static string Argument_OffsetUtcMismatch = "The UTC Offset for Utc DateTime instances must be 0.";
public static string Argument_UTCOutOfRange = "The UTC time represented when the offset is applied must be between year 0 and 10,000.";
public static string Argument_OffsetOutOfRange = "Offset must be within plus or minus 14 hours.";
public static string Argument_OffsetPrecision = "Offset must be specified in whole minutes.";
public static string Argument_FlagNotSupported = "One or more flags are not supported.";
public static string Argument_MustBeFalse = "Argument must be initialized to false";
public static string Argument_ToExclusiveLessThanFromExclusive = "fromInclusive must be less than or equal to toExclusive.";
public static string Argument_FrameworkNameTooShort = "FrameworkName cannot have less than two components or more than three components.";
public static string Argument_FrameworkNameInvalid  = "FrameworkName is invalid.";
public static string Argument_FrameworkNameMissingVersion = "FrameworkName version component is missing.";
//#if FEATURE_COMINTEROP
public static string Argument_TypeNotActivatableViaWindowsRuntime = "Type '{0}' does not have an activation factory because it is not activatable by Windows Runtime.";
public static string Argument_WinRTSystemRuntimeType = "Cannot marshal type '{0}' to Windows Runtime. Only 'System.RuntimeType' is supported.";
public static string Argument_Unexpected_TypeSource = "Unexpected TypeKind when marshaling Windows.Foundation.TypeName. ";
//#endif // FEATURE_COMINTEROP

//; ArgumentNullException
public static string ArgumentNull_Array = "Array cannot be null.";
public static string ArgumentNull_ArrayValue = "Found a null value within an array.";
public static string ArgumentNull_ArrayElement = "At least one element in the specified array was null.";
public static string ArgumentNull_Assembly = "Assembly cannot be null.";
public static string ArgumentNull_AssemblyName = "AssemblyName cannot be null.";
public static string ArgumentNull_AssemblyNameName = "AssemblyName.Name cannot be null or an empty string.";
public static string ArgumentNull_Buffer = "Buffer cannot be null.";
public static string ArgumentNull_Collection = "Collection cannot be null.";
public static string ArgumentNull_CultureInfo = "CultureInfo cannot be null.";
public static string ArgumentNull_Dictionary = "Dictionary cannot be null.";
public static string ArgumentNull_FileName = "File name cannot be null.";
public static string ArgumentNull_Key = "Key cannot be null.";
public static string ArgumentNull_Graph = "Object Graph cannot be null.";
public static string ArgumentNull_Path = "Path cannot be null.";
public static string ArgumentNull_Stream = "Stream cannot be null.";
public static string ArgumentNull_String = "String reference not set to an instance of a String.";
public static string ArgumentNull_Type = "Type cannot be null.";
public static string ArgumentNull_Obj = "Object cannot be null.";
public static string ArgumentNull_GUID = "GUID cannot be null.";
public static string ArgumentNull_NullMember = "Member at position {0} was null.";
public static string ArgumentNull_Generic = "Value cannot be null.";
public static string ArgumentNull_WithParamName = "Parameter '{0}' cannot be null.";
public static string ArgumentNull_Child = "Cannot have a null child.";
public static string ArgumentNull_SafeHandle = "SafeHandle cannot be null.";
public static string ArgumentNull_CriticalHandle = "CriticalHandle cannot be null.";
public static string ArgumentNull_TypedRefType = "Type in TypedReference cannot be null.";
public static string ArgumentNull_ApplicationTrust = "The application trust cannot be null.";
public static string ArgumentNull_TypeRequiredByResourceScope = "The type parameter cannot be null when scoping the resource's visibility to Private or Assembly.";
public static string ArgumentNull_Waithandles = "The waitHandles parameter cannot be null.";

//; ArgumentOutOfRangeException
public static string ArgumentOutOfRange_AddressSpace = "The number of bytes cannot exceed the virtual address space on a 32 bit machine.";
public static string ArgumentOutOfRange_ArrayLB = "Number was less than the array's lower bound in the first dimension.";
public static string ArgumentOutOfRange_ArrayLBAndLength = "Higher indices will exceed Int32.MaxValue because of large lower bound and/or length.";
public static string ArgumentOutOfRange_ArrayLength = "The length of the array must be between {0} and {1}, inclusive.";
public static string ArgumentOutOfRange_ArrayLengthMultiple = "The length of the array must be a multiple of {0}.";
public static string ArgumentOutOfRange_ArrayListInsert = "Insertion index was out of range. Must be non-negative and less than or equal to size.";
public static string ArgumentOutOfRange_ArrayTooSmall = "Destination array is not long enough to copy all the required data. Check array length and offset.";
public static string ArgumentOutOfRange_BeepFrequency = "Console.Beep's frequency must be between {0} and {1}.";
public static string ArgumentOutOfRange_BiggerThanCollection = "Larger than collection size.";
public static string ArgumentOutOfRange_Bounds_Lower_Upper = "Argument must be between {0} and {1}.";
public static string ArgumentOutOfRange_Count = "Count must be positive and count must refer to a location within the string/array/collection.";
public static string ArgumentOutOfRange_CalendarRange = "Specified time is not supported in this calendar. It should be between {0} (Gregorian date) and {1} (Gregorian date), inclusive.";
public static string ArgumentOutOfRange_ConsoleBufferBoundaries = "The value must be greater than or equal to zero and less than the console's buffer size in that dimension.";
public static string ArgumentOutOfRange_ConsoleBufferLessThanWindowSize = "The console buffer size must not be less than the current size and position of the console window, nor greater than or equal to Int16.MaxValue.";
public static string ArgumentOutOfRange_ConsoleWindowBufferSize = "The new console window size would force the console buffer size to be too large.";
public static string ArgumentOutOfRange_ConsoleTitleTooLong = "The console title is too long.";
public static string ArgumentOutOfRange_ConsoleWindowPos = "The window position must be set such that the current window size fits within the console's buffer, and the numbers must not be negative.";
public static string ArgumentOutOfRange_ConsoleWindowSize_Size = "The value must be less than the console's current maximum window size of {0} in that dimension. Note that this value depends on screen resolution and the console font.";
public static string ArgumentOutOfRange_ConsoleKey = "Console key values must be between 0 and 255.";
public static string ArgumentOutOfRange_CursorSize = "The cursor size is invalid. It must be a percentage between 1 and 100.";
public static string ArgumentOutOfRange_BadYearMonthDay = "Year, Month, and Day parameters describe an un-representable DateTime.";
public static string ArgumentOutOfRange_BadHourMinuteSecond = "Hour, Minute, and Second parameters describe an un-representable DateTime.";
public static string ArgumentOutOfRange_DateArithmetic = "The added or subtracted value results in an un-representable DateTime.";
public static string ArgumentOutOfRange_DateTimeBadMonths = "Months value must be between +/-120000.";
public static string ArgumentOutOfRange_DateTimeBadYears = "Years value must be between +/-10000.";
public static string ArgumentOutOfRange_DateTimeBadTicks = "Ticks must be between DateTime.MinValue.Ticks and DateTime.MaxValue.Ticks.";
public static string ArgumentOutOfRange_Day = "Day must be between 1 and {0} for month {1}.";
public static string ArgumentOutOfRange_DecimalRound = "Decimal can only round to between 0 and 28 digits of precision.";
public static string ArgumentOutOfRange_DecimalScale = "Decimal's scale value must be between 0 and 28, inclusive.";
public static string ArgumentOutOfRange_Era = "Time value was out of era range.";
public static string ArgumentOutOfRange_Enum = "Enum value was out of legal range.";
public static string ArgumentOutOfRange_FileLengthTooBig = "Specified file length was too large for the file system.";
public static string ArgumentOutOfRange_FileTimeInvalid = "Not a valid Win32 FileTime.";
public static string ArgumentOutOfRange_GetByteCountOverflow = "Too many characters. The resulting number of bytes is larger than what can be returned as an int.";
public static string ArgumentOutOfRange_GetCharCountOverflow = "Too many bytes. The resulting number of chars is larger than what can be returned as an int.";
public static string ArgumentOutOfRange_HashtableLoadFactor = "Load factor needs to be between 0.1 and 1.0.";
public static string ArgumentOutOfRange_HugeArrayNotSupported = "Arrays larger than 2GB are not supported.";
public static string ArgumentOutOfRange_InvalidHighSurrogate = "A valid high surrogate character is between 0xd800 and 0xdbff, inclusive.";
public static string ArgumentOutOfRange_InvalidLowSurrogate = "A valid low surrogate character is between 0xdc00 and 0xdfff, inclusive.";
public static string ArgumentOutOfRange_InvalidEraValue = "Era value was not valid.";
public static string ArgumentOutOfRange_InvalidUserDefinedAceType = "User-defined ACEs must not have a well-known ACE type.";
public static string ArgumentOutOfRange_InvalidUTF32 = "A valid UTF32 value is between 0x000000 and 0x10ffff, inclusive, and should not include surrogate codepoint values (0x00d800 ~ 0x00dfff).";
public static string ArgumentOutOfRange_Index = "Index was out of range. Must be non-negative and less than the size of the collection.";
public static string ArgumentOutOfRange_IndexString = "Index was out of range. Must be non-negative and less than the length of the string.";
public static string ArgumentOutOfRange_StreamLength = "Stream length must be non-negative and less than 2^31 - 1 - origin.";
public static string ArgumentOutOfRange_LessEqualToIntegerMaxVal = "Argument must be less than or equal to 2^31 - 1 milliseconds.";
public static string ArgumentOutOfRange_Month = "Month must be between one and twelve.";
public static string ArgumentOutOfRange_MustBeNonNegInt32 = "Value must be non-negative and less than or equal to Int32.MaxValue.";
public static string ArgumentOutOfRange_NeedNonNegNum = "Non-negative number required.";
public static string ArgumentOutOfRange_NeedNonNegOrNegative1 = "Number must be either non-negative and less than or equal to Int32.MaxValue or -1.";
public static string ArgumentOutOfRange_NeedPosNum = "Positive number required.";
public static string ArgumentOutOfRange_NegativeCapacity = "Capacity must be positive.";
public static string ArgumentOutOfRange_NegativeCount = "Count cannot be less than zero.";
public static string ArgumentOutOfRange_NegativeLength = "Length cannot be less than zero.";
public static string ArgumentOutOfRange_NegFileSize = "Length must be non-negative.";
public static string ArgumentOutOfRange_ObjectID = "objectID cannot be less than or equal to zero.";
public static string ArgumentOutOfRange_SmallCapacity = "capacity was less than the current size.";
public static string ArgumentOutOfRange_QueueGrowFactor = "Queue grow factor must be between {0} and {1}.";
public static string ArgumentOutOfRange_RoundingDigits = "Rounding digits must be between 0 and 15, inclusive.";
public static string ArgumentOutOfRange_StartIndex = "StartIndex cannot be less than zero.";
public static string ArgumentOutOfRange_MustBePositive = "'{0}' must be greater than zero.";
public static string ArgumentOutOfRange_MustBeNonNegNum = "'{0}' must be non-negative.";
public static string ArgumentOutOfRange_LengthGreaterThanCapacity = "The length cannot be greater than the capacity.";
public static string ArgumentOutOfRange_ListInsert = "Index must be within the bounds of the List.";
public static string ArgumentOutOfRange_StartIndexLessThanLength = "startIndex must be less than length of string.";
public static string ArgumentOutOfRange_StartIndexLargerThanLength = "startIndex cannot be larger than length of string.";
public static string ArgumentOutOfRange_EndIndexStartIndex = "endIndex cannot be greater than startIndex.";
public static string ArgumentOutOfRange_IndexCount = "Index and count must refer to a location within the string.";
public static string ArgumentOutOfRange_IndexCountBuffer = "Index and count must refer to a location within the buffer.";
public static string ArgumentOutOfRange_IndexLength = "Index and length must refer to a location within the string.";
public static string ArgumentOutOfRange_InvalidThreshold = "The specified threshold for creating dictionary is out of range.";
public static string ArgumentOutOfRange_Capacity = "Capacity exceeds maximum capacity.";
public static string ArgumentOutOfRange_Length = "The specified length exceeds maximum capacity of SecureString.";
public static string ArgumentOutOfRange_LengthTooLarge = "The specified length exceeds the maximum value of {0}.";
public static string ArgumentOutOfRange_SmallMaxCapacity = "MaxCapacity must be one or greater.";
public static string ArgumentOutOfRange_GenericPositive = "Value must be positive.";
public static string ArgumentOutOfRange_Range = "Valid values are between {0} and {1}, inclusive.";
public static string ArgumentOutOfRange_AddValue = "Value to add was out of range.";
public static string ArgumentOutOfRange_OffsetLength = "Offset and length must refer to a position in the string.";
public static string ArgumentOutOfRange_OffsetOut = "Either offset did not refer to a position in the string, or there is an insufficient length of destination character array.";
public static string ArgumentOutOfRange_PartialWCHAR = "Pointer startIndex and length do not refer to a valid string.";
public static string ArgumentOutOfRange_ParamSequence = "The specified parameter index is not in range.";
public static string ArgumentOutOfRange_Version = "Version's parameters must be greater than or equal to zero.";
public static string ArgumentOutOfRange_TimeoutTooLarge = "Time-out interval must be less than 2^32-2.";
public static string ArgumentOutOfRange_UIntPtrMax_1 = "The length of the buffer must be less than the maximum UIntPtr value for your platform.";
public static string ArgumentOutOfRange_UnmanagedMemStreamLength = "UnmanagedMemoryStream length must be non-negative and less than 2^63 - 1 - baseAddress.";
public static string ArgumentOutOfRange_UnmanagedMemStreamWrapAround = "The UnmanagedMemoryStream capacity would wrap around the high end of the address space.";
public static string ArgumentOutOfRange_PeriodTooLarge = "Period must be less than 2^32-2.";
public static string ArgumentOutOfRange_Year = "Year must be between 1 and 9999.";
public static string ArgumentOutOfRange_BinaryReaderFillBuffer = "The number of bytes requested does not fit into BinaryReader's internal buffer.";
public static string ArgumentOutOfRange_PositionLessThanCapacityRequired = "The position may not be greater or equal to the capacity of the accessor.";

//; ArithmeticException
public static string Arithmetic_NaN = "Function does not accept floating point Not-a-Number values.";

//; ArrayTypeMismatchException
public static string ArrayTypeMismatch_CantAssignType = "Source array type cannot be assigned to destination array type.";
public static string ArrayTypeMismatch_ConstrainedCopy = "Array.ConstrainedCopy will only work on array types that are provably compatible, without any form of boxing, unboxing, widening, or casting of each array element.  Change the array types (i.e., copy a Derived[] to a Base[]), or use a mitigation strategy in the CER for Array.Copy's less powerful reliability contract, such as cloning the array or throwing away the potentially corrupt destination array.";

//; BadImageFormatException
public static string BadImageFormat_ResType_SerBlobMismatch = "The type serialized in the .resources file was not the same type that the .resources file said it contained. Expected '{0}' but read '{1}'.";
public static string BadImageFormat_ResourcesIndexTooLong = "Corrupt .resources file. String for name index '{0}' extends past the end of the file.";
public static string BadImageFormat_ResourcesNameTooLong = "Corrupt .resources file. Resource name extends past the end of the file.";
public static string BadImageFormat_ResourcesNameInvalidOffset = "Corrupt .resources file. Invalid offset '{0}' into name section.";
public static string BadImageFormat_ResourcesHeaderCorrupted = "Corrupt .resources file. Unable to read resources from this file because of invalid header information. Try regenerating the .resources file.";
public static string BadImageFormat_ResourceNameCorrupted = "Corrupt .resources file. A resource name extends past the end of the stream.";
public static string BadImageFormat_ResourceNameCorrupted_NameIndex = "Corrupt .resources file. The resource name for name index {0} extends past the end of the stream.";
public static string BadImageFormat_ResourceDataLengthInvalid = "Corrupt .resources file.  The specified data length '{0}' is not a valid position in the stream.";
public static string BadImageFormat_TypeMismatch = "Corrupt .resources file.  The specified type doesn't match the available data in the stream.";
public static string BadImageFormat_InvalidType = "Corrupt .resources file.  The specified type doesn't exist.";
public static string BadImageFormat_ResourcesIndexInvalid = "Corrupt .resources file. The resource index '{0}' is outside the valid range.";
public static string BadImageFormat_StreamPositionInvalid = "Corrupt .resources file.  The specified position '{0}' is not a valid position in the stream.";
public static string BadImageFormat_ResourcesDataInvalidOffset = "Corrupt .resources file. Invalid offset '{0}' into data section.";
public static string BadImageFormat_NegativeStringLength = "Corrupt .resources file. String length must be non-negative.";
public static string BadImageFormat_ParameterSignatureMismatch = "The parameters and the signature of the method don't match.";

//; Cryptography
//; These strings still appear in bcl.small but should go away eventually
public static string Cryptography_CSSM_Error = "Error 0x{0} from the operating system security framework: '{1}'.";
public static string Cryptography_CSSM_Error_Unknown = "Error 0x{0} from the operating system security framework.";
public static string Cryptography_InvalidDSASignatureSize = "Length of the DSA signature was not 40 bytes.";
public static string Cryptography_InvalidHandle = "{0} is an invalid handle.";
public static string Cryptography_InvalidOID = "Object identifier (OID) is unknown.";
public static string Cryptography_OAEPDecoding = "Error occurred while decoding OAEP padding.";
public static string Cryptography_PasswordDerivedBytes_InvalidIV = "The Initialization vector should have the same length as the algorithm block size in bytes.";
public static string Cryptography_SSE_InvalidDataSize = "Length of the data to encrypt is invalid.";
public static string Cryptography_X509_ExportFailed = "The certificate export operation failed.";
public static string Cryptography_X509_InvalidContentType = "Invalid content type.";
public static string Cryptography_CryptoStream_FlushFinalBlockTwice = "FlushFinalBlock() method was called twice on a CryptoStream. It can only be called once.";
public static string Cryptography_HashKeySet = "Hash key cannot be changed after the first write to the stream.";
public static string Cryptography_HashNotYetFinalized = "Hash must be finalized before the hash value is retrieved.";
public static string Cryptography_InsufficientBuffer = "Input buffer contains insufficient data.";
public static string Cryptography_InvalidBlockSize = "Specified block size is not valid for this algorithm.";
public static string Cryptography_InvalidCipherMode = "Specified cipher mode is not valid for this algorithm.";
public static string Cryptography_InvalidIVSize = "Specified initialization vector (IV) does not match the block size for this algorithm.";
public static string Cryptography_InvalidKeySize = "Specified key is not a valid size for this algorithm.";
public static string Cryptography_PasswordDerivedBytes_FewBytesSalt = "Salt is not at least eight bytes.";
public static string Cryptography_PKCS7_InvalidPadding = "Padding is invalid and cannot be removed.";
public static string Cryptography_UnknownHashAlgorithm = "'{0}' is not a known hash algorithm.";
public static string Cryptography_LegacyNetCF_UnknownError = "Unknown Error '{0}'.";
public static string Cryptography_LegacyNetCF_CSP_CouldNotAcquire = "CryptoAPI cryptographic service provider (CSP) for this implementation could not be acquired.";

//#if FEATURE_CRYPTO
public static string Cryptography_Config_EncodedOIDError = "Encoded OID length is too large (greater than 0x7f bytes).";
public static string Cryptography_CSP_AlgKeySizeNotAvailable = "Algorithm implementation does not support a key size of {0}.";
public static string Cryptography_CSP_AlgorithmNotAvailable = "Cryptographic service provider (CSP) could not be found for this algorithm.";
public static string Cryptography_CSP_CFBSizeNotSupported = "Feedback size for the cipher feedback mode (CFB) must be 8 bits.";
public static string Cryptography_CSP_NotFound = "The requested key container was not found.";
public static string Cryptography_CSP_NoPrivateKey = "Object contains only the public half of a key pair. A private key must also be provided.";
public static string Cryptography_CSP_OFBNotSupported = "Output feedback mode (OFB) is not supported by this implementation.";
public static string Cryptography_CSP_WrongKeySpec = "The specified cryptographic service provider (CSP) does not support this key algorithm.";
public static string Cryptography_HashNameSet = "Hash name cannot be changed after the first write to the stream.";
public static string Cryptography_HashAlgorithmNameNullOrEmpty = "The hash algorithm name cannot be null or empty.";
public static string Cryptography_InvalidHashSize = "{0} algorithm hash size is {1} bytes.";
public static string Cryptography_InvalidKey_Weak = "Specified key is a known weak key for '{0}' and cannot be used.";
public static string Cryptography_InvalidKey_SemiWeak = "Specified key is a known semi-weak key for '{0}' and cannot be used.";
public static string Cryptography_InvalidKeyParameter = "Parameter '{0}' is not a valid key parameter.";
public static string Cryptography_InvalidFeedbackSize = "Specified feedback size is invalid.";
public static string Cryptography_InvalidOperation = "This operation is not supported for this class.";
public static string Cryptography_InvalidPaddingMode = "Specified padding mode is not valid for this algorithm.";
public static string Cryptography_InvalidFromXmlString = "Input string does not contain a valid encoding of the '{0}' '{1}' parameter.";
public static string Cryptography_MissingKey = "No asymmetric key object has been associated with this formatter object.";
public static string Cryptography_MissingOID = "Required object identifier (OID) cannot be found.";
public static string Cryptography_NotInteractive = "The current session is not interactive.";
public static string Cryptography_NonCompliantFIPSAlgorithm = "This implementation is not part of the Windows Platform FIPS validated cryptographic algorithms.";
public static string Cryptography_Padding_Win2KEnhOnly = "Direct Encryption and decryption using RSA are not available on this platform.";
public static string Cryptography_Padding_EncDataTooBig = "The data to be encrypted exceeds the maximum for this modulus of {0} bytes.";
public static string Cryptography_Padding_DecDataTooBig = "The data to be decrypted exceeds the maximum for this modulus of {0} bytes.";
public static string Cryptography_PasswordDerivedBytes_ValuesFixed = "Value of '{0}' cannot be changed after the bytes have been retrieved.";
public static string Cryptography_PasswordDerivedBytes_TooManyBytes = "Requested number of bytes exceeds the maximum.";
public static string Cryptography_PasswordDerivedBytes_InvalidAlgorithm = "Algorithm is unavailable or is not supported for this operation.";
public static string Cryptography_PKCS1Decoding = "Error occurred while decoding PKCS1 padding.";
public static string Cryptography_RC2_EKSKS = "EffectiveKeySize value must be at least as large as the KeySize value.";
public static string Cryptography_RC2_EKSKS2 = "EffectiveKeySize must be the same as KeySize in this implementation.";
public static string Cryptography_RC2_EKS40 = "EffectiveKeySize value must be at least 40 bits.";
public static string Cryptography_SSD_InvalidDataSize = "Length of the data to decrypt is invalid.";
public static string Cryptography_AddNullOrEmptyName = "CryptoConfig cannot add a mapping for a null or empty name.";
public static string Cryptography_AlgorithmTypesMustBeVisible = "Algorithms added to CryptoConfig must be accessable from outside their assembly.";
//#endif  // FEATURE_CRYPTO

//; EventSource
public static string EventSource_ToString = "EventSource({0}, {1})";
public static string EventSource_EventSourceGuidInUse = "An instance of EventSource with Guid {0} already exists.";
public static string EventSource_KeywordNeedPowerOfTwo = "Value {0} for keyword {1} needs to be a power of 2.";
public static string EventSource_UndefinedKeyword = "Use of undefined keyword value {0} for event {1}.";
public static string EventSource_UnsupportedEventTypeInManifest = "Unsupported type {0} in event source.";
public static string EventSource_ListenerNotFound = "Listener not found.";
public static string EventSource_ListenerCreatedInsideCallback = "Creating an EventListener inside a EventListener callback.";
public static string EventSource_AttributeOnNonVoid = "Event attribute placed on method {0} which does not return 'void'.";
public static string EventSource_NeedPositiveId = "Event IDs must be positive integers.";
public static string EventSource_ReservedOpcode = "Opcode values less than 11 are reserved for system use.";
public static string EventSource_ReservedKeywords = "Keywords values larger than 0x0000100000000000 are reserved for system use";
public static string EventSource_PayloadTooBig = "The payload for a single event is too large.";
public static string EventSource_NoFreeBuffers = "No Free Buffers available from the operating system (e.g. event rate too fast).";
public static string EventSource_NullInput = "Null passed as a event argument.";
public static string EventSource_TooManyArgs = "Too many arguments.";
public static string EventSource_SessionIdError = "Bit position in AllKeywords ({0}) must equal the command argument named \"EtwSessionKeyword\" ({1}).";
public static string EventSource_EnumKindMismatch = "The type of {0} is not expected in {1}.";
public static string EventSource_MismatchIdToWriteEvent = "Event {0} is givien event ID {1} but {2} was passed to WriteEvent.";
public static string EventSource_EventIdReused = "Event {0} has ID {1} which is already in use.";
public static string EventSource_EventNameReused = "Event name {0} used more than once.  If you wish to overload a method, the overloaded method should have a NonEvent attribute.";
public static string EventSource_UndefinedChannel = "Use of undefined channel value {0} for event {1}.";
public static string EventSource_UndefinedOpcode = "Use of undefined opcode value {0} for event {1}.";
public static string ArgumentOutOfRange_MaxArgExceeded = "The total number of parameters must not exceed {0}.";
public static string ArgumentOutOfRange_MaxStringsExceeded = "The number of String parameters must not exceed {0}.";
public static string ArgumentOutOfRange_NeedValidId = "The ID parameter must be in the range {0} through {1}.";
public static string EventSource_NeedGuid = "The Guid of an EventSource must be non zero.";
public static string EventSource_NeedName = "The name of an EventSource must not be null.";
public static string EventSource_EtwAlreadyRegistered = "The provider has already been registered with the operating system.";
public static string EventSource_ListenerWriteFailure = "An error occurred when writing to a listener.";
public static string EventSource_TypeMustDeriveFromEventSource = "Event source types must derive from EventSource.";
public static string EventSource_TypeMustBeSealedOrAbstract = "Event source types must be sealed or abstract.";
public static string EventSource_TaskOpcodePairReused = "Event {0} (with ID {1}) has the same task/opcode pair as event {2} (with ID {3}).";
public static string EventSource_EventMustHaveTaskIfNonDefaultOpcode = "Event {0} (with ID {1}) has a non-default opcode but not a task.";
public static string EventSource_EventNameDoesNotEqualTaskPlusOpcode = "Event {0} (with ID {1}) has a name that is not the concatenation of its task name and opcode.";
public static string EventSource_PeriodIllegalInProviderName = "Period character ('.') is illegal in an ETW provider name ({0}).";
public static string EventSource_IllegalOpcodeValue = "Opcode {0} has a value of {1} which is outside the legal range (11-238).";
public static string EventSource_OpcodeCollision = "Opcodes {0} and {1} are defined with the same value ({2}).";
public static string EventSource_IllegalTaskValue = "Task {0} has a value of {1} which is outside the legal range (1-65535).";
public static string EventSource_TaskCollision = "Tasks {0} and {1} are defined with the same value ({2}).";
public static string EventSource_IllegalKeywordsValue = "Keyword {0} has a value of {1} which is outside the legal range (0-0x0000080000000000).";
public static string EventSource_KeywordCollision = "Keywords {0} and {1} are defined with the same value ({2}).";
public static string EventSource_EventChannelOutOfRange = "Channel {0} has a value of {1} which is outside the legal range (16-254).";
public static string EventSource_ChannelTypeDoesNotMatchEventChannelValue = "Channel {0} does not match event channel value {1}.";
public static string EventSource_MaxChannelExceeded = "Attempt to define more than the maximum limit of 8 channels for a provider.";
public static string EventSource_DuplicateStringKey = "Multiple definitions for string \"{0}\".";
public static string EventSource_EventWithAdminChannelMustHaveMessage = "Event {0} specifies an Admin channel {1}. It must specify a Message property.";
public static string EventSource_UnsupportedMessageProperty = "Event {0} specifies an illegal or unsupported formatting message (\"{1}\").";
public static string EventSource_AbstractMustNotDeclareKTOC = "Abstract event source must not declare {0} nested type.";
public static string EventSource_AbstractMustNotDeclareEventMethods = "Abstract event source must not declare event methods ({0} with ID {1}).";
public static string EventSource_EventMustNotBeExplicitImplementation = "Event method {0} (with ID {1}) is an explicit interface method implementation. Re-write method as implicit implementation.";
public static string EventSource_EventParametersMismatch = "Event {0} was called with {1} argument(s) , but it is defined with {2} paramenter(s).";
public static string EventSource_InvalidCommand = "Invalid command value.";
public static string EventSource_InvalidEventFormat = "Can't specify both etw event format flags.";
public static string EventSource_AddScalarOutOfRange = "Getting out of bounds during scalar addition. ";
public static string EventSource_PinArrayOutOfRange = " Pins are out of range.";
public static string EventSource_DataDescriptorsOutOfRange = "Data descriptors are out of range.";
public static string EventSource_NotSupportedArrayOfNil = "Arrays of Nil are not supported.";
public static string EventSource_NotSupportedArrayOfBinary = "Arrays of Binary are not supported.";
public static string EventSource_NotSupportedArrayOfNullTerminatedString = "Arrays of null-terminated string are not supported.";
public static string EventSource_TooManyFields = "Too many fields in structure.";
public static string EventSource_RecursiveTypeDefinition = "Recursive type definition is not supported.";
public static string EventSource_NotSupportedEnumType = "Enum type {0} underlying type {1} is not supported for serialization.";
public static string EventSource_NonCompliantTypeError = "The API supports only anonymous types or types decorated with the EventDataAttribute. Non-compliant type: {0} dataType.";
public static string EventSource_NotSupportedNestedArraysEnums = "Nested arrays/enumerables are not supported.";
public static string EventSource_IncorrentlyAuthoredTypeInfo = "Incorrectly-authored TypeInfo - a type should be serialized as one field or as one group";
public static string EventSource_NotSupportedCustomSerializedData = "Enumerables of custom-serialized data are not supported";
public static string EventSource_StopsFollowStarts = "An event with stop suffix must follow a corresponding event with a start suffix.";
public static string EventSource_NoRelatedActivityId = "EventSource expects the first parameter of the Event method to be of type Guid and to be named \"relatedActivityId\" when calling WriteEventWithRelatedActivityId.";
public static string EventSource_VarArgsParameterMismatch = "The parameters to the Event method do not match the parameters to the WriteEvent method. This may cause the event to be displayed incorrectly.";

//; ExecutionEngineException
public static string ExecutionEngine_InvalidAttribute = "Attribute cannot have multiple definitions.";
public static string ExecutionEngine_MissingSecurityDescriptor = "Unable to retrieve security descriptor for this frame.";

//;;ExecutionContext
public static string ExecutionContext_UndoFailed = "Undo operation on a component context threw an exception";
public static string ExecutionContext_ExceptionInAsyncLocalNotification = "An exception was not handled in an AsyncLocal<T> notification callback.";


//; FieldAccessException
public static string FieldAccess_InitOnly = "InitOnly (aka ReadOnly) fields can only be initialized in the type/instance constructor.";

//; FormatException
public static string Format_AttributeUsage = "Duplicate AttributeUsageAttribute found on attribute type {0}.";
public static string Format_Bad7BitInt32 = "Too many bytes in what should have been a 7 bit encoded Int32.";
public static string Format_BadBase = "Invalid digits for the specified base.";
public static string Format_BadBase64Char = "The input is not a valid Base-64 string as it contains a non-base 64 character, more than two padding characters, or an illegal character among the padding characters. ";
public static string Format_BadBase64CharArrayLength = "Invalid length for a Base-64 char array or string.";
public static string Format_BadBoolean = "String was not recognized as a valid Boolean.";
public static string Format_BadDateTime = "String was not recognized as a valid DateTime.";
public static string Format_BadDateTimeCalendar = "The DateTime represented by the string is not supported in calendar {0}.";
public static string Format_BadDayOfWeek = "String was not recognized as a valid DateTime because the day of week was incorrect.";
public static string Format_DateOutOfRange = "The DateTime represented by the string is out of range.";
public static string Format_BadDatePattern = "Could not determine the order of year, month, and date from '{0}'.";
public static string Format_BadFormatSpecifier = "Format specifier was invalid.";
public static string Format_BadTimeSpan = "String was not recognized as a valid TimeSpan.";
public static string Format_BadQuote = "Cannot find a matching quote character for the character '{0}'.";
public static string Format_EmptyInputString = "Input string was either empty or contained only whitespace.";
public static string Format_ExtraJunkAtEnd = "Additional non-parsable characters are at the end of the string.";
public static string Format_GuidBrace = "Expected {0xdddddddd, etc}.";
public static string Format_GuidComma = "Could not find a comma, or the length between the previous token and the comma was zero (i.e., '0x,'etc.).";
public static string Format_GuidBraceAfterLastNumber = "Could not find a brace, or the length between the previous token and the brace was zero (i.e., '0x,'etc.).";
public static string Format_GuidDashes = "Dashes are in the wrong position for GUID parsing.";
public static string Format_GuidEndBrace = "Could not find the ending brace.";
public static string Format_GuidHexPrefix = "Expected hex 0x in '{0}'.";
public static string Format_GuidInvLen = "Guid should contain 32 digits with 4 dashes (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx).";
public static string Format_GuidInvalidChar = "Guid string should only contain hexadecimal characters.";
public static string Format_GuidUnrecognized = "Unrecognized Guid format.";
public static string Format_InvalidEnumFormatSpecification = "Format String can be only \"G\", \"g\", \"X\", \"x\", \"F\", \"f\", \"D\" or \"d\".";
public static string Format_InvalidGuidFormatSpecification = "Format String can be only \"D\", \"d\", \"N\", \"n\", \"P\", \"p\", \"B\", \"b\", \"X\" or \"x\".";
public static string Format_InvalidString = "Input string was not in a correct format.";
public static string Format_IndexOutOfRange = "Index (zero based) must be greater than or equal to zero and less than the size of the argument list.";
public static string Format_UnknowDateTimeWord = "The string was not recognized as a valid DateTime. There is an unknown word starting at index {0}.";
public static string Format_NeedSingleChar = "String must be exactly one character long.";
public static string Format_NoParsibleDigits = "Could not find any recognizable digits.";
public static string Format_RepeatDateTimePattern = "DateTime pattern '{0}' appears more than once with different values.";
public static string Format_StringZeroLength = "String cannot have zero length.";
public static string Format_TwoTimeZoneSpecifiers = "The String being parsed cannot contain two TimeZone specifiers.";
public static string Format_UTCOutOfRange = "The UTC representation of the date falls outside the year range 1-9999.";
public static string Format_OffsetOutOfRange = "The time zone offset must be within plus or minus 14 hours.";
public static string Format_MissingIncompleteDate = "There must be at least a partial date with a year present in the input.";

//; IndexOutOfRangeException
public static string IndexOutOfRange_ArrayRankIndex = "Array does not have that many dimensions.";
public static string IndexOutOfRange_IORaceCondition = "Probable I/O race condition detected while copying memory. The I/O package is not thread safe by default. In multithreaded applications, a stream must be accessed in a thread-safe way, such as a thread-safe wrapper returned by TextReader's or TextWriter's Synchronized methods. This also applies to classes like StreamWriter and StreamReader.";
public static string IndexOutOfRange_UMSPosition = "Unmanaged memory stream position was beyond the capacity of the stream.";

//; InsufficientMemoryException
public static string InsufficientMemory_MemFailPoint = "Insufficient available memory to meet the expected demands of an operation at this time.  Please try again later.";
public static string InsufficientMemory_MemFailPoint_TooBig = "Insufficient memory to meet the expected demands of an operation, and this system is likely to never satisfy this request.  If this is a 32 bit system, consider booting in 3 GB mode.";
public static string InsufficientMemory_MemFailPoint_VAFrag = "Insufficient available memory to meet the expected demands of an operation at this time, possibly due to virtual address space fragmentation.  Please try again later.";


//; InvalidCastException
public static string InvalidCast_DBNull = "Object cannot be cast to DBNull.";
public static string InvalidCast_DownCastArrayElement = "At least one element in the source array could not be cast down to the destination array type.";
public static string InvalidCast_Empty = "Object cannot be cast to Empty.";
public static string InvalidCast_FromDBNull = "Object cannot be cast from DBNull to other types.";
public static string InvalidCast_FromTo = "Invalid cast from '{0}' to '{1}'.";
public static string InvalidCast_IConvertible = "Object must implement IConvertible.";
public static string InvalidCast_OATypeMismatch = "OleAut reported a type mismatch.";
public static string InvalidCast_StoreArrayElement = "Object cannot be stored in an array of this type.";
public static string InvalidCast_CannotCoerceByRefVariant = "Object cannot be coerced to the original type of the ByRef VARIANT it was obtained from.";
public static string InvalidCast_CannotCastNullToValueType = "Null object cannot be converted to a value type.";
//#if FEATURE_COMINTEROP
public static string InvalidCast_WinRTIPropertyValueElement = "Object in an IPropertyValue is of type '{0}', which cannot be converted to a '{1}'.";
public static string InvalidCast_WinRTIPropertyValueCoersion = "Object in an IPropertyValue is of type '{0}' with value '{1}', which cannot be converted to a '{2}'.";
public static string InvalidCast_WinRTIPropertyValueArrayCoersion = "Object in an IPropertyValue is of type '{0}' which cannot be convereted to a '{1}' due to array element '{2}': {3}.";
//#endif // FEATURE_COMINTEROP

//; InvalidOperationException
public static string InvalidOperation_ActivationArgsAppTrustMismatch = "The activation arguments and application trust for the AppDomain must correspond to the same application identity.";
public static string InvalidOperation_AddContextFrozen = "Attempted to add properties to a frozen context.";
public static string InvalidOperation_AppDomainSandboxAPINeedsExplicitAppBase = "This API requires the ApplicationBase to be specified explicitly in the AppDomainSetup parameter.";
public static string InvalidOperation_CantCancelCtrlBreak = "Applications may not prevent control-break from terminating their process.";
public static string InvalidOperation_CalledTwice = "The method cannot be called twice on the same instance.";
public static string InvalidOperation_CollectionCorrupted = "A prior operation on this collection was interrupted by an exception. Collection's state is no longer trusted.";
public static string InvalidOperation_CriticalTransparentAreMutuallyExclusive = "SecurityTransparent and SecurityCritical attributes cannot be applied to the assembly scope at the same time.";
public static string InvalidOperation_SubclassedObject = "Cannot set sub-classed {0} object to {1} object.";
public static string InvalidOperation_ExceptionStateCrossAppDomain = "Thread.ExceptionState cannot access an ExceptionState from a different AppDomain.";
public static string InvalidOperation_DebuggerLaunchFailed = "Debugger unable to launch.";
public static string InvalidOperation_ApartmentStateSwitchFailed = "Failed to set the specified COM apartment state.";
public static string InvalidOperation_EmptyQueue = "Queue empty.";
public static string InvalidOperation_EmptyStack = "Stack empty.";
public static string InvalidOperation_CannotRemoveFromStackOrQueue = "Removal is an invalid operation for Stack or Queue.";
public static string InvalidOperation_EnumEnded = "Enumeration already finished.";
public static string InvalidOperation_EnumFailedVersion = "Collection was modified; enumeration operation may not execute.";
public static string InvalidOperation_EnumNotStarted = "Enumeration has not started. Call MoveNext.";
public static string InvalidOperation_EnumOpCantHappen = "Enumeration has either not started or has already finished.";
public static string InvalidOperation_ModifyRONumFmtInfo = "Unable to modify a read-only NumberFormatInfo object.";
//#if FEATURE_CAS_POLICY
public static string InvalidOperation_ModifyROPermSet = "ReadOnlyPermissionSet objects may not be modified.";
//#endif // FEATURE_CAS_POLICY
public static string InvalidOperation_MustBeSameThread = "This operation must take place on the same thread on which the object was created.";
public static string InvalidOperation_MustRevertPrivilege = "Must revert the privilege prior to attempting this operation.";
public static string InvalidOperation_ReadOnly = "Instance is read-only.";
public static string InvalidOperation_RegRemoveSubKey = "Registry key has subkeys and recursive removes are not supported by this method.";
public static string InvalidOperation_IComparerFailed = "Failed to compare two elements in the array.";
public static string InvalidOperation_InternalState = "Invalid internal state.";
public static string InvalidOperation_DuplicatePropertyName = "Another property by this name already exists.";
public static string InvalidOperation_NotCurrentDomain = "You can only define a dynamic assembly on the current AppDomain.";
public static string InvalidOperation_ContextAlreadyFrozen = "Context is already frozen.";
public static string InvalidOperation_WriteOnce = "This property has already been set and cannot be modified.";
public static string InvalidOperation_MethodBaked = "Type definition of the method is complete.";
public static string InvalidOperation_MethodHasBody = "Method already has a body.";
public static string InvalidOperation_ModificationOfNonCanonicalAcl = "This access control list is not in canonical form and therefore cannot be modified.";
public static string InvalidOperation_Method = "This method is not supported by the current object.";
public static string InvalidOperation_NotADebugModule = "Not a debug ModuleBuilder.";
public static string InvalidOperation_NoMultiModuleAssembly = "You cannot have more than one dynamic module in each dynamic assembly in this version of the runtime.";
public static string InvalidOperation_OpenLocalVariableScope = "Local variable scope was not properly closed.";
public static string InvalidOperation_SetVolumeLabelFailed = "Volume labels can only be set for writable local volumes.";
public static string InvalidOperation_SetData = "An additional permission should not be supplied for setting loader information.";
public static string InvalidOperation_SetData_OnlyOnce = "SetData can only be used to set the value of a given name once.";
public static string InvalidOperation_SetData_OnlyLocationURI = "SetData cannot be used to set the value for '{0}'.";
public static string InvalidOperation_TypeHasBeenCreated = "Unable to change after type has been created.";
public static string InvalidOperation_TypeNotCreated = "Type has not been created.";
public static string InvalidOperation_NoUnderlyingTypeOnEnum = "Underlying type information on enumeration is not specified.";
public static string InvalidOperation_ResMgrBadResSet_Type = "'{0}': ResourceSet derived classes must provide a constructor that takes a String file name and a constructor that takes a Stream.";
public static string InvalidOperation_AssemblyHasBeenSaved = "Assembly '{0}' has been saved.";
public static string InvalidOperation_ModuleHasBeenSaved = "Module '{0}' has been saved.";
public static string InvalidOperation_CannotAlterAssembly = "Unable to alter assembly information.";
public static string InvalidOperation_BadTransientModuleReference = "Unable to make a reference to a transient module from a non-transient module.";
public static string InvalidOperation_BadILGeneratorUsage = "ILGenerator usage is invalid.";
public static string InvalidOperation_BadInstructionOrIndexOutOfBound = "MSIL instruction is invalid or index is out of bounds.";
public static string InvalidOperation_ShouldNotHaveMethodBody = "Method body should not exist.";
public static string InvalidOperation_EntryMethodNotDefinedInAssembly = "Entry method is not defined in the same assembly.";
public static string InvalidOperation_CantSaveTransientAssembly = "Cannot save a transient assembly.";
public static string InvalidOperation_BadResourceContainer = "Unable to add resource to transient module or transient assembly.";
public static string InvalidOperation_CantInstantiateAbstractClass = "Instances of abstract classes cannot be created.";
public static string InvalidOperation_CantInstantiateFunctionPointer = "Instances of function pointers cannot be created.";
public static string InvalidOperation_BadTypeAttributesNotAbstract = "Type must be declared abstract if any of its methods are abstract.";
public static string InvalidOperation_BadInterfaceNotAbstract = "Interface must be declared abstract.";
public static string InvalidOperation_ConstructorNotAllowedOnInterface = "Interface cannot have constructors.";
public static string InvalidOperation_BadMethodBody = "Method '{0}' cannot have a method body.";
public static string InvalidOperation_MetaDataError = "Metadata operation failed.";
public static string InvalidOperation_BadEmptyMethodBody = "Method '{0}' does not have a method body.";
public static string InvalidOperation_EndInvokeCalledMultiple = "EndInvoke can only be called once for each asynchronous operation.";
public static string InvalidOperation_EndReadCalledMultiple = "EndRead can only be called once for each asynchronous operation.";
public static string InvalidOperation_EndWriteCalledMultiple = "EndWrite can only be called once for each asynchronous operation.";
public static string InvalidOperation_AsmLoadedForReflectionOnly = "Assembly has been loaded as ReflectionOnly. This API requires an assembly capable of execution.";
public static string InvalidOperation_NoAsmName = "Assembly does not have an assembly name. In order to be registered for use by COM, an assembly must have a valid assembly name.";
public static string InvalidOperation_NoAsmCodeBase = "Assembly does not have a code base.";
public static string InvalidOperation_HandleIsNotInitialized = "Handle is not initialized.";
public static string InvalidOperation_HandleIsNotPinned = "Handle is not pinned.";
public static string InvalidOperation_SlotHasBeenFreed = "LocalDataStoreSlot storage has been freed.";
public static string InvalidOperation_GlobalsHaveBeenCreated = "Type definition of the global function has been completed.";
public static string InvalidOperation_NotAVarArgCallingConvention = "Calling convention must be VarArgs.";
public static string InvalidOperation_CannotImportGlobalFromDifferentModule = "Unable to import a global method or field from a different module.";
public static string InvalidOperation_NonStaticComRegFunction = "COM register function must be static.";
public static string InvalidOperation_NonStaticComUnRegFunction = "COM unregister function must be static.";
public static string InvalidOperation_InvalidComRegFunctionSig = "COM register function must have a System.Type parameter and a void return type.";
public static string InvalidOperation_InvalidComUnRegFunctionSig = "COM unregister function must have a System.Type parameter and a void return type.";
public static string InvalidOperation_MultipleComRegFunctions = "Type '{0}' has more than one COM registration function.";
public static string InvalidOperation_MultipleComUnRegFunctions = "Type '{0}' has more than one COM unregistration function.";
public static string InvalidOperation_MustCallInitialize = "You must call Initialize on this object instance before using it.";
public static string InvalidOperation_MustLockForReadOrWrite = "Object must be locked for read or write.";
public static string InvalidOperation_MustLockForWrite = "Object must be locked for read.";
public static string InvalidOperation_NoValue = "Nullable object must have a value.";
public static string InvalidOperation_ResourceNotStream_Name = "Resource '{0}' was not a Stream - call GetObject instead.";
public static string InvalidOperation_ResourceNotString_Name = "Resource '{0}' was not a String - call GetObject instead.";
public static string InvalidOperation_ResourceNotString_Type = "Resource was of type '{0}' instead of String - call GetObject instead.";
public static string InvalidOperation_ResourceWriterSaved = "The resource writer has already been closed and cannot be edited.";
public static string InvalidOperation_UnderlyingArrayListChanged = "This range in the underlying list is invalid. A possible cause is that elements were removed.";
public static string InvalidOperation_AnonymousCannotImpersonate = "An anonymous identity cannot perform an impersonation.";
public static string InvalidOperation_DefaultConstructorILGen = "Unable to access ILGenerator on a constructor created with DefineDefaultConstructor.";
public static string InvalidOperation_DefaultConstructorDefineBody = "The method body of the default constructor cannot be changed.";
public static string InvalidOperation_ComputerName = "Computer name could not be obtained.";
public static string InvalidOperation_MismatchedAsyncResult = "The IAsyncResult object provided does not match this delegate.";
public static string InvalidOperation_PIAMustBeStrongNamed = "Primary interop assemblies must be strongly named.";
public static string InvalidOperation_HashInsertFailed = "Hashtable insert failed. Load factor too high. The most common cause is multiple threads writing to the Hashtable simultaneously.";
public static string InvalidOperation_UnknownEnumType = "Unknown enum type.";
public static string InvalidOperation_GetVersion = "OSVersion's call to GetVersionEx failed.";
public static string InvalidOperation_DateTimeParsing = "Internal Error in DateTime and Calendar operations.";
public static string InvalidOperation_UserDomainName = "UserDomainName native call failed.";
public static string InvalidOperation_WaitOnTransparentProxy = "Cannot wait on a transparent proxy.";
public static string InvalidOperation_NoPublicAddMethod = "Cannot add the event handler since no public add method exists for the event.";
public static string InvalidOperation_NoPublicRemoveMethod = "Cannot remove the event handler since no public remove method exists for the event.";
public static string InvalidOperation_NotSupportedOnWinRTEvent = "Adding or removing event handlers dynamically is not supported on WinRT events.";
public static string InvalidOperation_ConsoleKeyAvailableOnFile = "Cannot see if a key has been pressed when either application does not have a console or when console input has been redirected from a file. Try Console.In.Peek.";
public static string InvalidOperation_ConsoleReadKeyOnFile = "Cannot read keys when either application does not have a console or when console input has been redirected from a file. Try Console.Read.";
public static string InvalidOperation_ThreadWrongThreadStart = "The thread was created with a ThreadStart delegate that does not accept a parameter.";
public static string InvalidOperation_ThreadAPIsNotSupported = "Use CompressedStack.(Capture/Run) or ExecutionContext.(Capture/Run) APIs instead.";
public static string InvalidOperation_NotNewCaptureContext = "Cannot apply a context that has been marshaled across AppDomains, that was not acquired through a Capture operation or that has already been the argument to a Set call.";
public static string InvalidOperation_NullContext = "Cannot call Set on a null context";
public static string InvalidOperation_CannotCopyUsedContext = "Only newly captured contexts can be copied";
public static string InvalidOperation_CannotUseSwitcherOtherThread = "Undo operation must be performed on the thread where the corresponding context was Set.";
public static string InvalidOperation_SwitcherCtxMismatch = "The Undo operation encountered a context that is different from what was applied in the corresponding Set operation. The possible cause is that a context was Set on the thread and not reverted(undone).";
public static string InvalidOperation_CannotOverrideSetWithoutRevert = "Must override both HostExecutionContextManager.SetHostExecutionContext and HostExecutionContextManager.Revert.";
public static string InvalidOperation_CannotUseAFCOtherThread = "AsyncFlowControl object must be used on the thread where it was created.";
public static string InvalidOperation_CannotRestoreUnsupressedFlow = "Cannot restore context flow when it is not suppressed.";
public static string InvalidOperation_CannotSupressFlowMultipleTimes = "Context flow is already suppressed.";
public static string InvalidOperation_CannotUseAFCMultiple = "AsyncFlowControl object can be used only once to call Undo().";
public static string InvalidOperation_AsyncFlowCtrlCtxMismatch = "AsyncFlowControl objects can be used to restore flow only on the Context that had its flow suppressed.";
public static string InvalidOperation_TimeoutsNotSupported = "Timeouts are not supported on this stream.";
public static string InvalidOperation_Overlapped_Pack = "Cannot pack a packed Overlapped again.";
public static string InvalidOperation_OnlyValidForDS = "Adding ACEs with Object Flags and Object GUIDs is only valid for directory-object ACLs.";
public static string InvalidOperation_WrongAsyncResultOrEndReadCalledMultiple = "Either the IAsyncResult object did not come from the corresponding async method on this type, or EndRead was called multiple times with the same IAsyncResult.";
public static string InvalidOperation_WrongAsyncResultOrEndWriteCalledMultiple = "Either the IAsyncResult object did not come from the corresponding async method on this type, or EndWrite was called multiple times with the same IAsyncResult.";
public static string InvalidOperation_WrongAsyncResultOrEndCalledMultiple = "Either the IAsyncResult object did not come from the corresponding async method on this type, or the End method was called multiple times with the same IAsyncResult.";
public static string InvalidOperation_NoSecurityDescriptor = "The object does not contain a security descriptor.";
public static string InvalidOperation_NotAllowedInReflectionOnly = "The requested operation is invalid in the ReflectionOnly context.";
public static string InvalidOperation_NotAllowedInDynamicMethod = "The requested operation is invalid for DynamicMethod.";
public static string InvalidOperation_PropertyInfoNotAvailable = "This API does not support PropertyInfo tokens.";
public static string InvalidOperation_EventInfoNotAvailable = "This API does not support EventInfo tokens.";
public static string InvalidOperation_UnexpectedWin32Error = "Unexpected error when calling an operating system function.  The returned error code is 0x{0:x}.";
public static string InvalidOperation_AssertTransparentCode = "Cannot perform CAS Asserts in Security Transparent methods";
public static string InvalidOperation_NullModuleHandle = "The requested operation is invalid when called on a null ModuleHandle.";
public static string InvalidOperation_NotWithConcurrentGC = "This API is not available when the concurrent GC is enabled.";
public static string InvalidOperation_WithoutARM = "This API is not available when AppDomain Resource Monitoring is not turned on.";
public static string InvalidOperation_NotGenericType = "This operation is only valid on generic types.";
public static string InvalidOperation_TypeCannotBeBoxed = "The given type cannot be boxed.";
public static string InvalidOperation_HostModifiedSecurityState = "The security state of an AppDomain was modified by an AppDomainManager configured with the NoSecurityChanges flag.";
public static string InvalidOperation_StrongNameKeyPairRequired = "A strong name key pair is required to emit a strong-named dynamic assembly.";
//#if FEATURE_COMINTEROP
public static string InvalidOperation_EventTokenTableRequiresDelegate = "Type '{0}' is not a delegate type.  EventTokenTable may only be used with delegate types.";
//#endif // FEATURE_COMINTEROP
public static string InvalidOperation_NullArray = "The underlying array is null.";
//;system.security.claims
public static string InvalidOperation_ClaimCannotBeRemoved = "The Claim '{0}' was not able to be removed.  It is either not part of this Identity or it is a claim that is owned by the Principal that contains this Identity. For example, the Principal will own the claim when creating a GenericPrincipal with roles. The roles will be exposed through the Identity that is passed in the constructor, but not actually owned by the Identity.  Similar logic exists for a RolePrincipal.";
public static string InvalidOperationException_ActorGraphCircular = "Actor cannot be set so that circular directed graph will exist chaining the subjects together.";
public static string InvalidOperation_AsyncIOInProgress = "The stream is currently in use by a previous operation on the stream.";
public static string InvalidOperation_APIInvalidForCurrentContext = "The API '{0}' cannot be used on the current platform. See http://go.microsoft.com/fwlink/?LinkId=248273 for more information.";

//; InvalidProgramException
public static string InvalidProgram_Default = "Common Language Runtime detected an invalid program.";

//; Isolated Storage
//#if FEATURE_ISOSTORE
public static string IsolatedStorage_AssemblyMissingIdentity = "Unable to determine assembly of the caller.";
public static string IsolatedStorage_ApplicationMissingIdentity = "Unable to determine application identity of the caller.";
public static string IsolatedStorage_DomainMissingIdentity = "Unable to determine domain of the caller.";
public static string IsolatedStorage_AssemblyGrantSet = "Unable to determine granted permission for assembly.";
public static string IsolatedStorage_DomainGrantSet = "Unable to determine granted permission for domain.";
public static string IsolatedStorage_ApplicationGrantSet = "Unable to determine granted permission for application.";
public static string IsolatedStorage_Init = "Initialization failed.";
public static string IsolatedStorage_ApplicationNoEvidence = "Unable to determine identity of application.";
public static string IsolatedStorage_AssemblyNoEvidence = "Unable to determine identity of assembly.";
public static string IsolatedStorage_DomainNoEvidence = "Unable to determine the identity of domain.";
public static string IsolatedStorage_DeleteDirectories = "Unable to delete; directory or files in the directory could be in use.";
public static string IsolatedStorage_DeleteFile = "Unable to delete file.";
public static string IsolatedStorage_CreateDirectory = "Unable to create directory.";
public static string IsolatedStorage_DeleteDirectory = "Unable to delete, directory not empty or does not exist.";
public static string IsolatedStorage_Operation_ISFS = "Operation not permitted on IsolatedStorageFileStream.";
public static string IsolatedStorage_Operation = "Operation not permitted.";
public static string IsolatedStorage_Path = "Path must be a valid file name.";
public static string IsolatedStorage_FileOpenMode = "Invalid mode, see System.IO.FileMode.";
public static string IsolatedStorage_SeekOrigin = "Invalid origin, see System.IO.SeekOrigin.";
public static string IsolatedStorage_Scope_U_R_M = "Invalid scope, expected User, User|Roaming or Machine.";
public static string IsolatedStorage_Scope_Invalid = "Invalid scope.";
public static string IsolatedStorage_Exception = "An error occurred while accessing IsolatedStorage.";
public static string IsolatedStorage_QuotaIsUndefined = "{0} is not defined for this store. An operation was performed that requires access to {0}. Stores obtained using enumeration APIs do not have a well-defined {0}, since partial evidence is used to open the store.";
public static string IsolatedStorage_CurrentSizeUndefined = "Current size cannot be determined for this store.";
public static string IsolatedStorage_DomainUndefined = "Domain cannot be determined on an Assembly or Application store.";
public static string IsolatedStorage_ApplicationUndefined = "Application cannot be determined on an Assembly or Domain store.";
public static string IsolatedStorage_AssemblyUndefined = "Assembly cannot be determined for an Application store.";
public static string IsolatedStorage_StoreNotOpen = "Store must be open for this operation.";
public static string IsolatedStorage_OldQuotaLarger = "The new quota must be larger than the old quota.";
public static string IsolatedStorage_UsageWillExceedQuota = "There is not enough free space to perform the operation.";
public static string IsolatedStorage_NotValidOnDesktop = "The Site scope is currently not supported.";
public static string IsolatedStorage_OnlyIncreaseUserApplicationStore = "Increasing the quota of this scope is not supported.  Only the user application scope’s quota can be increased.";
//#endif  // FEATURE_ISOSTORE

//; Verification Exception
public static string Verification_Exception = "Operation could destabilize the runtime.";

//; IL stub marshaler exceptions
public static string Marshaler_StringTooLong = "Marshaler restriction: Excessively long string.";

//; Missing (General)
public static string MissingConstructor_Name = "Constructor on type '{0}' not found.";
public static string MissingField = "Field not found.";
public static string MissingField_Name = "Field '{0}' not found.";
public static string MissingMember = "Member not found.";
public static string MissingMember_Name = "Member '{0}' not found.";
public static string MissingMethod_Name = "Method '{0}' not found.";
public static string MissingModule = "Module '{0}' not found.";
public static string MissingType = "Type '{0}' not found.";

//; MissingManifestResourceException
public static string Arg_MissingManifestResourceException = "Unable to find manifest resource.";
public static string MissingManifestResource_LooselyLinked = "Could not find a manifest resource entry called \"{0}\" in assembly \"{1}\". Please check spelling, capitalization, and build rules to ensure \"{0}\" is being linked into the assembly.";
public static string MissingManifestResource_NoNeutralAsm = "Could not find any resources appropriate for the specified culture or the neutral culture.  Make sure \"{0}\" was correctly embedded or linked into assembly \"{1}\" at compile time, or that all the satellite assemblies required are loadable and fully signed.";
public static string MissingManifestResource_NoNeutralDisk = "Could not find any resources appropriate for the specified culture (or the neutral culture) on disk.";
public static string MissingManifestResource_MultipleBlobs = "A case-insensitive lookup for resource file \"{0}\" in assembly \"{1}\" found multiple entries. Remove the duplicates or specify the exact case.";
//#if !FEATURE_CORECLR
public static string MissingManifestResource_ResWFileNotLoaded = "Unable to load resources for resource file \"{0}\" in package \"{1}\".";
public static string MissingManifestResource_NoPRIresources = "Unable to open Package Resource Index.";
//#endif

//; MissingMember
public static string MissingMemberTypeRef = "FieldInfo does not match the target Type.";
public static string MissingMemberNestErr = "TypedReference can only be made on nested value Types.";

//; MissingSatelliteAssemblyException
public static string MissingSatelliteAssembly_Default = "Resource lookup fell back to the ultimate fallback resources in a satellite assembly, but that satellite either was not found or could not be loaded. Please consider reinstalling or repairing the application.";
public static string MissingSatelliteAssembly_Culture_Name = "The satellite assembly named \"{1}\" for fallback culture \"{0}\" either could not be found or could not be loaded. This is generally a setup problem. Please consider reinstalling or repairing the application.";

//; MulticastNotSupportedException
public static string Multicast_Combine = "Delegates that are not of type MulticastDelegate may not be combined.";

//; NotImplementedException
public static string Arg_NotImplementedException = "The method or operation is not implemented.";
public static string NotImplemented_ResourcesLongerThan2_63 = "Resource files longer than 2^63 bytes are not currently implemented.";

//; NotSupportedException
public static string NotSupported_NYI = "This feature is not currently implemented.";
public static string NotSupported_AbstractNonCLS = "This non-CLS method is not implemented.";
public static string NotSupported_ChangeType = "ChangeType operation is not supported.";
public static string NotSupported_ContainsStackPtr = "Cannot create boxed (or arrays) of TypedReference, ArgIterator, or RuntimeArgumentHandle Objects.";
public static string NotSupported_OpenType = "Cannot create arrays of open type. ";
public static string NotSupported_DBNullSerial = "Only one DBNull instance may exist, and calls to DBNull deserialization methods are not allowed.";
public static string NotSupported_DelegateSerHolderSerial = "DelegateSerializationHolder objects are designed to represent a delegate during serialization and are not serializable themselves.";
public static string NotSupported_DelegateCreationFromPT = "Application code cannot use Activator.CreateInstance to create types that derive from System.Delegate. Delegate.CreateDelegate can be used instead.";
public static string NotSupported_EncryptionNeedsNTFS = "File encryption support only works on NTFS partitions.";
public static string NotSupported_FileStreamOnNonFiles = "FileStream was asked to open a device that was not a file. For support for devices like 'com1:' or 'lpt1:', call CreateFile, then use the FileStream constructors that take an OS handle as an IntPtr.";
public static string NotSupported_FixedSizeCollection = "Collection was of a fixed size.";
public static string NotSupported_KeyCollectionSet = "Mutating a key collection derived from a dictionary is not allowed.";
public static string NotSupported_ValueCollectionSet = "Mutating a value collection derived from a dictionary is not allowed.";
public static string NotSupported_MemStreamNotExpandable = "Memory stream is not expandable.";
public static string NotSupported_ObsoleteResourcesFile = "Found an obsolete .resources file in assembly '{0}'. Rebuild that .resources file then rebuild that assembly.";
public static string NotSupported_OleAutBadVarType = "The given Variant type is not supported by this OleAut function.";
public static string NotSupported_PopulateData = "This Surrogate does not support PopulateData().";
public static string NotSupported_ReadOnlyCollection = "Collection is read-only.";
public static string NotSupported_RangeCollection = "The specified operation is not supported on Ranges.";
public static string NotSupported_SortedListNestedWrite = "This operation is not supported on SortedList nested types because they require modifying the original SortedList.";
public static string NotSupported_SubclassOverride = "Derived classes must provide an implementation.";
public static string NotSupported_TypeCannotDeserialized = "Direct deserialization of type '{0}' is not supported.";
public static string NotSupported_UnreadableStream = "Stream does not support reading.";
public static string NotSupported_UnseekableStream = "Stream does not support seeking.";
public static string NotSupported_UnwritableStream = "Stream does not support writing.";
public static string NotSupported_CannotWriteToBufferedStreamIfReadBufferCannotBeFlushed = "Cannot write to a BufferedStream while the read buffer is not empty if the underlying stream is not seekable. Ensure that the stream underlying this BufferedStream can seek or avoid interleaving read and write operations on this BufferedStream.";
public static string NotSupported_Method = "Method is not supported.";
public static string NotSupported_Constructor = "Object cannot be created through this constructor.";
public static string NotSupported_DynamicModule = "The invoked member is not supported in a dynamic module.";
public static string NotSupported_TypeNotYetCreated = "The invoked member is not supported before the type is created.";
public static string NotSupported_SymbolMethod = "Not supported in an array method of a type definition that is not complete.";
public static string NotSupported_NotDynamicModule = "The MethodRental.SwapMethodBody method can only be called to swap the method body of a method in a dynamic module.";
public static string NotSupported_DynamicAssembly = "The invoked member is not supported in a dynamic assembly.";
public static string NotSupported_NotAllTypesAreBaked = "Type '{0}' was not completed.";
public static string NotSupported_CannotSaveModuleIndividually = "Unable to save a ModuleBuilder if it was created underneath an AssemblyBuilder. Call Save on the AssemblyBuilder instead.";
public static string NotSupported_MaxWaitHandles = "The number of WaitHandles must be less than or equal to 64.";
public static string NotSupported_IllegalOneByteBranch = "Illegal one-byte branch at position: {0}. Requested branch was: {1}.";
public static string NotSupported_OutputStreamUsingTypeBuilder = "Output streams do not support TypeBuilders.";
public static string NotSupported_ValueClassCM = "Custom marshalers for value types are not currently supported.";
public static string NotSupported_Void = "Arrays of System.Void are not supported.";
public static string NotSupported_NoParentDefaultConstructor = "Parent does not have a default constructor. The default constructor must be explicitly defined.";
public static string NotSupported_NonReflectedType = "Not supported in a non-reflected type.";
public static string NotSupported_GlobalFunctionNotBaked = "The type definition of the global function is not completed.";
public static string NotSupported_SecurityPermissionUnion = "Union is not implemented.";
public static string NotSupported_UnitySerHolder = "The UnitySerializationHolder object is designed to transmit information about other types and is not serializable itself.";
public static string NotSupported_UnknownTypeCode = "TypeCode '{0}' was not valid.";
public static string NotSupported_WaitAllSTAThread = "WaitAll for multiple handles on a STA thread is not supported.";
public static string NotSupported_SignalAndWaitSTAThread = "SignalAndWait on a STA thread is not supported.";
public static string NotSupported_CreateInstanceWithTypeBuilder = "CreateInstance cannot be used with an object of type TypeBuilder.";
public static string NotSupported_NonUrlAttrOnMBR = "UrlAttribute is the only attribute supported for MarshalByRefObject.";
public static string NotSupported_ActivAttrOnNonMBR = "Activation Attributes are not supported for types not deriving from MarshalByRefObject.";
public static string NotSupported_ActivForCom = "Activation Attributes not supported for COM Objects.";
public static string NotSupported_NoCodepageData = "No data is available for encoding {0}. For information on defining a custom encoding, see the documentation for the Encoding.RegisterProvider method.";
public static string NotSupported_CodePage50229 = "The ISO-2022-CN Encoding (Code page 50229) is not supported.";
public static string NotSupported_DynamicAssemblyNoRunAccess = "Cannot execute code on a dynamic assembly without run access.";
public static string NotSupported_IDispInvokeDefaultMemberWithNamedArgs = "Invoking default method with named arguments is not supported.";
public static string NotSupported_Type = "Type is not supported.";
public static string NotSupported_GetMethod = "The 'get' method is not supported on this property.";
public static string NotSupported_SetMethod = "The 'set' method is not supported on this property.";
public static string NotSupported_DeclarativeUnion = "Declarative unionizing of these permissions is not supported.";
public static string NotSupported_StringComparison = "The string comparison type passed in is currently not supported.";
public static string NotSupported_WrongResourceReader_Type = "This .resources file should not be read with this reader. The resource reader type is \"{0}\".";
public static string NotSupported_MustBeModuleBuilder = "Module argument must be a ModuleBuilder.";
public static string NotSupported_CallToVarArg = "Vararg calling convention not supported.";
public static string NotSupported_TooManyArgs = "Stack size too deep. Possibly too many arguments.";
public static string NotSupported_DeclSecVarArg = "Assert, Deny, and PermitOnly are not supported on methods with a Vararg calling convention.";
public static string NotSupported_AmbiguousIdentity = "The operation is ambiguous because the permission represents multiple identities.";
public static string NotSupported_DynamicMethodFlags = "Wrong MethodAttributes or CallingConventions for DynamicMethod. Only public, static, standard supported";
public static string NotSupported_GlobalMethodSerialization = "Serialization of global methods (including implicit serialization via the use of asynchronous delegates) is not supported.";
public static string NotSupported_InComparableType = "A type must implement IComparable<T> or IComparable to support comparison.";
public static string NotSupported_ManagedActivation = "Cannot create uninitialized instances of types requiring managed activation.";
public static string NotSupported_ByRefReturn = "ByRef return value not supported in reflection invocation.";
public static string NotSupported_DelegateMarshalToWrongDomain = "Delegates cannot be marshaled from native code into a domain other than their home domain.";
public static string NotSupported_ResourceObjectSerialization = "Cannot read resources that depend on serialization.";
public static string NotSupported_One = "The arithmetic type '{0}' cannot represent the number one.";
public static string NotSupported_Zero = "The arithmetic type '{0}' cannot represent the number zero.";
public static string NotSupported_MaxValue = "The arithmetic type '{0}' does not have a maximum value.";
public static string NotSupported_MinValue = "The arithmetic type '{0}' does not have a minimum value.";
public static string NotSupported_PositiveInfinity = "The arithmetic type '{0}' cannot represent positive infinity.";
public static string NotSupported_NegativeInfinity = "The arithmetic type '{0}' cannot represent negative infinity.";
public static string NotSupported_UmsSafeBuffer = "This operation is not supported for an UnmanagedMemoryStream created from a SafeBuffer.";
public static string NotSupported_Reading = "Accessor does not support reading.";
public static string NotSupported_Writing = "Accessor does not support writing.";
public static string NotSupported_UnsafePointer = "This accessor was created with a SafeBuffer; use the SafeBuffer to gain access to the pointer.";
public static string NotSupported_CollectibleCOM = "COM Interop is not supported for collectible types.";
public static string NotSupported_CollectibleAssemblyResolve = "Resolving to a collectible assembly is not supported.";
public static string NotSupported_CollectibleBoundNonCollectible = "A non-collectible assembly may not reference a collectible assembly.";
public static string NotSupported_CollectibleDelegateMarshal = "Delegate marshaling for types within collectible assemblies is not supported.";
//#if FEATURE_WINDOWSPHONE
public static string NotSupported_UserDllImport = "DllImport cannot be used on user-defined methods.";
public static string NotSupported_UserCOM = "COM Interop is not supported for user-defined types.";
//#endif //FEATURE_WINDOWSPHONE
//#if FEATURE_CAS_POLICY
public static string NotSupported_RequiresCasPolicyExplicit = "This method explicitly uses CAS policy, which has been obsoleted by the .NET Framework. In order to enable CAS policy for compatibility reasons, please use the NetFx40_LegacySecurityPolicy configuration switch. Please see http://go.microsoft.com/fwlink/?LinkID=155570 for more information.";
public static string NotSupported_RequiresCasPolicyImplicit = "This method implicitly uses CAS policy, which has been obsoleted by the .NET Framework. In order to enable CAS policy for compatibility reasons, please use the NetFx40_LegacySecurityPolicy configuration switch. Please see http://go.microsoft.com/fwlink/?LinkID=155570 for more information.";
public static string NotSupported_CasDeny = "The Deny stack modifier has been obsoleted by the .NET Framework.  Please see http://go.microsoft.com/fwlink/?LinkId=155571 for more information.";
public static string NotSupported_SecurityContextSourceAppDomainInHeterogenous = "SecurityContextSource.CurrentAppDomain is not supported in heterogenous AppDomains.";
//#endif // FEATURE_CAS_POLICY
//#if FEATURE_APPX
public static string NotSupported_AppX = "{0} is not supported in AppX.";
public static string LoadOfFxAssemblyNotSupported_AppX = "{0} of .NET Framework assemblies is not supported in AppX.";
//#endif
//#if FEATURE_COMINTEROP
public static string NotSupported_WinRT_PartialTrust = "Windows Runtime is not supported in partial trust.";
//#endif // FEATURE_COMINTEROP
//; ReflectionTypeLoadException
public static string ReflectionTypeLoad_LoadFailed = "Unable to load one or more of the requested types. Retrieve the LoaderExceptions property for more information.";
//#if !FEATURE_CORECLR
public static string NotSupported_NoTypeInfo = "Cannot resolve {0} to a TypeInfo object.";
//#endif
//#if FEATURE_COMINTEROP
public static string NotSupported_PIAInAppxProcess = "A Primary Interop Assembly is not supported in AppX.";
//#endif
//#if FEATURE_WINDOWSPHONE
public static string NotSupported_WindowsPhone = "{0} is not supported on Windows Phone.";
public static string NotSupported_AssemblyLoadCodeBase = "Assembly.Load with a Codebase is not supported on Windows Phone.";
//#endif

//; TypeLoadException
public static string TypeLoad_ResolveType = "Could not resolve type '{0}'.";
public static string TypeLoad_ResolveTypeFromAssembly = "Could not resolve type '{0}' in assembly '{1}'.";
public static string TypeLoad_ResolveNestedType = "Could not resolve nested type '{0}' in type '{1}'.";
public static string FileNotFound_ResolveAssembly = "Could not resolve assembly '{0}'.";

//; NullReferenceException
public static string NullReference_This = "The pointer for this method was null.";

//; ObjectDisposedException
public static string ObjectDisposed_Generic = "Cannot access a disposed object.";
public static string ObjectDisposed_FileClosed = "Cannot access a closed file.";
public static string ObjectDisposed_ObjectName_Name = "Object name: '{0}'.";
public static string ObjectDisposed_ReaderClosed = "Cannot read from a closed TextReader.";
public static string ObjectDisposed_ResourceSet = "Cannot access a closed resource set.";
public static string ObjectDisposed_RegKeyClosed = "Cannot access a closed registry key.";
public static string ObjectDisposed_StreamClosed = "Cannot access a closed Stream.";
public static string ObjectDisposed_WriterClosed = "Cannot write to a closed TextWriter.";
public static string ObjectDisposed_ViewAccessorClosed = "Cannot access a closed accessor.";

//; OperationCanceledException
public static string OperationCanceled = "The operation was canceled.";

//; OutOfMemoryException
public static string OutOfMemory_GCHandleMDA = "The GCHandle MDA has run out of available cookies.";

//; OverflowException
public static string Overflow_Byte = "Value was either too large or too small for an unsigned byte.";
public static string Overflow_Char = "Value was either too large or too small for a character.";
public static string Overflow_Currency = "Value was either too large or too small for a Currency.";
public static string Overflow_Decimal = "Value was either too large or too small for a Decimal.";
public static string Overflow_Int16 = "Value was either too large or too small for an Int16.";
public static string Overflow_Int32 = "Value was either too large or too small for an Int32.";
public static string Overflow_Int64 = "Value was either too large or too small for an Int64.";
public static string Overflow_NegateTwosCompNum = "Negating the minimum value of a twos complement number is invalid.";
public static string Overflow_NegativeUnsigned = "The string was being parsed as an unsigned number and could not have a negative sign.";
public static string Overflow_SByte = "Value was either too large or too small for a signed byte.";
public static string Overflow_Single = "Value was either too large or too small for a Single.";
public static string Overflow_Double = "Value was either too large or too small for a Double.";
public static string Overflow_TimeSpanTooLong = "TimeSpan overflowed because the duration is too long.";
public static string Overflow_TimeSpanElementTooLarge = "The TimeSpan could not be parsed because at least one of the numeric components is out of range or contains too many digits.";
public static string Overflow_Duration = "The duration cannot be returned for TimeSpan.MinValue because the absolute value of TimeSpan.MinValue exceeds the value of TimeSpan.MaxValue.";
public static string Overflow_UInt16 = "Value was either too large or too small for a UInt16.";
public static string Overflow_UInt32 = "Value was either too large or too small for a UInt32.";
public static string Overflow_UInt64 = "Value was either too large or too small for a UInt64.";

//; PlatformNotsupportedException
public static string PlatformNotSupported_RequiresLonghorn = "This operation is only supported on Windows Vista and above.";
public static string PlatformNotSupported_RequiresNT = "This operation is only supported on Windows 2000, Windows XP, and higher.";
public static string PlatformNotSupported_RequiresW2kSP3 = "This operation is only supported on Windows 2000 SP3 or later operating systems.";
//#if FEATURE_COMINTEROP
public static string PlatformNotSupported_WinRT = "Windows Runtime is not supported on this operating system.";
//#endif // FEATURE_COMINTEROP

//; PolicyException
//; This still appears in bcl.small but should go away eventually
public static string Policy_Default = "Error occurred while performing a policy operation.";
public static string Policy_CannotLoadSemiTrustAssembliesDuringInit = "All assemblies loaded as part of AppDomain initialization must be fully trusted.";
//#if FEATURE_IMPERSONATION
public static string Policy_PrincipalTwice = "Default principal object cannot be set twice.";
//#endif // FEATURE_IMPERSONATION
//#if FEATURE_CAS_POLICY
public static string Policy_PolicyAlreadySet = "Policy for this domain cannot be set twice.";
public static string Policy_NoExecutionPermission = "Execution permission cannot be acquired.";
public static string Policy_NoRequiredPermission = "Required permissions cannot be acquired.";
public static string Policy_MultipleExclusive = "More than one exclusive group is not allowed.";
public static string Policy_RecoverNotFileBased = "PolicyLevel object not based on a file cannot be recovered.";
public static string Policy_RecoverNoConfigFile = "No old configuration file exists to recover.";
public static string Policy_UnableToSave = "Policy level '{0}' could not be saved: {1}.";
public static string Policy_BadXml = "Policy configuration XML is invalid. The required tag '{0}' is missing.";
public static string Policy_NonFullTrustAssembly = "Policy references an assembly not in the full trust assemblies list.";
public static string Policy_MissingActivationContextInAppEvidence = "The application evidence does not contain a Fusion activation context.";
public static string Policy_NoTrustManager = "A trust manager could not be loaded for this application.";
public static string Policy_GrantSetDoesNotMatchDomain = "An assembly was provided an invalid grant set by runtime host '{0}'. In a homogenous AppDomain, the only valid grant sets are FullTrust and the AppDomain's sandbox grant set.";
//#endif  // FEATURE_CAS_POLICY
public static string Policy_SaveNotFileBased = "PolicyLevel object not based on a file cannot be saved.";
public static string Policy_AppTrustMustGrantAppRequest = "ApplicationTrust grant set does not contain ActivationContext's minimum request set.";

public static string Error_SecurityPolicyFileParse = "Error occurred while parsing the '{0}' policy level. The default policy level was used instead.";
public static string Error_SecurityPolicyFileParseEx = "Error '{1}' occurred while parsing the '{0}' policy level. The default policy level was used instead.";

//#if FEATURE_CAS_POLICY
public static string Policy_EvidenceMustBeSerializable = "Objects used as evidence must be serializable.";
public static string Policy_DuplicateEvidence = "The evidence collection already contains evidence of type '{0}'. Multiple pieces of the same type of evidence are not allowed.";
public static string Policy_IncorrectHostEvidence = "Runtime host '{0}' returned evidence of type '{1}' from a request for evidence of type '{2}'.";
public static string Policy_NullHostEvidence = "Runtime host '{0}' returned null when asked for assembly evidence for assembly '{1}'.";
public static string Policy_NullHostGrantSet = "Runtime host '{0}' returned a null grant set from ResolvePolicy.";
//#endif // FEATURE_CAS_POLICY

//; Policy codegroup and permission set names and descriptions
//#if FEATURE_CAS_POLICY
public static string Policy_AllCode_Name = "All_Code";
public static string Policy_AllCode_DescriptionFullTrust = "Code group grants all code full trust and forms the root of the code group tree.";
public static string Policy_AllCode_DescriptionNothing = "Code group grants no permissions and forms the root of the code group tree.";
public static string Policy_MyComputer_Name = "My_Computer_Zone";
public static string Policy_MyComputer_Description = "Code group grants full trust to all code originating on the local computer";
public static string Policy_Intranet_Name = "LocalIntranet_Zone";
public static string Policy_Intranet_Description = "Code group grants the intranet permission set to code from the intranet zone. This permission set grants intranet code the right to use isolated storage, full UI access, some capability to do reflection, and limited access to environment variables.";
public static string Policy_IntranetNet_Name = "Intranet_Same_Site_Access";
public static string Policy_IntranetNet_Description = "All intranet code gets the right to connect back to the site of its origin.";
public static string Policy_IntranetFile_Name = "Intranet_Same_Directory_Access";
public static string Policy_IntranetFile_Description = "All intranet code gets the right to read from its install directory.";
public static string Policy_Internet_Name = "Internet_Zone";
public static string Policy_Internet_Description = "Code group grants code from the Internet zone the Internet permission set. This permission set grants Internet code the right to use isolated storage and limited UI access.";
public static string Policy_InternetNet_Name = "Internet_Same_Site_Access";
public static string Policy_InternetNet_Description = "All Internet code gets the right to connect back to the site of its origin.";
public static string Policy_Trusted_Name = "Trusted_Zone";
public static string Policy_Trusted_Description = "Code from a trusted zone is granted the Internet permission set. This permission set grants the right to use isolated storage and limited UI access.";
public static string Policy_TrustedNet_Name = "Trusted_Same_Site_Access";
public static string Policy_TrustedNet_Description = "All Trusted Code gets the right to connect back to the site of its origin.";
public static string Policy_Untrusted_Name = "Restricted_Zone";
public static string Policy_Untrusted_Description = "Code coming from a restricted zone does not receive any permissions.";
public static string Policy_Microsoft_Name = "Microsoft_Strong_Name";
public static string Policy_Microsoft_Description = "Code group grants full trust to code signed with the Microsoft strong name.";
public static string Policy_Ecma_Name = "ECMA_Strong_Name";
public static string Policy_Ecma_Description = "Code group grants full trust to code signed with the ECMA strong name.";

//; Policy permission set descriptions
public static string Policy_PS_FullTrust = "Allows full access to all resources";
public static string Policy_PS_Everything = "Allows unrestricted access to all resources covered by built-in permissions";
public static string Policy_PS_Nothing = "Denies all resources, including the right to execute";
public static string Policy_PS_Execution = "Permits execution";
public static string Policy_PS_SkipVerification = "Grants right to bypass the verification";
public static string Policy_PS_Internet = "Default rights given to Internet applications";
public static string Policy_PS_LocalIntranet = "Default rights given to applications on the local intranet";

//; default Policy level names
public static string Policy_PL_Enterprise = "Enterprise";
public static string Policy_PL_Machine = "Machine";
public static string Policy_PL_User = "User";
public static string Policy_PL_AppDomain = "AppDomain";
//#endif  // FEATURE_CAS_POLICY

//; RankException
public static string Rank_MultiDimNotSupported = "Only single dimension arrays are supported here.";
public static string Rank_MustMatch = "The specified arrays must have the same number of dimensions.";

//; TypeInitializationException
public static string TypeInitialization_Default = "Type constructor threw an exception.";
public static string TypeInitialization_Type = "The type initializer for '{0}' threw an exception.";

//; TypeLoadException


//;
//; Reflection exceptions
//;
public static string RtType_InvalidCaller = "Caller is not a friend.";

//;CustomAttributeFormatException
public static string RFLCT_InvalidPropFail = "'{0}' property specified was not found.";
public static string RFLCT_InvalidFieldFail = "'{0}' field specified was not found.";

//;InvalidFilterCriteriaException
public static string RFLCT_FltCritString = "A String must be provided for the filter criteria.";
public static string RFLCT_FltCritInt = "An Int32 must be provided for the filter criteria.";

//; TargetException
public static string RFLCT_Targ_ITargMismatch = "Object does not match target type.";
public static string RFLCT_Targ_StatMethReqTarg = "Non-static method requires a target.";
public static string RFLCT_Targ_StatFldReqTarg = "Non-static field requires a target.";

//;AmbiguousMatchException
public static string RFLCT_Ambiguous = "Ambiguous match found.";
public static string RFLCT_AmbigCust = "Multiple custom attributes of the same type found.";

//;
//; Remoting exceptions
//;
public static string Remoting_AppDomainUnloaded_ThreadUnwound = "The application domain in which the thread was running has been unloaded.";
public static string Remoting_AppDomainUnloaded = "The target application domain has been unloaded.";
public static string Remoting_CantRemotePointerType = "Pointer types cannot be passed in a remote call.";
public static string Remoting_TypeCantBeRemoted = "The given type cannot be passed in a remote call.";
public static string Remoting_Delegate_TooManyTargets = "The delegate must have only one target.";
public static string Remoting_InvalidContext = "The context is not valid.";
public static string Remoting_InvalidValueTypeFieldAccess = "An attempt was made to calculate the address of a value type field on a remote object. This was likely caused by an attempt to directly get or set the value of a field within this embedded value type. Avoid this and instead provide and use access methods for each field in the object that will be accessed remotely.";
public static string Remoting_Message_BadRetValOrOutArg = "Bad return value or out-argument inside the return message.";
public static string Remoting_NonPublicOrStaticCantBeCalledRemotely = "Permission denied: cannot call non-public or static methods remotely.";
public static string Remoting_Proxy_ProxyTypeIsNotMBR = "classToProxy argument must derive from MarshalByRef type.";
public static string Remoting_TP_NonNull = "The transparent proxy field of a real proxy must be null.";
//#if FEATURE_REMOTING
public static string Remoting_Activation_BadAttribute = "Activation attribute does not implement the IContextAttribute interface.";
public static string Remoting_Activation_BadObject = "Proxy Attribute returned an incompatible object when constructing an instance of type {0}.";
public static string Remoting_Activation_MBR_ProxyAttribute = "Proxy Attributes are supported on ContextBound types only.";
public static string Remoting_Activation_ConnectFailed = "An attempt to connect to the remote activator failed with exception '{0}'.";
public static string Remoting_Activation_Failed = "Activation failed due to an unknown reason.";
public static string Remoting_Activation_InconsistentState = "Inconsistent state during activation; there may be two proxies for the same object.";
public static string Remoting_Activation_MissingRemoteAppEntry = "Cannot find an entry for remote application '{0}'.";
public static string Remoting_Activation_NullReturnValue = "Return value of construction call was null.";
public static string Remoting_Activation_NullFromInternalUnmarshal = "InternalUnmarshal of returned ObjRef from activation call returned null.";
public static string Remoting_Activation_WellKnownCTOR = "Cannot run a non-default constructor when connecting to well-known objects.";
public static string Remoting_Activation_PermissionDenied = "Type '{0}' is not registered for activation.";
public static string Remoting_Activation_PropertyUnhappy = "A context property did not approve the candidate context for activating the object.";
public static string Remoting_Activation_AsyncUnsupported = "Async Activation not supported.";
public static string Remoting_AmbiguousCTOR = "Cannot resolve the invocation to the correct constructor.";
public static string Remoting_AmbiguousMethod = "Cannot resolve the invocation to the correct method.";
public static string Remoting_AppDomains_NYI = "This feature is not yet supported for cross-application domain.";
public static string Remoting_AppDomainsCantBeCalledRemotely = "Permission denied: cannot call methods on the AppDomain class remotely.";
public static string Remoting_AssemblyLoadFailed = "Cannot load assembly '{0}'.";
public static string Remoting_Attribute_UseAttributeNotsettable = "UseAttribute not allowed in SoapTypeAttribute.";
public static string Remoting_BadType = "Cannot load type '{0}'.";
public static string Remoting_BadField = "Remoting cannot find field '{0}' on type '{1}'.";
public static string Remoting_BadInternalState_ActivationFailure = "Invalid internal state: Activation service failed to initialize.";
public static string Remoting_BadInternalState_ProxySameAppDomain = "Invalid internal state: A marshal by ref object should not have a proxy in its own AppDomain.";
public static string Remoting_BadInternalState_FailEnvoySink = "Invalid internal state: Failed to create an envoy sink for the object.";
public static string Remoting_CantDisconnectClientProxy = "Cannot call disconnect on a proxy.";
public static string Remoting_CantInvokeIRemoteDispatch = "Cannot invoke methods on IRemoteDispatch.";
public static string Remoting_ChannelNameAlreadyRegistered = "The channel '{0}' is already registered.";
public static string Remoting_ChannelNotRegistered = "The channel '{0}' is not registered with remoting services.";
public static string Remoting_Channel_PopOnEmptySinkStack = "Tried to pop data from an empty channel sink stack.";
public static string Remoting_Channel_PopFromSinkStackWithoutPush = "A channel sink tried to pop data from the stack without first pushing data onto the stack.";
public static string Remoting_Channel_StoreOnEmptySinkStack = "A channel sink called the Store method when the sink stack was empty.";
public static string Remoting_Channel_StoreOnSinkStackWithoutPush = "A channel sink called the Store method on the sink stack without first pushing data onto the stack.";
public static string Remoting_Channel_CantCallAPRWhenStackEmpty = "Cannot call the AsyncProcessResponse method on the previous channel sink because the stack is empty.";
public static string Remoting_Channel_CantCallFRSWhenStackEmtpy = "Called FlipRememberedStack() when stack was not null.";
public static string Remoting_Channel_CantCallGetResponseStreamWhenStackEmpty = "Cannot call the GetResponseStream method on the previous channel sink because the stack is empty.";
public static string Remoting_Channel_DispatchSinkMessageMissing = "No message was deserialized prior to calling the DispatchChannelSink.";
public static string Remoting_Channel_DispatchSinkWantsNullRequestStream = "The request stream should be null when the DispatchChannelSink is called. ";
public static string Remoting_Channel_CannotBeSecured = "Channel {0} cannot be secured. Please consider using a channel that implements ISecurableChannel";
public static string Remoting_Config_ChannelMissingCtor = "To be used from a .config file, the channel type '{0}' must have a constructor of the form '{1}'";
public static string Remoting_Config_SinkProviderMissingCtor = "To be used from a .config file, the sink provider type '{0}' must have a constructor of the form '{1}'";
public static string Remoting_Config_SinkProviderNotFormatter = "A sink provider of type '{0}' is incorrectly labeled as a 'formatter'.";
public static string Remoting_Config_ConfigurationFailure = "Remoting configuration failed with the exception '{0}'.";
public static string Remoting_Config_InvalidTimeFormat = "Invalid time format '{0}'. Examples of valid time formats include 7D, 10H, 5M, 30S, or 20MS.";
public static string Remoting_Config_AppNameSet = "The remoting application name, '{0}', had already been set.";
public static string Remoting_Config_ErrorsModeSet = "The remoting custom errors mode had already been set.";
public static string Remoting_Config_CantRedirectActivationOfWellKnownService = "Attempt to redirect activation for type '{0}, {1}'. This is not allowed since either a well-known service type has already been registered with that type or that type has been registered has a activated service type.";
public static string Remoting_Config_CantUseRedirectedTypeForWellKnownService = "Attempt to register a well-known or activated service type of type '{0}, {1}'. This is not allowed since the type has already been redirected to activate elsewhere.";
public static string Remoting_Config_InvalidChannelType = "'{0}' does not implement IChannelReceiver or IChannelSender. All channels must implement one of these interfaces.";
public static string Remoting_Config_InvalidSinkProviderType = "Unable to use '{0}' as a channel sink provider. It does not implement '{1}'.";
public static string Remoting_Config_MissingWellKnownModeAttribute = "Well-known service entries must contain a 'mode' attribute with a value of 'Singleton' or 'SingleCall'.";
public static string Remoting_Config_MissingTypeAttribute = "'{0}' entries must contain a '{1}' attribute of the form 'typeName, assemblyName'.";
public static string Remoting_Config_MissingXmlTypeAttribute = "'{0}' entries must contain a '{1}' attribute of the form 'xmlTypeName, xmlTypeNamespace'.";
public static string Remoting_Config_NoAppName = "Improper remoting configuration: missing ApplicationName property.";
public static string Remoting_Config_NonTemplateIdAttribute = "Only '{0}' templates can have an 'id' attribute.";
public static string Remoting_Config_PreloadRequiresTypeOrAssembly = "Preload entries require a type or assembly attribute.";
public static string Remoting_Config_ProviderNeedsElementName = "Sink providers must have an element name of 'formatter' or 'provider'.";
public static string Remoting_Config_RequiredXmlAttribute = "'{0}' entries require a '{1}' attribute.";
public static string Remoting_Config_ReadFailure = ".Config file '{0}' cannot be read successfully due to exception '{1}'.";
public static string Remoting_Config_NodeMustBeUnique = "There can be only one '{0}' node in the '{1}' section of a config file.";
public static string Remoting_Config_TemplateCannotReferenceTemplate = "A '{0}' template cannot reference another '{0}' template.";
public static string Remoting_Config_TypeAlreadyRedirected = "Attempt to redirect activation of type '{0}, {1}' which is already redirected.";
public static string Remoting_Config_UnknownValue = "Unknown value {1} was found on the {0} node.";
public static string Remoting_Config_UnableToResolveTemplate = "Cannot resolve '{0}' template reference: '{1}'.";
public static string Remoting_Config_VersionPresent = "Version information is present in the assembly name '{0}' which is not allowed for '{1}' entries.";
public static string Remoting_Contexts_BadProperty = "A property that contributed a bad sink to the chain was found.";
public static string Remoting_Contexts_NoProperty = "A property with the name '{0}' was not found.";
public static string Remoting_Contexts_ContextNotFrozenForCallBack = "Context should be frozen before calling the DoCallBack method.";
public static string Remoting_Default = "Unknown remoting error.";
public static string Remoting_HandlerNotRegistered = "The tracking handler of type '{0}' is not registered with Remoting Services.";
public static string Remoting_InvalidMsg = "Invalid Message Object.";
public static string Remoting_InvalidCallingType = "Attempted to call a method declared on type '{0}' on an object which exposes '{1}'.";
public static string Remoting_InvalidRequestedType = "The server object type cannot be cast to the requested type '{0}'.";
public static string Remoting_InternalError = "Server encountered an internal error. For more information, turn off customErrors in the server's .config file.";
public static string Remoting_Lifetime_ILeaseReturn = "Expected a return object of type ILease, but received '{0}'.";
public static string Remoting_Lifetime_InitialStateInitialLeaseTime = "InitialLeaseTime property can only be set when the lease is in initial state. The state is '{0}'.";
public static string Remoting_Lifetime_InitialStateRenewOnCall = "RenewOnCallTime property can only be set when the lease is in initial state. The state is '{0}'.";
public static string Remoting_Lifetime_InitialStateSponsorshipTimeout = "SponsorshipTimeout property can only be set when the lease is in initial state. State is '{0}'.";
public static string Remoting_Lifetime_SetOnce = "'{0}' can only be set once within an AppDomain.";
public static string Remoting_Message_ArgMismatch = "{2} arguments were passed to '{0}::{1}'. {3} arguments were expected by this method.";
public static string Remoting_Message_BadAsyncResult = "The async result object is null or of an unexpected type.";
public static string Remoting_Message_BadType = "The method was called with a Message of an unexpected type.";
public static string Remoting_Message_CoercionFailed = "The argument type '{0}' cannot be converted into parameter type '{1}'.";
public static string Remoting_Message_MissingArgValue = "Expecting an instance of type '{0}' at pos {1} in the args array.";
public static string Remoting_Message_BadSerialization = "Invalid or malformed serialization information for the message object.";
public static string Remoting_NoIdentityEntry = "No remoting information was found for this object.";
public static string Remoting_NotRemotableByReference = "Trying to create a proxy to an unbound type.";
public static string Remoting_NullMessage = "The method was called with a null message.";
public static string Remoting_Proxy_BadType = "The proxy is of an unsupported type.";
public static string Remoting_ResetURI = "Attempt to reset the URI for an object from '{0}' to '{1}'.";
public static string Remoting_ServerObjectNotFound = "The server object for URI '{0}' is not registered with the remoting infrastructure (it may have been disconnected).";
public static string Remoting_SetObjectUriForMarshal__ObjectNeedsToBeLocal = "SetObjectUriForMarshal method should only be called for MarshalByRefObjects that exist in the current AppDomain.";
public static string Remoting_SetObjectUriForMarshal__UriExists = "SetObjectUriForMarshal method has already been called on this object or the object has already been marshaled.";
public static string Remoting_Proxy_BadReturnType = "Return argument has an invalid type.";
public static string Remoting_Proxy_ReturnValueTypeCannotBeNull = "ByRef value type parameter cannot be null.";
public static string Remoting_Proxy_BadReturnTypeForActivation = "Bad return type for activation call via Invoke: must be of type IConstructionReturnMessage.";
public static string Remoting_Proxy_BadTypeForActivation = "Type mismatch between proxy type '{0}' and activation type '{1}'.";
public static string Remoting_Proxy_ExpectedOriginalMessage = "The message passed to Invoke should be passed to PropagateOutParameters.";
public static string Remoting_Proxy_InvalidCall = "Trying to call proxy while constructor call is in progress.";
public static string Remoting_Proxy_InvalidState = "Channel sink does not exist. Failed to dispatch async call.";
public static string Remoting_Proxy_NoChannelSink = "This remoting proxy has no channel sink which means either the server has no registered server channels that are listening, or this application has no suitable client channel to talk to the server.";
public static string Remoting_Proxy_InvalidCallType = "Only the synchronous call type is supported for messages that are not of type Message.";
public static string Remoting_Proxy_WrongContext = " ExecuteMessage can be called only from the native context of the object.";
public static string Remoting_SOAPInteropxsdInvalid = "Soap Parse error, xsd:type '{0}' invalid {1}";
public static string Remoting_SOAPQNameNamespace = "SoapQName missing a Namespace value '{0}'.";
public static string Remoting_ThreadAffinity_InvalidFlag = "The specified flag '{0}' does not have one of the valid values.";
public static string Remoting_TrackingHandlerAlreadyRegistered = "The handler has already been registered with TrackingServices.";
public static string Remoting_URIClash = "Found two different objects associated with the same URI, '{0}'.";
public static string Remoting_URIExists = "The remoted object already has an associated URI.";
public static string Remoting_URIToProxy = "Trying to associate the URI with a proxy.";
public static string Remoting_WellKnown_MustBeMBR = "Attempted to create well-known object of type '{0}'. Well-known objects must derive from the MarshalByRefObject class.";
public static string Remoting_WellKnown_CtorCantMarshal = "'{0}': A well-known object cannot marshal itself in its constructor, or perform any action that would cause it to be marshaled (such as passing the 'this' pointer as a parameter to a remote method).";
public static string Remoting_WellKnown_CantDirectlyConnect = "Attempt to connect to a server using its object URI: '{0}'. A valid, complete URL must be used.";
public static string Remoting_Connect_CantCreateChannelSink = "Cannot create channel sink to connect to URL '{0}'. An appropriate channel has probably not been registered.";
public static string Remoting_UnexpectedNullTP = "Failed to create a transparent proxy. If a custom RealProxy is being used ensure it sets the proxy type.";
//; The following remoting exception messages appear in native resources too (mscorrc.rc)
public static string Remoting_Disconnected = "Object '{0}' has been disconnected or does not exist at the server.";
public static string Remoting_Message_MethodMissing = "The method '{0}' was not found on the interface/type '{1}'.";
//#endif  // FEATURE_REMOTING

//; Resources exceptions
//;
public static string Resources_StreamNotValid = "Stream is not a valid resource file.";
public static string ResourceReaderIsClosed = "ResourceReader is closed.";

//; RuntimeWrappedException
public static string RuntimeWrappedException = "An object that does not derive from System.Exception has been wrapped in a RuntimeWrappedException.";

//; UnauthorizedAccessException
public static string UnauthorizedAccess_MemStreamBuffer = "MemoryStream's internal buffer cannot be accessed.";
public static string UnauthorizedAccess_IODenied_Path = "Access to the path '{0}' is denied.";
public static string UnauthorizedAccess_IODenied_NoPathName = "Access to the path is denied.";
public static string UnauthorizedAccess_RegistryKeyGeneric_Key = "Access to the registry key '{0}' is denied.";
public static string UnauthorizedAccess_RegistryNoWrite = "Cannot write to the registry key.";
public static string UnauthorizedAccess_SystemDomain = "Cannot execute an assembly in the system domain.";

//;
//; Security exceptions
//;

//;SecurityException
//; These still appear in bcl.small but should go away eventually
public static string Security_Generic = "Request for the permission of type '{0}' failed.";
public static string Security_GenericNoType = "Request failed.";
public static string Security_NoAPTCA = "That assembly does not allow partially trusted callers.";
public static string Security_RegistryPermission = "Requested registry access is not allowed.";
public static string Security_MustRevertOverride = "Stack walk modifier must be reverted before another modification of the same type can be performed.";
//#if FEATURE_CAS_POLICY
public static string Security_CannotGenerateHash = "Hash for the assembly cannot be generated.";
public static string Security_CannotGetRawData = "Assembly bytes could not be retrieved.";
public static string Security_PrincipalPermission = "Request for principal permission failed.";
public static string Security_Action = "The action that failed was:";
public static string Security_TypeFirstPermThatFailed = "The type of the first permission that failed was:";
public static string Security_FirstPermThatFailed = "The first permission that failed was:";
public static string Security_Demanded = "The demand was for:";
public static string Security_GrantedSet = "The granted set of the failing assembly was:";
public static string Security_RefusedSet = "The refused set of the failing assembly was:";
public static string Security_Denied = "The denied permissions were:";
public static string Security_PermitOnly = "The only permitted permissions were:";
public static string Security_Assembly = "The assembly or AppDomain that failed was:";
public static string Security_Method = "The method that caused the failure was:";
public static string Security_Zone = "The Zone of the assembly that failed was:";
public static string Security_Url = "The Url of the assembly that failed was:";
public static string Security_AnonymouslyHostedDynamicMethodCheckFailed = "The demand failed due to the code access security information captured during the creation of an anonymously hosted dynamic method. In order for this operation to succeed, ensure that the demand would have succeeded at the time the method was created. See http://go.microsoft.com/fwlink/?LinkId=288746 for more information.";
//#endif  // FEATURE_CAS_POLICY

//;
//; HostProtection exceptions
//;

public static string HostProtection_HostProtection = "Attempted to perform an operation that was forbidden by the CLR host.";
public static string HostProtection_ProtectedResources = "The protected resources (only available with full trust) were:";
public static string HostProtection_DemandedResources = "The demanded resources were:";

//;
//; IO exceptions
//;

//; EOFException
public static string IO_EOF_ReadBeyondEOF = "Unable to read beyond the end of the stream.";

//; FileNotFoundException
public static string IO_FileNotFound = "Unable to find the specified file.";
public static string IO_FileNotFound_FileName = "Could not find file '{0}'.";
public static string IO_FileName_Name = "File name: '{0}'";
public static string IO_FileLoad = "Could not load the specified file.";

//; IOException
public static string IO_IO_AlreadyExists_Name = "Cannot create \"{0}\" because a file or directory with the same name already exists.";
public static string IO_IO_BindHandleFailed = "BindHandle for ThreadPool failed on this handle.";
public static string IO_IO_FileExists_Name = "The file '{0}' already exists.";
public static string IO_IO_FileStreamHandlePosition = "The OS handle's position is not what FileStream expected. Do not use a handle simultaneously in one FileStream and in Win32 code or another FileStream. This may cause data loss.";
public static string IO_IO_FileTooLong2GB = "The file is too long. This operation is currently limited to supporting files less than 2 gigabytes in size.";
public static string IO_IO_FileTooLongOrHandleNotSync = "IO operation will not work. Most likely the file will become too long or the handle was not opened to support synchronous IO operations.";
public static string IO_IO_FixedCapacity = "Unable to expand length of this stream beyond its capacity.";
public static string IO_IO_InvalidStringLen_Len = "BinaryReader encountered an invalid string length of {0} characters.";
public static string IO_IO_NoConsole = "There is no console.";
public static string IO_IO_NoPermissionToDirectoryName = "<Path discovery permission to the specified directory was denied.>";
public static string IO_IO_SeekBeforeBegin = "An attempt was made to move the position before the beginning of the stream.";
public static string IO_IO_SeekAppendOverwrite = "Unable seek backward to overwrite data that previously existed in a file opened in Append mode.";
public static string IO_IO_SetLengthAppendTruncate = "Unable to truncate data that previously existed in a file opened in Append mode.";
public static string IO_IO_SharingViolation_File = "The process cannot access the file '{0}' because it is being used by another process.";
public static string IO_IO_SharingViolation_NoFileName = "The process cannot access the file because it is being used by another process.";
public static string IO_IO_StreamTooLong = "Stream was too long.";
public static string IO_IO_CannotCreateDirectory = "The specified directory '{0}' cannot be created.";
public static string IO_IO_SourceDestMustBeDifferent = "Source and destination path must be different.";
public static string IO_IO_SourceDestMustHaveSameRoot = "Source and destination path must have identical roots. Move will not work across volumes.";

//; DirectoryNotFoundException
public static string IO_DriveNotFound_Drive = "Could not find the drive '{0}'. The drive might not be ready or might not be mapped.";
public static string IO_PathNotFound_Path = "Could not find a part of the path '{0}'.";
public static string IO_PathNotFound_NoPathName = "Could not find a part of the path.";

//; PathTooLongException
public static string IO_PathTooLong = "The specified path, file name, or both are too long. The fully qualified file name must be less than 260 characters, and the directory name must be less than 248 characters.";

//#if FEATURE_CORECLR
//; SecurityException
public static string FileSecurityState_OperationNotPermitted = "File operation not permitted. Access to path '{0}' is denied.";
//#endif

//; PrivilegeNotHeldException
public static string PrivilegeNotHeld_Default = "The process does not possess some privilege required for this operation.";
public static string PrivilegeNotHeld_Named = "The process does not possess the '{0}' privilege which is required for this operation.";

//; General strings used in the IO package
public static string IO_UnknownFileName = "[Unknown]";
public static string IO_StreamWriterBufferedDataLost = "A StreamWriter was not closed and all buffered data within that StreamWriter was not flushed to the underlying stream.  (This was detected when the StreamWriter was finalized with data in its buffer.)  A portion of the data was lost.  Consider one of calling Close(), Flush(), setting the StreamWriter's AutoFlush property to true, or allocating the StreamWriter with a \"using\" statement.  Stream type: {0}\r\nFile name: {1}\r\nAllocated from:\r\n {2}";
public static string IO_StreamWriterBufferedDataLostCaptureAllocatedFromCallstackNotEnabled = "callstack information is not captured by default for performance reasons. Please enable captureAllocatedCallStack config switch for streamWriterBufferedDataLost MDA (refer to MSDN MDA documentation for how to do this).  ";

//;
//; Serialization Exceptions
//;
//#if FEATURE_SERIALIZATION
//; SerializationException
public static string Serialization_NoID = "Object has never been assigned an objectID.";
public static string Serialization_UnknownMemberInfo = "Only FieldInfo, PropertyInfo, and SerializationMemberInfo are recognized.";
public static string Serialization_UnableToFixup = "Cannot perform fixup.";
public static string Serialization_NoType = "Object does not specify a type.";
public static string Serialization_ValueTypeFixup = "ValueType fixup on Arrays is not implemented.";
public static string Serialization_PartialValueTypeFixup = "Fixing up a partially available ValueType chain is not implemented.";
public static string Serialization_InvalidData = "An error occurred while deserializing the object.  The serialized data is corrupt.";
public static string Serialization_InvalidID = "Object specifies an invalid ID.";
public static string Serialization_InvalidPtrValue = "An IntPtr or UIntPtr with an eight byte value cannot be deserialized on a machine with a four byte word size.";
public static string Serialization_DuplicateSelector = "Selector is already on the list of checked selectors.";
public static string Serialization_MemberTypeNotRecognized = "Unknown member type.";
public static string Serialization_NoBaseType = "Object does not specify a base type.";
public static string Serialization_ArrayNoLength = "Array does not specify a length.";
public static string Serialization_CannotGetType = "Cannot get the type '{0}'.";
public static string Serialization_AssemblyNotFound = "Unable to find assembly '{0}'.";
public static string Serialization_ArrayInvalidLength = "Array specifies an invalid length.";
public static string Serialization_MalformedArray = "The array information in the stream is invalid.";
public static string Serialization_InsufficientState = "Insufficient state to return the real object.";
public static string Serialization_InvalidFieldState = "Object fields may not be properly initialized.";
public static string Serialization_MissField = "Field {0} is missing.";
public static string Serialization_MultipleMembers = "Cannot resolve multiple members with the same name.";
public static string Serialization_NullSignature = "The method signature cannot be null.";
public static string Serialization_ObjectUsedBeforeDeserCallback = "An object was used before its deserialization callback ran, which may break higher-level consistency guarantees in the application.";
public static string Serialization_UnknownMember = "Cannot get the member '{0}'.";
public static string Serialization_RegisterTwice = "An object cannot be registered twice.";
public static string Serialization_IdTooSmall = "Object IDs must be greater than zero.";
public static string Serialization_NotFound = "Member '{0}' was not found.";
public static string Serialization_InsufficientDeserializationState = "Insufficient state to deserialize the object. Missing field '{0}'. More information is needed.";
public static string Serialization_UnableToFindModule = "The given module {0} cannot be found within the assembly {1}.";
public static string Serialization_TooManyReferences = "The implementation of the IObjectReference interface returns too many nested references to other objects that implement IObjectReference.";
public static string Serialization_NotISer = "The given object does not implement the ISerializable interface.";
public static string Serialization_InvalidOnDeser = "OnDeserialization method was called while the object was not being deserialized.";
public static string Serialization_MissingKeys = "The Keys for this Hashtable are missing.";
public static string Serialization_MissingKeyValuePairs = "The KeyValuePairs for this Dictionary are missing.";
public static string Serialization_MissingValues = "The values for this dictionary are missing.";
public static string Serialization_NullKey = "One of the serialized keys is null.";
public static string Serialization_KeyValueDifferentSizes = "The keys and values arrays have different sizes.";
public static string Serialization_SurrogateCycleInArgument = "Selector contained a cycle.";
public static string Serialization_SurrogateCycle = "Adding selector will introduce a cycle.";
public static string Serialization_NeverSeen = "A fixup is registered to the object with ID {0}, but the object does not appear in the graph.";
public static string Serialization_IORIncomplete = "The object with ID {0} implements the IObjectReference interface for which all dependencies cannot be resolved. The likely cause is two instances of IObjectReference that have a mutual dependency on each other.";
public static string Serialization_NotCyclicallyReferenceableSurrogate = "{0}.SetObjectData returns a value that is neither null nor equal to the first parameter. Such Surrogates cannot be part of cyclical reference.";
public static string Serialization_ObjectNotSupplied = "The object with ID {0} was referenced in a fixup but does not exist.";
public static string Serialization_TooManyElements = "The internal array cannot expand to greater than Int32.MaxValue elements.";
public static string Serialization_SameNameTwice = "Cannot add the same member twice to a SerializationInfo object.";
public static string Serialization_InvalidType = "Only system-provided types can be passed to the GetUninitializedObject method. '{0}' is not a valid instance of a type.";
public static string Serialization_MissingObject = "The object with ID {0} was referenced in a fixup but has not been registered.";
public static string Serialization_InvalidFixupType = "A member fixup was registered for an object which implements ISerializable or has a surrogate. In this situation, a delayed fixup must be used.";
public static string Serialization_InvalidFixupDiscovered = "A fixup on an object implementing ISerializable or having a surrogate was discovered for an object which does not have a SerializationInfo available.";
public static string Serialization_InvalidFormat = "The input stream is not a valid binary format. The starting contents (in bytes) are: {0} ...";
public static string Serialization_ParentChildIdentical = "The ID of the containing object cannot be the same as the object ID.";
public static string Serialization_IncorrectNumberOfFixups = "The ObjectManager found an invalid number of fixups. This usually indicates a problem in the Formatter.";
public static string Serialization_BadParameterInfo = "Non existent ParameterInfo. Position bigger than member's parameters length.";
public static string Serialization_NoParameterInfo = "Serialized member does not have a ParameterInfo.";
public static string Serialization_StringBuilderMaxCapacity = "The serialized MaxCapacity property of StringBuilder must be positive and greater than or equal to the String length.";
public static string Serialization_StringBuilderCapacity = "The serialized Capacity property of StringBuilder must be positive, less than or equal to MaxCapacity and greater than or equal to the String length.";
public static string Serialization_InvalidDelegateType = "Cannot serialize delegates over unmanaged function pointers, dynamic methods or methods outside the delegate creator's assembly.";
public static string Serialization_OptionalFieldVersionValue = "Version value must be positive.";
public static string Serialization_MissingDateTimeData = "Invalid serialized DateTime data. Unable to find 'ticks' or 'dateData'.";
public static string Serialization_DateTimeTicksOutOfRange = "Invalid serialized DateTime data. Ticks must be between DateTime.MinValue.Ticks and DateTime.MaxValue.Ticks. ";
//; The following serialization exception messages appear in native resources too (mscorrc.rc)
public static string Serialization_NonSerType = "Type '{0}' in Assembly '{1}' is not marked as serializable.";
public static string Serialization_ConstructorNotFound = "The constructor to deserialize an object of type '{0}' was not found.";

//; SerializationException used by Formatters
public static string Serialization_ArrayType = "Invalid array type '{0}'.";
public static string Serialization_ArrayTypeObject = "Array element type is Object, 'dt' attribute is null.";
public static string Serialization_Assembly = "No assembly information is available for object on the wire, '{0}'.";
public static string Serialization_AssemblyId = "No assembly ID for object type '{0}'.";
public static string Serialization_BinaryHeader = "Binary stream '{0}' does not contain a valid BinaryHeader. Possible causes are invalid stream or object version change between serialization and deserialization.";
public static string Serialization_CrossAppDomainError = "Cross-AppDomain BinaryFormatter error; expected '{0}' but received '{1}'.";
public static string Serialization_CorruptedStream = "Invalid BinaryFormatter stream.";
public static string Serialization_HeaderReflection = "Header reflection error: number of value members: {0}.";
public static string Serialization_ISerializableTypes = "Types not available for ISerializable object '{0}'.";
public static string Serialization_ISerializableMemberInfo = "MemberInfo requested for ISerializable type.";
public static string Serialization_MBRAsMBV = "Type {0} must be marshaled by reference in this context.";
public static string Serialization_Map = "No map for object '{0}'.";
public static string Serialization_MemberInfo = "MemberInfo cannot be obtained for ISerialized Object '{0}'.";
public static string Serialization_Method = "Invalid MethodCall or MethodReturn stream format.";
public static string Serialization_MissingMember = "Member '{0}' in class '{1}' is not present in the serialized stream and is not marked with {2}.";
public static string Serialization_NoMemberInfo = "No MemberInfo for Object {0}.";
public static string Serialization_ObjNoID = "Object {0} has never been assigned an objectID.";
public static string Serialization_ObjectTypeEnum = "Invalid ObjectTypeEnum {0}.";
public static string Serialization_ParseError = "Parse error. Current element is not compatible with the next element, {0}.";
public static string Serialization_SerMemberInfo = "MemberInfo type {0} cannot be serialized.";
public static string Serialization_Stream = "Attempting to deserialize an empty stream.";
public static string Serialization_StreamEnd = "End of Stream encountered before parsing was completed.";
public static string Serialization_TopObject = "No top object.";
public static string Serialization_TopObjectInstantiate = "Top object cannot be instantiated for element '{0}'.";
public static string Serialization_TypeCode = "Invalid type code in stream '{0}'.";
public static string Serialization_TypeExpected = "Invalid expected type.";
public static string Serialization_TypeMissing = "Type is missing for member of type Object '{0}'.";
public static string Serialization_TypeRead = "Invalid read type request '{0}'.";
public static string Serialization_TypeSecurity = "Type {0} and the types derived from it (such as {1}) are not permitted to be deserialized at this security level.";
public static string Serialization_TypeWrite = "Invalid write type request '{0}'.";
public static string Serialization_XMLElement = "Invalid element '{0}'.";
public static string Serialization_Security = "Because of security restrictions, the type {0} cannot be accessed.";
public static string Serialization_TypeLoadFailure = "Unable to load type {0} required for deserialization.";
public static string Serialization_RequireFullTrust = "A type '{0}' that is defined in a partially trusted assembly cannot be type forwarded from an assembly with a different Public Key Token or without a public key token. To fix this, please either turn on unsafeTypeForwarding flag in the configuration file or remove the TypeForwardedFrom attribute.";
//; The following serialization exception messages appear in native resources too (mscorrc.rc)
public static string Serialization_TypeResolved = "Type is not resolved for member '{0}'.";
public static string Serialization_MemberOutOfRange = "The deserialized value of the member \"{0}\" in the class \"{1}\" is out of range.";
//#endif  // FEATURE_SERIALIZATION

//;
//; StringBuilder Exceptions
//;
public static string Arg_LongerThanSrcString = "Source string was not long enough. Check sourceIndex and count.";


//;
//; System.Threading
//;

//;
//; Thread Exceptions
//;
public static string ThreadState_NoAbortRequested = "Unable to reset abort because no abort was requested.";
public static string Threading_WaitHandleTooManyPosts = "The WaitHandle cannot be signaled because it would exceed its maximum count.";
//;
//; WaitHandleCannotBeOpenedException
//;
public static string Threading_WaitHandleCannotBeOpenedException = "No handle of the given name exists.";
public static string Threading_WaitHandleCannotBeOpenedException_InvalidHandle = "A WaitHandle with system-wide name '{0}' cannot be created. A WaitHandle of a different type might have the same name.";

//;
//; AbandonedMutexException
//;
public static string Threading_AbandonedMutexException = "The wait completed due to an abandoned mutex.";

//; AggregateException
public static string AggregateException_ctor_DefaultMessage = "One or more errors occurred.";
public static string AggregateException_ctor_InnerExceptionNull = "An element of innerExceptions was null.";
public static string AggregateException_DeserializationFailure = "The serialization stream contains no inner exceptions.";
public static string AggregateException_ToString = "{0}{1}---> (Inner Exception #{2}) {3}{4}{5}";

//; Cancellation
public static string CancellationToken_CreateLinkedToken_TokensIsEmpty = "No tokens were supplied.";
public static string CancellationTokenSource_Disposed = "The CancellationTokenSource has been disposed.";
public static string CancellationToken_SourceDisposed = "The CancellationTokenSource associated with this CancellationToken has been disposed.";

//; Exceptions shared by all concurrent collection
public static string ConcurrentCollection_SyncRoot_NotSupported = "The SyncRoot property may not be used for the synchronization of concurrent collections.";

//; Exceptions shared by ConcurrentStack and ConcurrentQueue
public static string ConcurrentStackQueue_OnDeserialization_NoData = "The serialization stream contains no elements.";

//; ConcurrentStack<T>
public static string ConcurrentStack_PushPopRange_StartOutOfRange = "The startIndex argument must be greater than or equal to zero.";
public static string ConcurrentStack_PushPopRange_CountOutOfRange = "The count argument must be greater than or equal to zero.";
public static string ConcurrentStack_PushPopRange_InvalidCount = "The sum of the startIndex and count arguments must be less than or equal to the collection's Count.";

//; ConcurrentDictionary<TKey, TValue>
public static string ConcurrentDictionary_ItemKeyIsNull = "TKey is a reference type and item.Key is null.";
public static string ConcurrentDictionary_SourceContainsDuplicateKeys = "The source argument contains duplicate keys.";
public static string ConcurrentDictionary_IndexIsNegative = "The index argument is less than zero.";
public static string ConcurrentDictionary_ConcurrencyLevelMustBePositive = "The concurrencyLevel argument must be positive.";
public static string ConcurrentDictionary_CapacityMustNotBeNegative = "The capacity argument must be greater than or equal to zero.";
public static string ConcurrentDictionary_ArrayNotLargeEnough = "The index is equal to or greater than the length of the array, or the number of elements in the dictionary is greater than the available space from index to the end of the destination array.";
public static string ConcurrentDictionary_ArrayIncorrectType = "The array is multidimensional, or the type parameter for the set cannot be cast automatically to the type of the destination array.";
public static string ConcurrentDictionary_KeyAlreadyExisted = "The key already existed in the dictionary.";
public static string ConcurrentDictionary_TypeOfKeyIncorrect = "The key was of an incorrect type for this dictionary.";
public static string ConcurrentDictionary_TypeOfValueIncorrect = "The value was of an incorrect type for this dictionary.";

//; Partitioner
public static string Partitioner_DynamicPartitionsNotSupported = "Dynamic partitions are not supported by this partitioner.";

//; OrderablePartitioner
public static string OrderablePartitioner_GetPartitions_WrongNumberOfPartitions = "GetPartitions returned an incorrect number of partitions.";

//; PartitionerStatic
public static string PartitionerStatic_CurrentCalledBeforeMoveNext = "MoveNext must be called at least once before calling Current.";
public static string PartitionerStatic_CanNotCallGetEnumeratorAfterSourceHasBeenDisposed = "Can not call GetEnumerator on partitions after the source enumerable is disposed";

//; CDSCollectionETWBCLProvider events
public static string event_ConcurrentStack_FastPushFailed = "Push to ConcurrentStack spun {0} time(s).";
public static string event_ConcurrentStack_FastPopFailed = "Pop from ConcurrentStack spun {0} time(s).";
public static string event_ConcurrentDictionary_AcquiringAllLocks = "ConcurrentDictionary acquiring all locks on {0} bucket(s).";
public static string event_ConcurrentBag_TryTakeSteals = "ConcurrentBag stealing in TryTake.";
public static string event_ConcurrentBag_TryPeekSteals = "ConcurrentBag stealing in TryPeek.";

//; CountdownEvent
public static string CountdownEvent_Decrement_BelowZero = "Invalid attempt made to decrement the event's count below zero.";
public static string CountdownEvent_Increment_AlreadyZero = "The event is already signaled and cannot be incremented.";
public static string CountdownEvent_Increment_AlreadyMax = "The increment operation would cause the CurrentCount to overflow.";

//; Parallel
public static string Parallel_Invoke_ActionNull = "One of the actions was null.";
public static string Parallel_ForEach_OrderedPartitionerKeysNotNormalized = "This method requires the use of an OrderedPartitioner with the KeysNormalized property set to true.";
public static string Parallel_ForEach_PartitionerNotDynamic = "The Partitioner used here must support dynamic partitioning.";
public static string Parallel_ForEach_PartitionerReturnedNull = "The Partitioner used here returned a null partitioner source.";
public static string Parallel_ForEach_NullEnumerator = "The Partitioner source returned a null enumerator.";

//; SemaphyoreFullException
public static string Threading_SemaphoreFullException = "Adding the specified count to the semaphore would cause it to exceed its maximum count.";

//; Lazy
public static string Lazy_ctor_ValueSelectorNull = "The valueSelector argument is null.";
public static string Lazy_ctor_InfoNull = "The info argument is null.";
public static string Lazy_ctor_deserialization_ValueInvalid = "The Value cannot be null.";
public static string Lazy_ctor_ModeInvalid = "The mode argument specifies an invalid value.";
public static string Lazy_CreateValue_NoParameterlessCtorForT = "The lazily-initialized type does not have a public, parameterless constructor.";
public static string Lazy_StaticInit_InvalidOperation = "ValueFactory returned null.";
public static string Lazy_Value_RecursiveCallsToValue = "ValueFactory attempted to access the Value property of this instance.";
public static string Lazy_ToString_ValueNotCreated = "Value is not created.";


//;ThreadLocal
public static string ThreadLocal_Value_RecursiveCallsToValue = "ValueFactory attempted to access the Value property of this instance.";
public static string ThreadLocal_Disposed = "The ThreadLocal object has been disposed.";
public static string ThreadLocal_ValuesNotAvailable = "The ThreadLocal object is not tracking values. To use the Values property, use a ThreadLocal constructor that accepts the trackAllValues parameter and set the parameter to true.";

//; SemaphoreSlim
public static string SemaphoreSlim_ctor_InitialCountWrong = "The initialCount argument must be non-negative and less than or equal to the maximumCount.";
public static string SemaphoreSlim_ctor_MaxCountWrong = "The maximumCount argument must be a positive number. If a maximum is not required, use the constructor without a maxCount parameter.";
public static string SemaphoreSlim_Wait_TimeoutWrong = "The timeout must represent a value between -1 and Int32.MaxValue, inclusive.";
public static string SemaphoreSlim_Release_CountWrong = "The releaseCount argument must be greater than zero.";
public static string SemaphoreSlim_Disposed = "The semaphore has been disposed.";

//; ManualResetEventSlim
public static string ManualResetEventSlim_ctor_SpinCountOutOfRange = "The spinCount argument must be in the range 0 to {0}, inclusive.";
public static string ManualResetEventSlim_ctor_TooManyWaiters = "There are too many threads currently waiting on the event. A maximum of {0} waiting threads are supported.";
public static string ManualResetEventSlim_Disposed = "The event has been disposed.";

//; SpinLock
public static string SpinLock_TryEnter_ArgumentOutOfRange = "The timeout must be a value between -1 and Int32.MaxValue, inclusive.";
public static string SpinLock_TryEnter_LockRecursionException = "The calling thread already holds the lock.";
public static string SpinLock_TryReliableEnter_ArgumentException = "The tookLock argument must be set to false before calling this method.";
public static string SpinLock_Exit_SynchronizationLockException = "The calling thread does not hold the lock.";
public static string SpinLock_IsHeldByCurrentThread = "Thread tracking is disabled.";

//; SpinWait
public static string SpinWait_SpinUntil_TimeoutWrong = "The timeout must represent a value between -1 and Int32.MaxValue, inclusive.";
public static string SpinWait_SpinUntil_ArgumentNull = "The condition argument is null.";

//; CdsSyncEtwBCLProvider events
public static string event_SpinLock_FastPathFailed = "SpinLock beginning to spin.";
public static string event_SpinWait_NextSpinWillYield = "Next spin will yield.";
public static string event_Barrier_PhaseFinished = "Barrier finishing phase {1}.";

//;
//; System.Threading.Tasks
//;

//; AsyncMethodBuilder
public static string AsyncMethodBuilder_InstanceNotInitialized = "The builder was not properly initialized.";

//; TaskAwaiter and YieldAwaitable
public static string AwaitableAwaiter_InstanceNotInitialized = "The awaitable or awaiter was not properly initialized.";
public static string TaskAwaiter_TaskNotCompleted = "The awaited task has not yet completed.";

//; Task<T>
public static string TaskT_SetException_HasAnInitializer = "A task's Exception may only be set directly if the task was created without a function.";
public static string TaskT_TransitionToFinal_AlreadyCompleted = "An attempt was made to transition a task to a final state when it had already completed.";
public static string TaskT_ctor_SelfReplicating = "It is invalid to specify TaskCreationOptions.SelfReplicating for a Task<TResult>.";
public static string TaskT_DebuggerNoResult = "{Not yet computed}";

//; Task
public static string Task_ctor_LRandSR = "(Internal)An attempt was made to create a LongRunning SelfReplicating task.";
public static string Task_ThrowIfDisposed = "The task has been disposed.";
public static string Task_Dispose_NotCompleted = "A task may only be disposed if it is in a completion state (RanToCompletion, Faulted or Canceled).";
public static string Task_Start_Promise = "Start may not be called on a promise-style task.";
public static string Task_Start_AlreadyStarted = "Start may not be called on a task that was already started.";
public static string Task_Start_TaskCompleted = "Start may not be called on a task that has completed.";
public static string Task_Start_ContinuationTask = "Start may not be called on a continuation task.";
public static string Task_RunSynchronously_AlreadyStarted = "RunSynchronously may not be called on a task that was already started.";
public static string Task_RunSynchronously_TaskCompleted = "RunSynchronously may not be called on a task that has already completed.";
public static string Task_RunSynchronously_Promise = "RunSynchronously may not be called on a task not bound to a delegate, such as the task returned from an asynchronous method.";
public static string Task_RunSynchronously_Continuation = "RunSynchronously may not be called on a continuation task.";
public static string Task_ContinueWith_NotOnAnything = "The specified TaskContinuationOptions excluded all continuation kinds.";
public static string Task_ContinueWith_ESandLR = "The specified TaskContinuationOptions combined LongRunning and ExecuteSynchronously.  Synchronous continuations should not be long running.";
public static string Task_MultiTaskContinuation_NullTask = "The tasks argument included a null value.";
public static string Task_MultiTaskContinuation_FireOptions = "It is invalid to exclude specific continuation kinds for continuations off of multiple tasks.";
public static string Task_MultiTaskContinuation_EmptyTaskList = "The tasks argument contains no tasks.";
public static string Task_FromAsync_TaskManagerShutDown = "FromAsync was called with a TaskManager that had already shut down.";
public static string Task_FromAsync_SelfReplicating = "It is invalid to specify TaskCreationOptions.SelfReplicating in calls to FromAsync.";
public static string Task_FromAsync_LongRunning = "It is invalid to specify TaskCreationOptions.LongRunning in calls to FromAsync.";
public static string Task_FromAsync_PreferFairness = "It is invalid to specify TaskCreationOptions.PreferFairness in calls to FromAsync.";
public static string Task_WaitMulti_NullTask = "The tasks array included at least one null element.";
public static string Task_Delay_InvalidMillisecondsDelay = "The value needs to be either -1 (signifying an infinite timeout), 0 or a positive integer.";
public static string Task_Delay_InvalidDelay = "The value needs to translate in milliseconds to -1 (signifying an infinite timeout), 0 or a positive integer less than or equal to Int32.MaxValue.";

//; TaskCanceledException
public static string TaskCanceledException_ctor_DefaultMessage = "A task was canceled.";

//;TaskCompletionSource<T>
public static string TaskCompletionSourceT_TrySetException_NullException = "The exceptions collection included at least one null element.";
public static string TaskCompletionSourceT_TrySetException_NoExceptions = "The exceptions collection was empty.";

//;TaskExceptionHolder
public static string TaskExceptionHolder_UnknownExceptionType = "(Internal)Expected an Exception or an IEnumerable<Exception>";
public static string TaskExceptionHolder_UnhandledException = "A Task's exception(s) were not observed either by Waiting on the Task or accessing its Exception property. As a result, the unobserved exception was rethrown by the finalizer thread.";

//; TaskScheduler
public static string TaskScheduler_ExecuteTask_TaskAlreadyExecuted = "ExecuteTask may not be called for a task which was already executed.";
public static string TaskScheduler_ExecuteTask_WrongTaskScheduler = "ExecuteTask may not be called for a task which was previously queued to a different TaskScheduler.";
public static string TaskScheduler_InconsistentStateAfterTryExecuteTaskInline = "The TryExecuteTaskInline call to the underlying scheduler succeeded, but the task body was not invoked.";
public static string TaskScheduler_FromCurrentSynchronizationContext_NoCurrent = "The current SynchronizationContext may not be used as a TaskScheduler.";

//; TaskSchedulerException
public static string TaskSchedulerException_ctor_DefaultMessage = "An exception was thrown by a TaskScheduler.";

//;
//; ParallelState ( used in Parallel.For(), Parallel.ForEach() )
public static string ParallelState_Break_InvalidOperationException_BreakAfterStop = "Break was called after Stop was called.";
public static string ParallelState_Stop_InvalidOperationException_StopAfterBreak = "Stop was called after Break was called.";
public static string ParallelState_NotSupportedException_UnsupportedMethod = "This method is not supported.";

//;
//; TPLETWProvider events
public static string event_ParallelLoopBegin = "Beginning {3} loop {2} from Task {1}.";
public static string event_ParallelLoopEnd = "Ending loop {2} after {3} iterations.";
public static string event_ParallelInvokeBegin = "Beginning ParallelInvoke {2} from Task {1} for {4} actions.";
public static string event_ParallelInvokeEnd = "Ending ParallelInvoke {2}.";
public static string event_ParallelFork = "Task {1} entering fork/join {2}.";
public static string event_ParallelJoin = "Task {1} leaving fork/join {2}.";
public static string event_TaskScheduled = "Task {2} scheduled to TaskScheduler {0}.";
public static string event_TaskStarted = "Task {2} executing.";
public static string event_TaskCompleted = "Task {2} completed.";
public static string event_TaskWaitBegin = "Beginning wait ({3}) on Task {2}.";
public static string event_TaskWaitEnd = "Ending wait on Task {2}.";


//;
//; Weak Reference Exception
//;
public static string WeakReference_NoLongerValid  = "The weak reference is no longer valid.";


//;
//; Interop Exceptions
//;
public static string Interop_COM_TypeMismatch  = "Type mismatch between source and destination types.";
public static string Interop_Marshal_Unmappable_Char  = "Cannot marshal: Encountered unmappable character.";

//#if FEATURE_COMINTEROP_WINRT_DESKTOP_HOST
public static string WinRTHostDomainName  = "Windows Runtime Object Host Domain for '{0}'";
//#endif

//;
//; Loader Exceptions
//;
public static string Loader_InvalidPath  = "Relative path must be a string that contains the substring, \"..\", or does not contain a root directory.";
public static string Loader_Name  = "Name:";
public static string Loader_NoContextPolicies  = "There are no context policies.";
public static string Loader_ContextPolicies  = "Context Policies:";

//;
//; AppDomain Exceptions
public static string AppDomain_RequireApplicationName  = "ApplicationName must be set before the DynamicBase can be set.";
public static string AppDomain_AppBaseNotSet  = "The ApplicationBase must be set before retrieving this property.";

//#if FEATURE_HOST_ASSEMBLY_RESOLVER
public static string AppDomain_BindingModelIsLocked  = "Binding model is already locked for the AppDomain and cannot be reset.";
public static string Argument_CustomAssemblyLoadContextRequestedNameMismatch  = "Resolved assembly's simple name should be the same as of the requested assembly.";
//#endif // FEATURE_HOST_ASSEMBLY_RESOLVER
//;
//; XMLSyntaxExceptions
public static string XMLSyntax_UnexpectedEndOfFile  = "Unexpected end of file.";
public static string XMLSyntax_ExpectedCloseBracket  = "Expected > character.";
public static string XMLSyntax_ExpectedSlashOrString  = "Expected / character or string.";
public static string XMLSyntax_UnexpectedCloseBracket  = "Unexpected > character.";
public static string XMLSyntax_SyntaxError  = "Invalid syntax on line {0}.";
public static string XMLSyntax_SyntaxErrorEx  = "Invalid syntax on line {0} - '{1}'.";
public static string XMLSyntax_InvalidSyntax  = "Invalid syntax.";
public static string XML_Syntax_InvalidSyntaxInFile  = "Invalid XML in file '{0}' near element '{1}'.";
public static string XMLSyntax_InvalidSyntaxSatAssemTag  = "Invalid XML in file \"{0}\" near element \"{1}\". The <satelliteassemblies> section only supports <assembly> tags.";
public static string XMLSyntax_InvalidSyntaxSatAssemTagBadAttr  = "Invalid XML in file \"{0}\" near \"{1}\" and \"{2}\". In the <satelliteassemblies> section, the <assembly> tag must have exactly 1 attribute called 'name', whose value is a fully-qualified assembly name.";
public static string XMLSyntax_InvalidSyntaxSatAssemTagNoAttr  = "Invalid XML in file \"{0}\". In the <satelliteassemblies> section, the <assembly> tag must have exactly 1 attribute called 'name', whose value is a fully-qualified assembly name.";

//; CodeGroup
//#if FEATURE_CAS_POLICY
public static string NetCodeGroup_PermissionSet  = "Same site Web";
public static string MergeLogic_Union  = "Union";
public static string MergeLogic_FirstMatch  = "First Match";
public static string FileCodeGroup_PermissionSet  = "Same directory FileIO - '{0}'";
//#endif // FEATURE_CAS_POLICY

//; MembershipConditions
public static string StrongName_ToString  = "StrongName - {0}{1}{2}";
public static string StrongName_Name  = "name = {0}";
public static string StrongName_Version  = "version = {0}";
public static string Site_ToString  = "Site";
public static string Publisher_ToString  = "Publisher";
public static string Hash_ToString  = "Hash - {0} = {1}";
public static string ApplicationDirectory_ToString  = "ApplicationDirectory";
public static string Zone_ToString  = "Zone - {0}";
public static string All_ToString  = "All code";
public static string Url_ToString  = "Url";
public static string GAC_ToString  = "GAC";
//#if FEATURE_CAS_POLICY
public static string Site_ToStringArg  = "Site - {0}";
public static string Publisher_ToStringArg  = "Publisher - {0}";
public static string Url_ToStringArg  = "Url - {0}";
//#endif // FEATURE_CAS_POLICY


//; Interop non exception strings.
public static string TypeLibConverter_ImportedTypeLibProductName  = "Assembly imported from type library '{0}'.";

//;
//; begin System.TimeZoneInfo ArgumentException's
//;
public static string Argument_AdjustmentRulesNoNulls  = "The AdjustmentRule array cannot contain null elements.";
public static string Argument_AdjustmentRulesOutOfOrder  = "The elements of the AdjustmentRule array must be in chronological order and must not overlap.";
public static string Argument_AdjustmentRulesAmbiguousOverlap  = "The elements of the AdjustmentRule array must not contain ambiguous time periods that extend beyond the DateStart or DateEnd properties of the element.";
public static string Argument_AdjustmentRulesrDaylightSavingTimeOverlap  = "The elements of the AdjustmentRule array must not contain Daylight Saving Time periods that overlap adjacent elements in such a way as to cause invalid or ambiguous time periods.";
public static string Argument_AdjustmentRulesrDaylightSavingTimeOverlapNonRuleRange  = "The elements of the AdjustmentRule array must not contain Daylight Saving Time periods that overlap the DateStart or DateEnd properties in such a way as to cause invalid or ambiguous time periods.";
public static string Argument_AdjustmentRulesInvalidOverlap  = "The elements of the AdjustmentRule array must not contain invalid time periods that extend beyond the DateStart or DateEnd properties of the element. ";
public static string Argument_ConvertMismatch  = "The conversion could not be completed because the supplied DateTime did not have the Kind property set correctly.  For example, when the Kind property is DateTimeKind.Local, the source time zone must be TimeZoneInfo.Local.";
public static string Argument_DateTimeHasTimeOfDay  = "The supplied DateTime includes a TimeOfDay setting.   This is not supported.";
public static string Argument_DateTimeIsInvalid  = "The supplied DateTime represents an invalid time.  For example, when the clock is adjusted forward, any time in the period that is skipped is invalid.";
public static string Argument_DateTimeIsNotAmbiguous  = "The supplied DateTime is not in an ambiguous time range.";
public static string Argument_DateTimeOffsetIsNotAmbiguous  = "The supplied DateTimeOffset is not in an ambiguous time range.";
public static string Argument_DateTimeKindMustBeUnspecified  = "The supplied DateTime must have the Kind property set to DateTimeKind.Unspecified.";
public static string Argument_DateTimeHasTicks  = "The supplied DateTime must have the Year, Month, and Day properties set to 1.  The time cannot be specified more precisely than whole milliseconds.";
public static string Argument_InvalidId  = "The specified ID parameter '{0}' is not supported.";
public static string Argument_InvalidSerializedString  = "The specified serialized string '{0}' is not supported.";
public static string Argument_InvalidREG_TZI_FORMAT  = "The REG_TZI_FORMAT structure is corrupt.";
public static string Argument_OutOfOrderDateTimes  = "The DateStart property must come before the DateEnd property.";
public static string Argument_TimeSpanHasSeconds  = "The TimeSpan parameter cannot be specified more precisely than whole minutes.";
public static string Argument_TimeZoneInfoBadTZif  = "The tzfile does not begin with the magic characters 'TZif'.  Please verify that the file is not corrupt.";
public static string Argument_TimeZoneInfoInvalidTZif  = "The TZif data structure is corrupt.";
public static string Argument_TransitionTimesAreIdentical  = "The DaylightTransitionStart property must not equal the DaylightTransitionEnd property.";
//;
//; begin System.TimeZoneInfo ArgumentOutOfRangeException's
//;
public static string ArgumentOutOfRange_DayParam  = "The Day parameter must be in the range 1 through 31.";
public static string ArgumentOutOfRange_DayOfWeek  = "The DayOfWeek enumeration must be in the range 0 through 6.";
public static string ArgumentOutOfRange_MonthParam  = "The Month parameter must be in the range 1 through 12.";
public static string ArgumentOutOfRange_UtcOffset  = "The TimeSpan parameter must be within plus or minus 14.0 hours.";
public static string ArgumentOutOfRange_UtcOffsetAndDaylightDelta  = "The sum of the BaseUtcOffset and DaylightDelta properties must within plus or minus 14.0 hours.";
public static string ArgumentOutOfRange_Week  = "The Week parameter must be in the range 1 through 5.";
//;
//; begin System.TimeZoneInfo InvalidTimeZoneException's
//;
public static string InvalidTimeZone_InvalidRegistryData  = "The time zone ID '{0}' was found on the local computer, but the registry information was corrupt.";
public static string InvalidTimeZone_InvalidWin32APIData  = "The Local time zone was found on the local computer, but the data was corrupt.";
//;
//; begin System.TimeZoneInfo SecurityException's
//;
public static string Security_CannotReadRegistryData  = "The time zone ID '{0}' was found on the local computer, but the application does not have permission to read the registry information.";
//;
//; begin System.TimeZoneInfo SerializationException's
//;
public static string Serialization_CorruptField  = "The value of the field '{0}' is invalid.  The serialized data is corrupt.  ";
public static string Serialization_InvalidEscapeSequence  = "The serialized data contained an invalid escape sequence '\\{0}'.";
//;
//; begin System.TimeZoneInfo TimeZoneNotFoundException's
//;
public static string TimeZoneNotFound_MissingRegistryData  = "The time zone ID '{0}' was not found on the local computer.";
//;
//; end System.TimeZoneInfo
//;


//; Tuple
public static string ArgumentException_TupleIncorrectType = "Argument must be of type {0}.";
public static string ArgumentException_TupleNonIComparableElement = "The tuple contains an element of type {0} which does not implement the IComparable interface.";
public static string ArgumentException_TupleLastArgumentNotATuple = "The last element of an eight element Tuple must be a Tuple.";
public static string ArgumentException_OtherNotArrayOfCorrectLength = "Object is not a array with the same number of elements as the array to compare it to.";

//; WinRT collection adapters
public static string Argument_IndexOutOfArrayBounds = "The specified index is out of bounds of the specified array.";
public static string Argument_InsufficientSpaceToCopyCollection = "The specified space is not sufficient to copy the elements from this Collection.";
public static string ArgumentOutOfRange_IndexLargerThanMaxValue = "This collection cannot work with indices larger than Int32.MaxValue - 1 (0x7FFFFFFF - 1).";
public static string ArgumentOutOfRange_IndexOutOfRange = "The specified index is outside the current index range of this collection.";
public static string InvalidOperation_CollectionBackingListTooLarge = "The collection backing this List contains too many elements.";
public static string InvalidOperation_CollectionBackingDictionaryTooLarge = "The collection backing this Dictionary contains too many elements.";
public static string InvalidOperation_CannotRemoveLastFromEmptyCollection = "Cannot remove the last element from an empty collection.";


//;------------------

//; ValueTuple
public static string ArgumentException_ValueTupleIncorrectType = "Argument must be of type {0}.";
public static string ArgumentException_ValueTupleLastArgumentNotAValueTuple = "The last element of an eight element ValueTuple must be a ValueTuple.";

//; ThreadPoolBoundHandle 
public static string Argument_PreAllocatedAlreadyAllocated = "'preAllocated' is already in use.";
public static string InvalidOperation_NativeOverlappedReused = "NativeOverlapped cannot be reused for multiple operations.";
public static string Argument_NativeOverlappedAlreadyFree = "'overlapped' has already been freed.";
public static string Argument_AlreadyBoundOrSyncHandle = "'handle' has already been bound to the thread pool, or was not opened for asynchronous I/O.";
public static string Argument_NativeOverlappedWrongBoundHandle = "'overlapped' was not allocated by this ThreadPoolBoundHandle instance.";

//; RuntimeInformation
public static string Argument_EmptyValue = "Value cannot be empty.";

//#if FEATURE_CRYPTO
public static string Cryptography_X509_InvalidFlagCombination = "The flags '{0}' may not be specified together.";
//#endif




// And the following strings, which were originally implemented as "properties", were found at
// "https://github.com/Kooboo/Kooboo/blob/master/Kooboo.HttpServer/src/Kooboo.HttpServer/System/SR_StrongType.cs".

public static string ArgumentException_BufferNotFromPool = "The buffer is not associated with this pool and may not be returned to it.";
public static string AdvanceToInvalidCursor = "Pipe is already advanced past provided cursor.";
// public static string Argument_BadFormatSpecifier = "Format specifier was invalid.";
public static string Argument_CannotParsePrecision = "Characters following the format symbol must be a number of {0} or less.";
public static string Argument_DestinationTooShort = "Destination is too short.";
public static string Argument_GWithPrecisionNotSupported = "The 'G' format combined with a precision is not supported.";
public static string Argument_InvalidTypeWithPointersNotSupported = "Cannot use type '{0}'. Only value types without pointers or references are supported.";
public static string Argument_OverlapAlignmentMismatch = "Overlapping spans have mismatching alignment.";
public static string Argument_PrecisionTooLarge = "Precision cannot be larger than {0}.";
public static string BackpressureDeadlock = "Advancing examined to the end would cause pipe to deadlock because FlushAsync is waiting.";
public static string CannotAdvancePastCurrentBufferSize = "Can't advance past buffer size.";
public static string CannotCompleteWhileReading = "Can't complete reader while reading.";
public static string CannotCompleteWhiteWriting = "Can&apos;t complete writer while writing.";
public static string ConcurrentOperationsNotSupported = "Concurrent reads or writes are not supported.";
public static string EndPositionNotReached = "End position was not reached during enumeration.";
public static string GetResultBeforeCompleted = "Can't GetResult unless awaiter is completed.";
public static string MemoryDisposed = "Memory<T> has been disposed.";
public static string NoReadingOperationToComplete = "No reading operation to complete.";
public static string NotSupported_CannotCallEqualsOnSpan = "Equals() on Span and ReadOnlySpan is not supported. Use operator== instead.";
public static string NotSupported_CannotCallGetHashCodeOnSpan = "GetHashCode() on Span and ReadOnlySpan is not supported.";
public static string NoWritingOperation = "No writing operation. Make sure GetMemory() was called.";
public static string OutstandingReferences = "Release all references before disposing this instance.";
public static string ReaderAndWriterHasToBeCompleted = "Both reader and writer has to be completed to be able to reset the pipe.";
public static string ReadingAfterCompleted = "Reading is not allowed after reader was completed.";
public static string ReadingIsInProgress = "Reading is already in progress.";
public static string UnexpectedSegmentType = "Unexpected segment type.";
public static string WritingAfterCompleted = "Writing is not allowed after writer was completed.";
// public static string ArgumentException_ValueTupleIncorrectType = "The parameter should be a ValueTuple type of appropriate arity.";
// public static string ArgumentException_ValueTupleLastArgumentNotAValueTuple = "The TRest type argument of ValueTuple`8 must be a ValueTuple.";
public static string CannotCallGetHashCodeOnSpan = "GetHashCode() on Span and ReadOnlySpan is not supported.";
public static string CannotCallEqualsOnSpan = "Equals() on Span and ReadOnlySpan is not supported. Use operator== instead.";



// And finally, there's the log messages we couldn't find, that we simply had to *make* *up*:
//
public static string Arg_KeyNotFoundWithKey = "The given key ({0}) was not present in the dictionary.";
public static string Memory_OutstandingReferences = "Release all references to this memory before disposing of it.";
public static string InvalidOperation_ConcurrentOperationsNotSupported = "Invalid Operation: Concurrent operations are not supported.";
public static string Argument_CannotExtractScalar = "Cannot extract scalar from the argument.";

public static String Format(String format, params object[] args)											 => String.Format(format, args);
public static String Format(String format, object arg0, object arg1, object arg2)							 => String.Format(format, arg0, arg1, arg2);
public static String Format(String format, object arg0, object arg1)										 => String.Format(format, arg0, arg1);
public static String Format(String format, object arg0)														 => String.Format(format, arg0);
public static String Format(IFormatProvider provider, String format, params object[] args)					 => String.Format(provider, format, args);
public static String Format(IFormatProvider provider, String format, object arg0, object arg1, object arg2)	 => String.Format(provider, format, arg0, arg1, arg2);
public static String Format(IFormatProvider provider, String format, object arg0, object arg1)				 => String.Format(provider, format, arg0, arg1);
public static String Format(IFormatProvider provider, String format, object arg0)							 => String.Format(provider, format, arg0);

	}
}
