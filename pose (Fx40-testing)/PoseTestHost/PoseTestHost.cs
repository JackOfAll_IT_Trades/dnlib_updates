﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.Reflection;

using System.IO;


namespace PoseTestHost
{
	class Program
	{
		static Dictionary<string, string> TopLevelDictionary = new Dictionary<string, string>();
		static Dictionary<string, string>  AnotherDictionary = new Dictionary<string, string>();

		static Assembly PoseTest_DLL;

		static int CycleSleepSeconds = 5;

		static Thread DictionaryTestThread = new Thread( delegate()
		{
			while (true)
			{
				string testEntryKey   = "TestEntryKey";
				string testEntryValue = "TestEntryValue";

				System.Console.WriteLine( "Inserting dictionary element:  TopLevelDictionary[{0}] = '{1}'...",
										  testEntryKey, testEntryValue );
				TopLevelDictionary[testEntryKey] = testEntryValue;

				System.Console.WriteLine( "Reading dictionary element:    TopLevelDictionary[{0}] = '{1}'...",
										  testEntryKey, TopLevelDictionary[testEntryKey] );

				System.Console.WriteLine( "Removing dictionary element:   TopLevelDictionary[{0}]...",
										  testEntryKey );

				TopLevelDictionary.Remove(testEntryKey);

				System.Console.WriteLine( "Sleeping for {0} seconds...", CycleSleepSeconds );
				Thread.Sleep( CycleSleepSeconds * 1000 );
			}
		});

		static Thread PoseTestThread = new Thread( delegate()
		{
			Type PoseTestClass = PoseTest_DLL.GetType("PoseTest_DLL.PoseTest");

//			var c = Activator.CreateInstance(theType);

			MethodInfo fixDictionary_Func = PoseTestClass.GetMethod("FixDictionaryBug");

			fixDictionary_Func.Invoke(null, new object[] { @"NewValue" });
		});

		static void Main(string[] args)
		{
			System.Console.WriteLine("Main():  Entering...");

			string assemblyPath = Assembly.GetExecutingAssembly().Location;
			string assemblyDir  = Path.GetDirectoryName(assemblyPath);

//			string PoseTest_DLL_path = Path.Combine(assemblyDir, "PoseTest_DLL.dll");

			string binaries_Dir = Path.GetFullPath(Path.Combine( assemblyDir, @".."));
			string project__Dir = Path.GetFullPath(Path.Combine(binaries_Dir, @".."));
			string solution_Dir = Path.GetFullPath(Path.Combine(project__Dir, @".."));

			string PoseTest_DLL_path = Path.Combine(solution_Dir, @"PoseTest_DLL\bin\Debug\netstandard2.0\PoseTest_DLL.dll");

			PoseTest_DLL = Assembly.LoadFile(PoseTest_DLL_path);

			string mainTestEntryKey   = "MainTestEntryKey";
			string mainTestEntryValue = "MainTestEntryValue";

			System.Console.WriteLine("Main() -- Inserting dictionary element:  TopLevelDictionary[{0}] = '{1}'...",
									  mainTestEntryKey, mainTestEntryValue);
			TopLevelDictionary[mainTestEntryKey] = mainTestEntryValue;

			System.Console.WriteLine("Main() -- Reading dictionary element:    TopLevelDictionary[{0}] = '{1}'...",
									  mainTestEntryKey, TopLevelDictionary[mainTestEntryKey]);

			System.Console.WriteLine("Main() -- Removing dictionary element:   TopLevelDictionary[{0}]...",
									  mainTestEntryKey);

			TopLevelDictionary.Remove(mainTestEntryKey);

			System.Console.WriteLine("Main() -- Inserting another dictionary element:  AnotherDictionary[{0}] = '{1}'...",
									  mainTestEntryKey, mainTestEntryValue);
			AnotherDictionary[mainTestEntryKey] = mainTestEntryValue;

			//			string BetterDictionaryTypeName = "PoseTest_DLL.BetterDictionary";
			string BetterDictionaryTypeName = "PoseTest_DLL.BetterDictionary`2";
			Type   BetterDictionaryClass    = PoseTest_DLL.GetType(BetterDictionaryTypeName);

			System.Console.WriteLine("Main():  Starting <DictionaryTestThread>...");
			DictionaryTestThread.Start();

			System.Console.WriteLine("Waiting for 3 cycles...");
			Thread.Sleep( 3 * CycleSleepSeconds * 1000 );

			System.Console.WriteLine("Main():  Starting <PoseTestThread>...");
			PoseTestThread.Start();

		}
	}
}
